<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontpage extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here	
	}

	public function index()
	{
		$this->load->model('Berita_model');
		$this->load->model('Produk_model');
		$this->load->model('Slide_model');
		$this->load->model('Testimoni_model');
		$m_berita = new Berita_model();
		$m_produk = new Produk_model();
		$m_slide  = new Slide_model();
		$m_testimoni = new Testimoni_model();

		$data['data_artikel'] = $m_berita->view_data_berita_limit()->result();
		$data['data_produk']  = $m_produk->view_data_produk()->result();
		$data['data_slide']   = $m_slide->view_data_slide()->result();
		$data['data_testimoni'] = $m_testimoni->view_data_testimoni()->result();

		$this->load->view('frontpages/page/home_view', $data);		
	}

	public function produk_view() {
		$this->load->model('Produk_model');
		$m_produk = new Produk_model();

		$data['data_produk'] = $m_produk->view_data_produk()->result();
		$this->load->view('frontpages/page/produk_view', $data);
	}

	public function testimoni_view() {
		$this->load->view('frontpages/page/testimoni_view');
	}

	public function news_view() {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();
		$data['data_artikel'] = $m_berita->view_data_berita_limit()->result();
		$this->load->view('frontpages/page/news_view', $data);
	}

	public function news_detail_view($slug_article) {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();

		$data['data_berita'] = $m_berita->view_data_berita_by_slug('tbl_kukila_artikel', $slug_article)->result();
		$this->load->view('frontpages/page/detail_view', $data);
	}

	public function profil_view() {
		$this->load->view('frontpages/page/profil_view');
	}

	public function kontak_view() {

		$this->load->view('frontpages/page/kontak_view', $data);
	}

	public function galeri_view() {
		$this->load->view('frontpages/page/galery_view');
	}

	public function distribusi() {
		$this->load->model('Customer_model');
		$this->load->model('Wilayah_model');
		$m_customer = new Customer_model();
		$m_wilayah = new Wilayah_model();


		$data['data_wilayah'] = $m_wilayah->view_data_wilayah()->result();
		$data['data_customer'] = $m_customer->view_data_customer();

		$this->load->view('frontpages/page/daftar_distribusi_view', $data);
	}

	public function detail_distribusi($nama_wilayah, $latitude, $longitude) {
		$this->load->model('Customer_model');
		$m_customer = new Customer_model();

		$data['data_detail_customer'] = $m_customer->view_detail_customer_by_maps($nama_wilayah, $latitude, $longitude);
		$data['data_customer_wilayah'] = $m_customer->view_detail_customer_by_wilayah($nama_wilayah, $latitude, $longitude);
		$data['nama_wilayah'] = strtoupper(str_replace('-', ' ', $nama_wilayah));
		$this->load->view('frontpages/page/detail_distribusi_view', $data);

	}

	public function detail_puyuh_petelur_view() {
		$this->load->view('frontpages/page/detail_puyuh_petelur_view');
	}

	public function detail_ayam_petelur_view() {
		$this->load->view('frontpages/page/detail_ayam_petelur');
	}

	public function detail_ayam_pedaging_view() {
		$this->load->view('frontpages/page/detail_ayam_pedaging_view');
	}

	public function detail_ayam_joper_view() {
		$this->load->view('frontpages/page/detail_ayam_joper_view');
	}

	/**
	
		TODO:
		- backend system processing
		- Web Developer Alif Benden Arnado 
		- 09-04-2018
	
	 */
	

	public function action_kontak() {
		$this->load->model('Kontak_modal');
		$m_kontak = new Kontak_modal();;

		$kode_pesan		= date('yW').'-'.implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 6), 6));
		$nama_pesan 	= htmlspecialchars($this->input->post('nama_pesan'));
		$email_address 	= htmlspecialchars($this->input->post('email_address'));
		$nomor_telepon 	= htmlspecialchars($this->input->post('nomor_telepon'));
		$type_pesan 	= htmlspecialchars($this->input->post('type_pesan'));
		$alamat 		= htmlspecialchars($this->input->post('alamat'));
		$wilayah 		= htmlspecialchars($this->input->post('wilayah'));
		$nama_pakan 	= htmlspecialchars($this->input->post('nama_pakan'));
		$isi_pesan 		= $this->input->post('isi_pesan');

		if ($type_pesan === "kritik_saran") {

			$data_pesan_kritik_saran = array(
					'kode_pesan' 	=> $kode_pesan,
					'nama_pesan' 	=> $nama_pesan,
					'email_address' => $email_address,
					'nomor_telepon' => $nomor_telepon,
					'type_pesan' 	=> $type_pesan,
					'alamat' 		=> $alamat,
					'isi_pesan' 	=> $isi_pesan
			);
			$m_kontak->input_data_kontak('tbl_kukila_kontak', $data_pesan_kritik_saran);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Terimakasih Pesan Kritik dan Saran Berhasil Di Kirim.');
			redirect('admin/berita-video','refresh');

		} else if ($type_pesan === "order_pakan") {
			$data_pesan_order_pakan = array(
					'kode_pesan' 	=> $kode_pesan,
					'nama_pesan' 	=> $nama_pesan,
					'email_address' => $email_address,
					'nomor_telepon' => $nomor_telepon,
					'type_pesan' 	=> $type_pesan,
					'alamat' 		=> $alamat,
					'wilayah'		=> $wilayah,
					'isi_pesan' 	=> $isi_pesan
			);
			$m_kontak->input_data_kontak('tbl_kukila_kontak', $data_pesan_order_pakan);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Terimakasih Order Pakan Anda Berhasil Di Kirim.');
			redirect('admin/berita-video','refresh');


		} else if ($type_pesan === "request_pakan") {
			$data_pesan_request_pakan = array(
					'kode_pesan' 	=> $kode_pesan,
					'nama_pesan' 	=> $nama_pesan,
					'email_address' => $email_address,
					'nomor_telepon' => $nomor_telepon,
					'type_pesan' 	=> $type_pesan,
					'alamat' 		=> $alamat,
					'nama_pakan'	=> $nama_pakan,
					'isi_pesan' 	=> $isi_pesan
			);
			$m_kontak->input_data_kontak('tbl_kukila_kontak', $data_pesan_request_pakan);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Terimakasih Pesan Request Pakan Berhasil Di Kirim dan sedang proses.');
			redirect('admin/berita-video','refresh');

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Maff pesan anda belum dapat terkirim');
			redirect($this->agent->referrer());
		}
	}

	public function search_news() {
		// print_r($this->input->get());

		$search =htmlentities(trim($this->input->get('search')));
		$kata_kunci_split = preg_split('/[\s]+/', $search);
		$total_kata_kunci = count($kata_kunci_split);

		$where = "";
		foreach($kata_kunci_split as $key=>$kunci) {
			//set variabel $where untuk query nanti
			$where .= "nama_artikel LIKE '%$kunci%'";
			//jika kata kunci lebih dari 1 (2 dan seterusnya) maka di tambahkan OR di perulangannya
			if($key != ($total_kata_kunci - 1)){
				$where .= " OR ";
			}
		}

		$where_produk = "";
		foreach($kata_kunci_split as $key=>$kunci) {
			//set variabel $where untuk query nanti
			$where_produk .= "nama_produk LIKE '%$kunci%'";
			//jika kata kunci lebih dari 1 (2 dan seterusnya) maka di tambahkan OR di perulangannya
			if($key != ($total_kata_kunci - 1)){
				$where_produk .= " OR ";
			}
		}

		$sql = "SELECT * FROM tbl_kukila_artikel where $where";
		$sql_produk = "SELECT * FROM tbl_kukila_produk_link where $where_produk";

		// print_r($sql_produk);


		$data['keyword_search'] = $search;
		$data['data_search']    = $this->db->query($sql)->result();
		$data['data_produk']	= $this->db->query($sql_produk)->result();
		$data['count_result']   = $this->db->query($sql)->num_rows();
		$data['count_result_produk']	= $this->db->query($sql_produk)->result();
		
		if (($data['count_result'] == 0) && ($data['count_result_produk']) == 0) {
			$this->load->view('frontpages/page/news_result_found_view', $data);
		} else {
			$this->load->view('frontpages/page/news_result_search_view', $data);
		}
	}


}

/* End of file Frontpage.php */
/* Location: ./application/controllers/Frontpage.php */