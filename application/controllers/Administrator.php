<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$this->load->view('adminpages/page/home_view');
	}

	public function data_produk_view() {
		$this->load->model('Produk_model');
		$m_produk = new Produk_model();

		$data['data_produk'] = $m_produk->view_data_produk()->result();
		$this->load->view('adminpages/page/data_produk_view', $data);
	}

	public function tambah_produk_view() {
		$this->load->view('adminpages/page/tambah_produk_view');
	}

	public function edit_data_produk($kode_produk) {
		$this->load->model('Produk_model');
		$m_produk = new Produk_model();

		$valid = $this->db->query("SELECT * FROM tbl_kukila_produk WHERE kode_produk = '$kode_produk'")->num_rows();
		if ($valid > 0) {
			$data['data_produk'] = $m_produk->view_data_produk_by_kode_produk('tbl_kukila_produk', $kode_produk)->result();
			$data['data_analisa'] = $m_produk->view_data_produk_by_kode_produk('tbl_kukila_analisa_produk', $kode_produk)->result();
			$this->load->view('adminpages/page/edit_data_produk_view', $data);
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kode Tidak Tersedia');
			redirect($this->agent->referrer());
		}
	}

	public function data_berita_view() {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();

		$data['data_berita_text'] = $m_berita->view_data_berita_by_type('tbl_kukila_artikel', 'Text')->result();
		$this->load->view('adminpages/page/data_berita_view', $data);
	}

	public function data_berita_video_view() {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();

		$data['data_berita_video'] = $m_berita->view_data_berita_by_type('tbl_kukila_artikel', 'Video')->result();
		$this->load->view('adminpages/page/data_berita_video_view', $data);
	}

	public function tambah_berita_view() {
		$this->load->view('adminpages/page/tambah_berita_view');
	}

	public function data_testimoni_view() {
		$this->load->model('Testimoni_model');
		$m_testimoni = new Testimoni_model();

		$data['data_testimoni_text'] = $m_testimoni->view_data_testimoni_by_type('tbl_kukila_testimoni', 'Testimoni Text')->result();
		$this->load->view('adminpages/page/data_testimoni_view', $data);
	}

	public function data_testimoni_video_view() {
		$this->load->model('Testimoni_model');
		$m_testimoni = new Testimoni_model();

		$data['data_testimoni_video'] = $m_testimoni->view_data_testimoni_by_type('tbl_kukila_testimoni','Video')->result();
		$this->load->view('adminpages/page/data_testimoni_video_view', $data);
	}

	public function tambah_testimoni_view() {
		$this->load->view('adminpages/page/tambah_testimoni_view');
	}

	public function data_kontak_pesan_view() {
		$this->load->model('Kontak_model');
		$m_kontak = new Kontak_model();
		$data['data_kontak'] = $m_kontak->view_data_kontak_by_type('tbl_kukila_kontak', 'kritik_saran')->result();
		$this->load->view('adminpages/page/data_kontak_pesan_saran_view', $data);
	}

	public function data_kontak_order_view() {
		$this->load->model('Kontak_model');
		$m_kontak = new Kontak_model();
		$data['data_kontak'] = $m_kontak->view_data_kontak_by_type('tbl_kukila_kontak', 'order_pakan')->result();
		$this->load->view('adminpages/page/data_kontak_order_view', $data);
	}

	public function data_kontak_request_pakan_view() {
		$this->load->model('Kontak_model');
		$m_kontak = new Kontak_model();
		$data['data_kontak'] = $m_kontak->view_data_kontak_by_type('tbl_kukila_kontak', 'request_pakan')->result();
		$this->load->view('adminpages/page/data_kontak_request_pakan_view', $data);
	}

	public function data_distributor() {
		$this->load->model('Wilayah_model');
		$this->load->model('Customer_model');

    	$m_customer = new Customer_model();
    	$m_wilayah = new Wilayah_model();

    	$data['data_wilayah'] = $m_wilayah->view_data_wilayah()->result();
    	$data['data_depo']	= $m_customer->view_data_customer();
		$this->load->view('adminpages/page/data_distributor_view', $data);
	}

	public function data_galleri() {
		
	}

	public function tambah_data_galleri_view() {

	}


	/**
	 *
	 * Setting Menu
	 *
	 */
	
	public function data_slide_view() {
		$this->load->model('Slide_model');
		$m_slide = new Slide_model();

		$data['data_slide'] = $m_slide->view_data_slide()->result();

		$this->load->view('adminpages/page/data_slide_view', $data);
	}

	public function tambah_slide_view() {
		$this->load->view('adminpages/page/tambah_slide_view');
	}	


	/**
		TODO:
		- Backend Prosess 
		- Alif Benden Arnado
		- Support Web Developer
	 */
	
	public function action_tambah_produk() {
		$this->load->model('Produk_model');
		$m_produk = new Produk_model();

		$kode_produk 		= implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 18), 6));
		$nama_produk 		= $this->input->post('nama_produk');
		$remove_strip 		= str_replace("-"," ", $nama_produk);
		$kata_kunci_produk  = $this->slug($remove_strip);

		$kategori_produk 	= $this->input->post('kategori_produk');
		$deskripsi_produk 	= $this->input->post('deskripsi_produk');

		/** 
		 * Nilai analisa
		 */
		$nama_analisa 			= $this->input->post('nama_analisa');
		$nilai_analisa 			= $this->input->post('nilai_analisa');
		$hasil_produk  			= $this->input->post('hasil_produk');
		$rate_analisa           = $this->input->post('rate');

		$file_name  = $_FILES['foto_cover']['name'];
		$error      = $_FILES['foto_cover']['error'];
		$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
		$new_file_name_cover = date('yW').'_produk_'.$kata_kunci_produk.'.'.$file_type;

		$config['upload_path'] = './storage_img/img_produk/';
		$config['allowed_types'] = 'jpg|png';
		$config['max_size']  = '*';
		$config['file_name'] = $new_file_name_cover;

		
		if ($error > 0) {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Gambar tidak ada');
			redirect($this->agent->referrer());
		} else {

			$this->load->library('upload', $config);
			if (!is_dir('./storage_img/img_produk/')) {
				mkdir('./storage_img/img_produk/', 0777);
				if ( ! $this->upload->do_upload('foto_cover')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menambahkan Produk');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

			} else {
				if ( ! $this->upload->do_upload('foto_cover')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menambahkan Produk');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}
			}
		}

			$data_produk = array(
				'kode_produk' 		=> $kode_produk,
				'kata_kunci'  		=> $kata_kunci_produk,
				'nama_produk' 		=> $nama_produk,
				'kategori_produk' 	=> $kategori_produk,
				'cover_produk'		=> $new_file_name_cover,
				'deskripsi_produk' 	=> $deskripsi_produk,
				'hasil_produk'		=> $hasil_produk,
				'rate_analisa'		=> $rate_analisa,
				'create_by' 		=> 'admin',
				'create_at' 		=> date("Y-m-d H:i:s"),
				'status_post'		=> 'active'
			);


			for ($i = 0; $i < count($nama_analisa) ; $i++) {
				$data_analisa = array(
						'kode_produk' => $kode_produk,
						'nama_analisa' => $nama_analisa[$i],
						'nilai_analisa' => $nilai_analisa[$i]
					);
				$m_produk->analisa_produk_insert_data('tbl_kukila_analisa_produk', $data_analisa);
			}

		$m_produk->produk_insert_data('tbl_kukila_produk', $data_produk);
		$this->session->set_flashdata('message_data', '<strong>Success</strong> Produk berhasil ditambahkan.');
		redirect($this->agent->referrer());

	}

	public function action_edit_produk() {

		$this->load->model('Produk_model');
		$m_produk = new Produk_model();

		$kode_produk 		= $this->input->post('kode_produk');
		$nama_produk 		= $this->input->post('nama_produk');
		$remove_strip 		= str_replace("-"," ", $nama_produk);
		$kata_kunci_produk  = $this->slug($remove_strip);

		$kategori_produk 	= $this->input->post('kategori_produk');
		$deskripsi_produk 	= $this->input->post('deskripsi_produk');
		$old_cover          = $this->input->post('old_cover');

		$kode_analisa           = $this->input->post('kode_analisa');
		$nama_analisa 			= $this->input->post('nama_analisa');
		$nilai_analisa 			= $this->input->post('nilai_analisa');
		$hasil_produk  			= $this->input->post('hasil_produk');
		$rate_analisa           = $this->input->post('rate');


		$valid = $this->db->query("SELECT * FROM tbl_kukila_produk WHERE kode_produk = '$kode_produk'")->num_rows();
		if ($valid > 0) {

			if ($_FILES['foto_cover']['error'] > 0) {
				$data_produk = array(
					'kata_kunci'  		=> $kata_kunci_produk,
					'nama_produk' 		=> $nama_produk,
					'kategori_produk' 	=> $kategori_produk,
					'deskripsi_produk' 	=> $deskripsi_produk,
					'cover_produk'		=> $old_cover,
					'hasil_produk'		=> $hasil_produk,
					'rate_analisa'		=> $rate_analisa
				);

				$m_produk->edit_data_produk('tbl_kukila_produk', $kode_produk, $data_produk);

				for ($i = 0; $i < count($nama_analisa) ; $i++) {
					$data_analisa = array(
							'kode_produk' 	=> $kode_produk,
							'nama_analisa' 	=> $nama_analisa[$i],
							'nilai_analisa' => $nilai_analisa[$i]
						);
					$m_produk->analisa_produk_update_data('tbl_kukila_analisa_produk', $kode_analisa[$i], $kode_produk, $data_analisa);
				}

				$this->session->set_flashdata('message_data', '<strong>Success</strong> Produk berhasil diedit.');
				redirect('admin/produk', 'refresh');

			} else {
				
				$file_name  = $_FILES['foto_cover']['name'];
				$error      = $_FILES['foto_cover']['error'];
				$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
				$new_file_name_cover = date('yW').'_produk_'.$kata_kunci_produk.'.'.$file_type;

				$dir_remove_file_old = "./storage_img/img_produk/".$old_cover;
				if (file_exists($dir_remove_file_old)) {
					unlink($dir_remove_file_old);
				} 

				$config['upload_path'] = './storage_img/img_produk/';
				$config['allowed_types'] = 'jpg|png';
				$config['max_size']  = '*';
				$config['file_name'] = $new_file_name_cover;
				
				$this->load->library('upload', $config);
				if (!is_dir('./storage_img/img_produk/')) {
					mkdir('./storage_img/img_produk/', 0777);
					if ( ! $this->upload->do_upload('foto_cover')){
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat mengubah Produk');
						redirect('admin/produk', 'refresh');
					}
					else{
						$data = array('upload_data' => $this->upload->data());
					}

				} else {
					if ( ! $this->upload->do_upload('foto_cover')){
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat mengubah Produk');
						redirect('admin/produk', 'refresh');
					}
					else{
						$data = array('upload_data' => $this->upload->data());
					}
				}

				$data_produk = array(
					'kata_kunci'  		=> $kata_kunci_produk,
					'nama_produk' 		=> $nama_produk,
					'kategori_produk' 	=> $kategori_produk,
					'deskripsi_produk' 	=> $deskripsi_produk,
					'cover_produk'		=> $new_file_name_cover,
					'hasil_produk'		=> $hasil_produk,
					'rate_analisa'		=> $rate_analisa
				);

				$m_produk->edit_data_produk('tbl_kukila_produk', $kode_produk, $data_produk);

				for ($i = 0; $i < count($nama_analisa) ; $i++) {
					$data_analisa = array(
							'kode_produk' 	=> $kode_produk,
							'nama_analisa' 	=> $nama_analisa[$i],
							'nilai_analisa' => $nilai_analisa[$i]
						);
					$m_produk->analisa_produk_update_data('tbl_kukila_analisa_produk', $kode_analisa[$i], $kode_produk, $data_analisa);
				}

				$this->session->set_flashdata('message_data', '<strong>Success</strong> Produk berhasil diedit.');
				redirect('admin/produk', 'refresh');
			}

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat edit produk');
			redirect('admin/produk', 'refresh');
		}

	}

	public function action_delete_produk($kode_produk) {

		$this->load->model('Produk_model');
		$m_produk = new Produk_model();

		$get_file_name_album = $m_produk->analisa_produk_view_by_kode_produk('tbl_kukila_produk_album', $kode_produk)->result();
		$get_file_name 		 = $this->db->query("SELECT cover_produk FROM tbl_kukila_produk WHERE kode_produk = '$kode_produk'")->row();
		$valid = $this->db->query("SELECT * FROM tbl_kukila_produk WHERE kode_produk = '$kode_produk'")->num_rows();
		if ($valid > 0) {
			$dir_remove_file_old = "./storage_img/img_produk/".$get_file_name->cover_produk;
			if (file_exists($dir_remove_file_old)) {
				unlink($dir_remove_file_old);
			} 

			$m_produk->produk_delete_data('tbl_kukila_produk', $kode_produk);
			$m_produk->analisa_produk_delete_data('tbl_kukila_analisa_produk', $kode_produk);

			$this->session->set_flashdata('message_data', '<strong>Success</strong> Produk berhasil hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat hapus produk');
			redirect($this->agent->referrer());
		}

	}

	/*==============================
	=            Berita            =
	==============================*/
	
	public function action_tambah_berita() {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();

        $kode_artikel 		= date('yW').'-'.implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 6), 6));
		$judul_artikel    = htmlspecialchars($this->input->post('judul_artikel'));
		$type_artikel     = htmlspecialchars($this->input->post('type_artikel'));
		$kategori_artikel = htmlspecialchars($this->input->post('kategori_artikel'));
		$link_id_youtube  = htmlspecialchars($this->input->post('link_id_youtube'));
		$isi_artikel     = $this->input->post('isi_artikel');

		$tags = $this->input->post('tags');

		$remove_strip 	  = str_replace("-"," ", $judul_artikel);
		$kata_kunci_name  = $this->slug($remove_strip);

		if ($type_artikel === "Video") {

			$data_artikel_video = array(
										'kode_artikel'		=> $kode_artikel,
										'key_nama_artikel' 	=> $kata_kunci_name,
										'nama_artikel' 		=> $judul_artikel,
										'type_artikel' 		=> $type_artikel,
										'kategori_artikel' 	=> $kategori_artikel,
										'link_video'		=> $link_id_youtube,
										'isi_artikel'		=> $isi_artikel,
										'tags'				=> $tags,
										'status_post'		=> 'active',
										'create_by'			=> 'admin',
										'create_at'			=> date("Y-m-d H:i:s")
									);
			$m_berita->input_data_berita('tbl_kukila_artikel', $data_artikel_video);

			$this->session->set_flashdata('message_data', '<strong>Success</strong> Berita Video berhasil di tambahkan.');
			redirect('admin/berita-video','refresh');

		} else {

			$error_cover = $_FILES['cover_artikel']['error'];
			if ($error_cover > 0) {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Gambar Tidak Boleh Kosong');
				redirect($this->agent->referrer());
			} else {

				$remove_strip 	  = str_replace("-"," ", $judul_artikel);
				$kata_kunci_name  = $this->slug($remove_strip);

				$file_name  = $_FILES['cover_artikel']['name'];
				$error      = $_FILES['cover_artikel']['error'];
				$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
				$new_file_name = date('ymd').'-'.$kata_kunci_name.'-berita.'.$file_type;

				$config['upload_path'] = './storage_img/img_berita/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '1024';
				$config['file_name'] = $new_file_name;
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('cover_artikel')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menambahkan artikel');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

				$data_artikel_text = array(
										'kode_artikel'		=> $kode_artikel,
										'key_nama_artikel' 	=> $kata_kunci_name,
										'nama_artikel' 		=> $judul_artikel,
										'type_artikel' 		=> $type_artikel,
										'kategori_artikel' 	=> $kategori_artikel,
										'cover_artikel'		=> $new_file_name,
										'isi_artikel'		=> $isi_artikel,
										'tags'				=> $tags,
										'status_post'		=> 'active',
										'create_by'			=> 'admin',
										'create_at'			=> date("Y-m-d H:i:s")
									);
				$m_berita->input_data_berita('tbl_kukila_artikel', $data_artikel_text);

				$this->session->set_flashdata('message_data', '<strong>Success</strong> Berita Text berhasil di tambahkan.');
				redirect('admin/berita','refresh');
			}

		}
	}

	public function action_edit_berita_video() {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();

		$kode_artikel 	  = htmlspecialchars($this->input->post('kode_artikel'));
		$judul_artikel 	  = htmlspecialchars($this->input->post('judul_artikel'));
		$kategori_artikel = htmlspecialchars($this->input->post('kategori_artikel'));
		$link_id_youtube  = htmlspecialchars($this->input->post('link_id_youtube'));
		$isi_artikel	  = $this->input->post('isi_artikel');
		$tags 		 	  = $this->input->post('tags');

		$valid = $this->db->query("SELECT * FROM tbl_kukila_artikel WHERE type_artikel = 'Video' AND kode_artikel = '$kode_artikel'")->num_rows();
		if ($valid > 0) {

			$remove_strip 	  = str_replace("-"," ", $judul_artikel);
			$kata_kunci_name  = $this->slug($remove_strip);
			$data_artikel_video = array(
						'key_nama_artikel' 	=> $kata_kunci_name,
						'nama_artikel' 		=> $judul_artikel,
						'kategori_artikel'	=> $kategori_artikel,
						'link_video'		=> $link_id_youtube,
						'isi_artikel'		=> $isi_artikel,
						'tags'				=> $tags
					);

			$m_berita->edit_data_berita('tbl_kukila_artikel', $kode_artikel, 'Video', $data_artikel_video);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Artikel Video berhasil di ubah.');
			redirect('admin/berita-video','refresh');
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Data artikel tidak tersedia');
			redirect($this->agent->referrer());
		}
	}

	public function action_edit_berita() {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();

		$kode_artikel 		= $this->input->post('kode_artikel');
		$judul_artikel 		= $this->input->post('judul_artikel');
		$kategori_artikel 	= $this->input->post('kategori_artikel');
		$cover_old 			= $this->input->post('cover_old');
		$isi_artikel 		= $this->input->post('isi_artikel');
		$tags               = $this->input->post('tags');

		$valid = $this->db->query("SELECT * FROM tbl_kukila_artikel WHERE type_artikel = 'Text' AND kode_artikel = '$kode_artikel'")->num_rows();
		if($valid > 0) {

			$cover_error = $_FILES['cover_artikel']['error'];
			if ($cover_error > 0) {
				$remove_strip 	  = str_replace("-"," ", $judul_artikel);
				$kata_kunci_name  = $this->slug($remove_strip);

				$data_artikel_text = array(
					'key_nama_artikel' 	=> $kata_kunci_name,
					'nama_artikel' 		=> $judul_artikel,
					'kategori_artikel' 	=> $kategori_artikel,
					'cover_artikel'		=> $cover_old,
					'isi_artikel'		=> $isi_artikel,
					'tags'				=> $tags
				);

				$m_berita->edit_data_berita('tbl_kukila_artikel', $kode_artikel, 'Text', $data_artikel_text);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Artikel Text berhasil di ubah.');
				redirect('admin/berita','refresh');

			} else {
				
				$remove_strip 	  = str_replace("-"," ", $judul_artikel);
				$kata_kunci_name  = $this->slug($remove_strip);

				$file_name  = $_FILES['cover_artikel']['name'];
				$error      = $_FILES['cover_artikel']['error'];
				$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
				$new_file_name = date('ymd').'-'.$kata_kunci_name.'-berita.'.$file_type;

				$config['upload_path'] = './storage_img/img_berita/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '1024';
				$config['file_name'] = $new_file_name;

				$dir = './storage_img/img_berita/';
				if (is_dir($dir)) {
					unlink($dir.$cover_old);
				}
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('cover_artikel')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat mengubah artikel');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

				$data_artikel_text = array(
					'key_nama_artikel' 	=> $kata_kunci_name,
					'nama_artikel' 		=> $judul_artikel,
					'kategori_artikel' 	=> $kategori_artikel,
					'cover_artikel'		=> $new_file_name,
					'isi_artikel'		=> $isi_artikel,
					'tags'				=> $tags
				);

				$m_berita->edit_data_berita('tbl_kukila_artikel', $kode_artikel, 'Text', $data_artikel_text);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Artikel Text berhasil di ubah.');
				redirect('admin/berita','refresh');
			}

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Data artikel tidak tersedia');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_berita($kode_berita) {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();

		$sql = "SELECT * FROM tbl_kukila_artikel WHERE kode_artikel = '$kode_berita'";
		$valid = $this->db->query($sql)->num_rows();
		if ($valid > 0) {

			$data_berita = $this->db->query($sql)->row();

			if ($data_berita->type_artikel === "Text") {
				$dir = './storage_img/img_berita/';
				if (is_dir($dir)) {
					unlink($dir.$data_berita->cover_artikel);
				}
			}

			$m_berita->delete_data_berita('tbl_kukila_artikel', $kode_berita);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Artikel berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Data artikel tidak tersedia');
			redirect($this->agent->referrer());
		}
	}

	/*=====  End of Berita  ======*/


	public function action_tambah_testimoni() {
		$this->load->model('Testimoni_model');
		$m_testimoni = new Testimoni_model();

        $kode_testimoni 	= date('yW').'-'.implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 6), 6));
		$kategori_testimoni = htmlspecialchars($this->input->post('kategori_testimoni'));
		if ($kategori_testimoni === "Video") {
			$nama_testimoni 	= htmlspecialchars($this->input->post('nama_testimoni'));
			$profesi_testimoni = htmlspecialchars($this->input->post('profesi_testimoni'));
			$link_id_youtube 	= htmlspecialchars($this->input->post('link_id_youtube'));
			$isi_testimoni 		= htmlspecialchars($this->input->post('isi_testimoni'));

			$data_testimoni_video = array(
						'kode_testimoni' => $kode_testimoni,
						'nama_testimoni' => $nama_testimoni,
						'profesi_testimoni' => $profesi_testimoni,
						'kategori_testimoni' => $kategori_testimoni,
						'link_video' => $link_id_youtube,
						'isi_testimoni' => $isi_testimoni,
						'create_at' => date("Y-m-d H:i:s")
				);
			$m_testimoni->input_data_testimoni('tbl_kukila_testimoni', $data_testimoni_video);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni Video berhasil di tambahkan.');
			redirect('admin/testimoni-video','refresh');

		} else {
			$img_error = $_FILES['cover_testimoni']['error'];
			if ($img_error > 0) {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Gambar Tidak Boleh Kosong');
				redirect($this->agent->referrer());
			} else {

				$nama_testimoni = htmlspecialchars($this->input->post('nama_testimoni'));
				$profesi_testimoni = htmlspecialchars($this->input->post('profesi_testimoni'));
				$isi_testimoni  = htmlspecialchars($this->input->post('isi_testimoni'));

				$remove_strip 	  = str_replace("-"," ", $nama_testimoni);
				$kata_kunci_name  = $this->slug($remove_strip);

				$file_name  = $_FILES['cover_testimoni']['name'];
				$error      = $_FILES['cover_testimoni']['error'];
				$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
				$new_file_name = date('ymd').'_'.$kode_testimoni.'_'.$kata_kunci_name.'_testimoni.'.$file_type;

				$config['upload_path'] = './storage_img/img_testimoni/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '1024';
				$config['file_name']  = $new_file_name;
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('cover_testimoni')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menambahkan Testimoni');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

				$data_testimoni_text = array(
						'kode_testimoni' => $kode_testimoni,
						'nama_testimoni' => $nama_testimoni,
						'profesi_testimoni' => $profesi_testimoni,
						'kategori_testimoni' => $kategori_testimoni,
						'cover_testimoni' => $new_file_name,
						'isi_testimoni' => $isi_testimoni,
						'create_at' => date("Y-m-d H:i:s")
				);

				$m_testimoni->input_data_testimoni('tbl_kukila_testimoni', $data_testimoni_text);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni Text berhasil di tambahkan.');
				redirect('admin/testimoni','refresh');

			}
		}
	}

	public function action_edit_testimoni() {
		$this->load->model('Testimoni_model');
		$m_testimoni = new Testimoni_model();

		$kode_testimoni 	= $this->input->post('kode_testimoni');
		$nama_testimoni     = $this->input->post('nama_testimoni');
		$profesi_testimoni  = $this->input->post('profesi_testimoni');
		$cover_old 			= $this->input->post('cover_old');
		$isi_testimoni 		= $this->input->post('isi_testimoni');

		$valid = $this->db->query("SELECT * FROM tbl_kukila_testimoni WHERE type_testimoni = 'Testimoni Text' AND kode_testimoni = '$kode_testimoni'")->num_rows();
		if ($valid > 0) {

			$error_testimoni = $_FILES['cover_testimoni']['error'];
			if ($error_testimoni > 0) {
				$data_testimoni_text = array(
						'nama_testimoni'     => $nama_testimoni,
						'profesi_testimoni'  => $profesi_testimoni,
						'cover_testimoni'    => $cover_old,
						'isi_testimoni'      => $isi_testimoni
				);
				$m_testimoni->edit_data_testimoni('tbl_kukila_testimoni', $kode_testimoni, 'Testimoni Text', $data_testimoni_text);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni Text berhasil di ubah.');
				redirect('admin/testimoni','refresh');

			} else {

				$remove_strip 	  = str_replace("-"," ", $nama_testimoni);
				$kata_kunci_name  = $this->slug($remove_strip);

				$file_name  = $_FILES['cover_testimoni']['name'];
				$error      = $_FILES['cover_testimoni']['error'];
				$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
				$new_file_name = date('ymd').'_'.$kode_testimoni.'_'.$kata_kunci_name.'_testimoni.'.$file_type;

				$dir = "./storage_img/img_testimoni/";
				if(is_dir($dir)){
					unlink($dir.$cover_old);
				}

				$config['upload_path'] = './storage_img/img_testimoni/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '1024';
				$config['file_name']  = $new_file_name;
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('cover_testimoni')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menambahkan Testimoni');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

				$data_testimoni_text = array(
						'nama_testimoni' => $nama_testimoni,
						'profesi_testimoni' => $profesi_testimoni,
						'cover_testimoni' => $new_file_name,
						'isi_testimoni' => $isi_testimoni
				);

				$m_testimoni->edit_data_testimoni('tbl_kukila_testimoni', $kode_testimoni, 'Testimoni Text', $data_testimoni_text);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni Text berhasil di ubah.');
				redirect('admin/testimoni','refresh');
			}	

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat mengubah Testimoni');
			redirect($this->agent->referrer());
		}
	}

	public function action_edit_testimoni_video() {
		$this->load->model('Testimoni_model');
		$m_testimoni = new Testimoni_model();

		$kode_testimoni     = htmlspecialchars($this->input->post('kode_testimoni'));
		$nama_testimoni 	= htmlspecialchars($this->input->post('nama_testimoni'));
		$link_id_youtube 	= htmlspecialchars($this->input->post('link_id_youtube'));
		$profesi_testimoni 		= htmlspecialchars($this->input->post('profesi_testimoni'));

		$data = array(
					'nama_testimoni' => $nama_testimoni,
					'profesi_testimoni' => $profesi_testimoni,
					'link_video' => $link_id_youtube,
					'profesi_testimoni' => $profesi_testimoni
				);
		$result = $m_testimoni->edit_data_testimoni_all('tbl_kukila_testimoni', $kode_testimoni, $data);
		
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni Video berhasil di ubah.');
			redirect('admin/testimoni-video','refresh');
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat mengubah Testimoni');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_testimoni($kode_testimoni) {
		$this->load->model('Testimoni_model');
		$m_testimoni = new Testimoni_model();
		$result = $m_testimoni->delete_data_testimoni('tbl_kukila_testimoni', $kode_testimoni);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni berhasil di hapus.');
			redirect('admin/testimoni-video','refresh');
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Data testimoni tidak tersedia');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_kontak($kode_kontak) {
		$this->load->model('Kontak_model');
		$m_kontak = new Kontak_modal();

		$valid = $this->db->query("SELECT * FROM tbl_kukila_testimoni")->num_rows();
		if($valid > 0) {
			$m_kontak->delete_data_kontak('tbl_kukila_kontak', $kode_kontak);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Kontak berhasil di hapus.');
			redirect('admin/kontak','refresh');
		} else{
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Data kontak tidak tersedia');
			redirect($this->agent->referrer());
		}
	}

	/**
	
		TODO:
		- Create slug url 
		- Function
	
	 */
	
	public function action_insert_slide() {
		$this->load->model('Slide_model');
		$m_slide = new Slide_model();

		$head_text = $this->input->post('head_slide');
		$body_text = $this->input->post('body_slide');

		$remove_strip 	  = str_replace("-"," ", $head_text);
		$kata_kunci_name  = $this->slug($remove_strip);

		$file_name  = $_FILES['cover_slide']['name'];
		$error      = $_FILES['cover_slide']['error'];
		$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
		$new_file_name = date('ymd').'-'.$kata_kunci_name.'-slide.'.$file_type;

		if ($error > 0) {
			
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Gambar Tidak Boleh Kosong');
			redirect($this->agent->referrer());

		} else {

			$config['upload_path'] = './storage_img/img_slide/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '1024';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('cover_slide')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menambahkan slide');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data_slide = array(
				'deskripsi_1' => $head_text,
				'deskripsi_2' => $body_text,
				'cover_slide' => $new_file_name,
				'create_at' => date("Y-m-d H:i:s")
			);

			$m_slide->insert_data_slide('tbl_kukila_slide', $data_slide);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Slide berhasil di tambahkan.');
			redirect('admin/slide','refresh');
		}

	}

	public function action_edit_slide() {
		$this->load->model('Slide_model');
		$m_slide = new Slide_model();

		$head_text  = $this->input->post('head_text');
		$body_text  = $this->input->post('body_text');
		$kode_slide = $this->input->post('kode_slide');
		$cover_old  = $this->input->post('cover_old');

		$cover_error = $_FILES['cover_slide']['error'];

		$valid = $this->db->query("SELECT * FROM tbl_kukila_slide WHERE kode_slide = '$kode_slide'")->num_rows();
		
		if ($valid > 0) {
			if ($cover_error > 0) {

				$data_slide = array(
					'deskripsi_1' => $head_text,
					'deskripsi_2' => $body_text,
					'cover_slide' => $cover_old
				);
				$m_slide->update_data_slide('tbl_kukila_slide', $kode_slide, $data_slide);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Slide berhasil di ubah.');
				redirect('admin/slide','refresh');
			} else {
				$remove_strip 	  = str_replace("-"," ", $head_text);
				$kata_kunci_name  = $this->slug($remove_strip);

				$dir = "./storage_img/img_slide/".$cover_old;
				unlink($dir);

				$file_name  = $_FILES['cover_slide']['name'];
				$error      = $_FILES['cover_slide']['error'];
				$file_type  = pathinfo($file_name, PATHINFO_EXTENSION);
				$new_file_name = date('ymd').'-'.$kata_kunci_name.'-slide.'.$file_type;

				$config['upload_path'] = './storage_img/img_slide/';
				$config['allowed_types'] = '*';
				$config['max_size']  = '*';
				$config['file_name'] = $new_file_name;
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('cover_slide')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat mengubah slide');
					redirect($this->agent->referrer());
				}
				else {
					$data = array('upload_data' => $this->upload->data());
				}

				$data_slide = array(
					'deskripsi_1' => $head_text,
					'deskripsi_2' => $body_text,
					'cover_slide' => $new_file_name
				);

				$m_slide->update_data_slide('tbl_kukila_slide', $kode_slide, $data_slide);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Slide berhasil di ubah.');
				redirect('admin/slide','refresh');
			}
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan data slide tidak tersedia');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_slide($kode_slide) {
		$this->load->model('Slide_model');
		$m_slide = new Slide_model();

		$sql = "SELECT * FROM tbl_kukila_slide WHERE kode_slide = '$kode_slide'";
		$valid = $this->db->query($sql)->num_rows();
		if ($valid > 0) {
			$dir = "./storage_img/img_slide/";
			if (is_dir($dir)) {
				$data = $this->db->query($sql)->row();
				unlink($dir.$data->cover_slide);
				$m_slide->delete_data_slide('tbl_kukila_slide', $kode_slide);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Slide berhasil di hapus.');
				redirect('admin/slide','refresh');
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menghapus slide');
				redirect($this->agent->referrer());
			}
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menghapus slide');
			redirect($this->agent->referrer());
		}
	}
	
	public function slug($string, $space="-") {
        $string = utf8_encode($string);
        $string = preg_replace("/[^a-zA-Z0-9 \-]/", "", $string);
        $string = trim(preg_replace("/\\s+/", " ", $string));
        $string = strtolower($string);
        $string = str_replace(" ", $space, $string);

        return $string;
    }

    public function action_tambah_wilayah() {
    	$this->load->model('Wilayah_model');
    	$m_wilayah = new Wilayah_model();

    	$nama_wilayah 	= htmlspecialchars($this->input->post('nama_wilayah'));
    	$wilayah_maps 	= htmlspecialchars($this->input->post('wilayah_maps'));
    	$state 			= htmlspecialchars($this->input->post('state'));
    	$koordinat_x 	= htmlspecialchars($this->input->post('koordinat_x'));
    	$koordinat_y 	= htmlspecialchars($this->input->post('koordinat_y'));

    	$kapital = strtoupper($nama_wilayah);

    	$valid = $this->db->query("SELECT * FROM tbl_kukila_wilayah WHERE nama_wilayah = '$kapital'")->num_rows();
    	if ($valid > 0) {
    		$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Wilayah sudah tersedia');
			redirect($this->agent->referrer());
    	} else {
    		$data_wilayah = array(
    						'nama_wilayah' => strtoupper($nama_wilayah),
    						'wilayah_maps' => $wilayah_maps,
    						'state' => $state,
    						'koordinat_x' => $koordinat_x,
    						'koordinat_y' => $koordinat_y
    					);
    		$m_wilayah->insert_data_wilayah('tbl_kukila_wilayah', $data_wilayah);
	    	$this->session->set_flashdata('message_data', '<strong>Success</strong> Wilayah baru berhasil di tambahkan.');
			redirect('admin/distributor','refresh');
    	}
    }


    public function action_delete_wilayah($kode_wilayah) {
    	$this->load->model('Wilayah_model');
    	$m_wilayah = new Wilayah_model();

    	$valid = $this->db->query("SELECT * FROM tbl_kukila_wilayah WHERE kode_wilayah = '$kode_wilayah'")->num_rows();
    	if ($valid > 0) {
    		$m_wilayah->delete_data_wilayah('tbl_kukila_wilayah', $kode_wilayah);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Wilayah berhasil di hapus.');
			redirect($this->agent->referrer());
    	} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Data wilayah tidak tersedia');
			redirect($this->agent->referrer());
    	}
    }

    public function action_tambah_customer() {
    	$this->load->model('Customer_model');
    	$m_customer = new Customer_model();

    	$nama_customer  = htmlspecialchars($this->input->post('nama_customer'));
    	$posisi_wilayah = htmlspecialchars($this->input->post('posisi_wilayah'));
    	$nomor_telpon   = htmlspecialchars($this->input->post('nomor_telpon'));
    	$alamat = htmlspecialchars($this->input->post('alamat'));
    	$X 		= $this->input->post('long_x');
    	$Y 		= $this->input->post('long_y');

    	$data_depo = array(
    		'nama_customer'	=> $nama_customer,
    		'kode_wilayah' 	=> $posisi_wilayah,
    		'nomor_telpon' 	=> $nomor_telpon,
    		'alamat'		=> $alamat,
    		'X'				=> $X,
    		'Y'				=> $Y,
    		'status_cus'	=>'Active'
    	);

    	$m_customer->insert_data_depo('tbl_kukila_depo', $data_depo);
    	$this->session->set_flashdata('message_data', '<strong>Success</strong> Depo berhasil di tambahkan.');
		redirect($this->agent->referrer());
    }

    public function action_edit_customer() {
    	$this->load->model('Customer_model');
    	$m_customer = new Customer_model();

    	$kode_depo  = htmlspecialchars($this->input->post('kode_depo'));
    	$nama_customer  = htmlspecialchars($this->input->post('nama_customer'));
    	$posisi_wilayah = htmlspecialchars($this->input->post('posisi_wilayah'));
    	$nomor_telpon   = htmlspecialchars($this->input->post('nomor_telpon'));
    	$alamat = htmlspecialchars($this->input->post('alamat'));
    	$X 		= $this->input->post('long_x');
    	$Y 		= $this->input->post('long_y');

    	$valid = $this->db->query("SELECT * FROM tbl_kukila_depo WHERE kode_depo = '$kode_depo'")->num_rows();
    	if ($valid > 0) {
    		$data_depo = array(
	    		'nama_customer'	=> $nama_customer,
	    		'kode_wilayah' 	=> $posisi_wilayah,
	    		'nomor_telpon' 	=> $nomor_telpon,
	    		'alamat'		=> $alamat,
	    		'X'				=> $X,
	    		'Y'				=> $Y,
	    		'status_cus'	=>'Active'
	    	);
	    	// print_r($data_depo);
	    	$m_customer->update_data_depo('tbl_kukila_depo', $kode_depo, $data_depo);
	    	$this->session->set_flashdata('message_data', '<strong>Success</strong> Depo berhasil di update.');
			redirect($this->agent->referrer());
    	} else {
    		$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Data Depo tidak tersedia');
			redirect($this->agent->referrer());
    	}

    }


    public function action_delete_customer($kode_depo) {
    	$this->load->model('Customer_model');
    	$m_customer = new Customer_model();

    	$valid = $this->db->query("SELECT * FROM tbl_kukila_depo WHERE kode_depo = '$kode_depo'")->num_rows();
    	if ($valid > 0) {
    		$m_customer->delete_data_depo('tbl_kukila_depo', $kode_depo);
    		$this->session->set_flashdata('message_data', '<strong>Success</strong> Depo berhasil di hapus.');
			redirect($this->agent->referrer());
    	} else {
    		$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Data Depo tidak tersedia');
			redirect($this->agent->referrer());
    	}

    }
	


}

/* End of file Administrator.php */
/* Location: ./application/controllers/Administrator.php */