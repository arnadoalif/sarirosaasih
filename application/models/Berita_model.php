<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita_model extends CI_Model {

	public function view_data_berita() {
		$this->db->select('*');
		return $this->db->get('tbl_kukila_artikel');
	}

	public function view_data_berita_limit() {
		return $this->db->query("SELECT TOP 6 * FROM tbl_kukila_artikel");
	}

	public function view_data_berita_by_tag($table_name, $tag_name) {
		$this->db->select('*');
		$this->db->where('tags', $tag_name);
		return $this->db->get($table_name);
	}

	public function view_data_berita_by_type($table_name, $kategori_name) {
		$this->db->select('*');
		$this->db->where('type_artikel', $kategori_name);
		return $this->db->get($table_name);
	}

	public function view_data_by_name_berita($table_berita, $nama_berita) {
		$this->db->select('*');
		$this->db->like('nama_berita', $nama_berita);
		return $this->db->get($table_name);
	} 

	public function view_data_berita_by_slug($table_name, $slug_berita) {
		$this->db->select('*');
		$this->db->where('key_nama_artikel', $slug_berita);
		return $this->db->get($table_name);
	}

	public function input_data_berita($table_name, $data_berita) {
		$this->db->insert($table_name, $data_berita);
	}

	public function edit_data_berita($table_name, $kode_artikel, $type_artikel, $data_update) {
		$this->db->select('*');
		$this->db->where('kode_artikel', $kode_artikel);
		$this->db->where('type_artikel', $type_artikel);
		$this->db->update($table_name, $data_update);
	}

	public function delete_data_berita($table_name, $kode_artikel) {
		$this->db->select('*');
		$this->db->where('kode_artikel', $kode_artikel);
		$this->db->delete($table_name);
	}

}

/* End of file Berita_model.php */
/* Location: ./application/models/Berita_model.php */