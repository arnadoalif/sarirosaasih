<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni_model extends CI_Model {

	public function view_data_testimoni() {
		$this->db->select('*');
		$this->db->order_by('kode_testimoni', 'desc');
		return $this->db->get('tbl_kukila_testimoni', 3);

	}	

	public function view_data_testimoni_by_type($table_name, $kategori_testimoni) {
		$this->db->select('*');
		$this->db->where('kategori_testimoni', $kategori_testimoni);
		return $this->db->get($table_name);
	}


	public function input_data_testimoni($table_name, $data_testimoni) {
		$this->db->insert($table_name, $data_testimoni);
	}

	public function edit_data_testimoni_all($table_name, $kode_testimoni, $data) {
		$result = $this->db->where('kode_testimoni', $kode_testimoni);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			$this->db->where('kode_testimoni', $kode_testimoni);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

	public function edit_data_testimoni($table_name, $kode_testimoni, $kategori_testimoni, $update_data) {
		$this->db->select('*');
		$this->db->where('kode_testimoni', $kode_testimoni);
		$this->db->where('kategori_testimoni', $kategori_testimoni);
		$this->db->update($table_name, $update_data);
	}

	public function delete_data_testimoni($table_name, $kode_testimoni) {
		$result = $this->db->where('kode_testimoni', $kode_testimoni);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			$this->db->where('kode_testimoni', $kode_testimoni);
			$this->db->delete($table_name);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Testimoni_model.php */
/* Location: ./application/models/Testimoni_model.php */