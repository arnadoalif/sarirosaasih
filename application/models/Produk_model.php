<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_model extends CI_Model {

	public function view_data_produk() {
		$this->db->select('*');
		$this->db->order_by('kategori_produk', 'asc');
		return $this->db->get('tbl_kukila_produk');
	}


	public function view_data_produk_by_kode_produk($table_name, $kode_produk) {
		$this->db->select('*');
		$this->db->where('kode_produk', $kode_produk);
		return $this->db->get($table_name);
	}

	public function view_data_by_slug($table_name, $slug_url) {
		$this->db->select('*');
		$this->db->where('kata_kunci', $slug_url);
		return $this->db->get($table_name, 1);
	}

	public function view_data_produk_by_kategori($table_name, $kategori_produk) {
		$this->db->select('*');
		$this->db->where('kategori_produk', $kategori_produk);
		return $this->db->get($table_name);
	}

	public function insert_data_produk($table_name, $data_produk) {
		$this->db->insert($table_name, $data_produk);
	}

	public function edit_data_produk($table_name, $kode_produk, $where_data) {
		$this->db->select('*');
		$this->db->where('kode_produk', $kode_produk);
		$this->db->update($table_name, $where_data);
	}

	public function delete_data_produk($table_name, $kode_produk) {
		$this->db->select('*');
		$this->db->where('kode_produk', $kode_produk);
		$this->db->delete($table_name);
	}

	/**
	
		TODO:
		- insert analisa produk
	
	 */
	
	public function analisa_produk_view_all() {
		$this->db->select('*');
		$this->db->order_by('kode_analisa_produk', 'desc');
		return $this->db->get('tbl_kukila_analisa_produk');
	}

	public function analisa_produk_view_by_kode_produk($table, $kode_produk) {
		$this->db->where('kode_produk', $kode_produk);
		$this->db->select('*');
		return $this->db->get($table);
	}

	public function analisa_produk_insert_data($table, $data) {
		$this->db->insert($table, $data);
	}

	public function analisa_produk_update_data($table, $kode_analisa, $kode_produk, $where) {
		$this->db->select('*');
		$this->db->where('kode_analisa', $kode_analisa);
		$this->db->where('kode_produk', $kode_produk);
		$this->db->update($table, $where);
	}

	public function analisa_produk_delete_data($table, $kode_produk) {
		$this->db->select('*');
		$this->db->where('kode_produk', $kode_produk);
		$this->db->delete($table);
	}

}

/* End of file Produk_model.php */
/* Location: ./application/models/Produk_model.php */