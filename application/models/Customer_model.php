<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends CI_Model {

	public function view_data_customer() {
		$sql = "SELECT
					depo.kode_depo,
					wilayah.nama_wilayah,
					depo.nama_customer,
					depo.nomor_telpon,
					depo.alamat,
					depo.X,
					depo.Y 
				from tbl_kukila_depo depo , tbl_kukila_wilayah wilayah 
				where depo.kode_wilayah = wilayah.kode_wilayah order by wilayah.kode_wilayah desc";
		return $this->db->query($sql)->result();
	}

	public function view_detail_customer_by_maps($nama_wilayah, $X, $Y) {
		$wilayah = strtoupper(str_replace('-', ' ', $nama_wilayah));
		$sql = "SELECT
				wilayah.kode_wilayah,
				wilayah.nama_wilayah,
				depo.*
				from tbl_kukila_wilayah wilayah , tbl_kukila_depo depo
				where 
				wilayah.kode_wilayah = depo.kode_wilayah and wilayah.nama_wilayah = '$wilayah' and depo.X = '$X' and depo.Y = '$Y'";
		return $this->db->query($sql)->result();
	}

	public function view_detail_customer_by_wilayah($nama_wilayah, $X, $Y) {
		$wilayah = strtoupper(str_replace('-', ' ', $nama_wilayah));
		$sql = "SELECT
				wilayah.kode_wilayah,
				wilayah.nama_wilayah,
				depo.*
				from tbl_kukila_wilayah wilayah , tbl_kukila_depo depo
				where 
				wilayah.kode_wilayah = depo.kode_wilayah and wilayah.nama_wilayah = '$wilayah' and depo.X != '$X' and depo.Y != '$Y'";
		return $this->db->query($sql)->result();
	}

	public function insert_data_depo($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function update_data_depo($table_name, $kode_depo, $where) {
		$this->db->select('*');
		$this->db->where('kode_depo', $kode_depo);
		$this->db->update($table_name, $where);
	}

	public function delete_data_depo($table_name, $kode_depo) {
		$this->db->select('*');
		$this->db->where('kode_depo', $kode_depo);
		$this->db->delete($table_name);
	}

}

/* End of file Customer_model.php */
/* Location: ./application/models/Customer_model.php */