<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah_model extends CI_Model {

	public function view_data_wilayah() {
		$this->db->select('*');
		return $this->db->get('tbl_kukila_wilayah');
	}	

	public function insert_data_wilayah($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function update_data_wilayah($table_name, $kode_wilayah, $where) {
		$this->db->select('*');
		$this->db->where('kode_wilayah', $kode_wilayah);
		$this->db->update($table_name, $where);
	}

	public function delete_data_wilayah($table_name, $kode_wilayah) {
		$this->db->select('*');
		$this->db->where('kode_wilayah', $kode_wilayah);
		$this->db->delete($table_name);
	}

}

/* End of file Wilayah_model.php */
/* Location: ./application/models/Wilayah_model.php */