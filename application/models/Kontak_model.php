<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak_model extends CI_Model {

	public function view_data_kontak() {
		$this->db->select('*');
		return $this->db->get('tbl_kukila_kontak');
	}

	public function view_data_kontak_by_type($table_name, $type_pesan) {
		$this->db->select('*');
		$this->db->where('type_pesan', $type_pesan);
		return $this->db->get($table_name);
	}

	public function input_data_kontak($table_name, $data_kontak) {
		$this->db->insert($table_name, $data_kontak);
	}

	public function delete_data_kontak($table_name, $kode_kontak) {
		$this->db->select('*');
		$this->db->where('kode_kontak', $kode_kontak);
		$this->db->delete($table_name);
	}

}

/* End of file Kontak_model.php */
/* Location: ./application/models/Kontak_model.php */