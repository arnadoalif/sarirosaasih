<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slide_model extends CI_Model {

	public function view_data_slide() {
		$this->db->select('*');
                $this->db->order_by('kode_slide', 'asc');
		return $this->db->get('tbl_kukila_slide');
	}	

	public function insert_data_slide($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_data_slide($table, $kode_slide, $where) {
		$this->db->select('*');
		$this->db->where('kode_slide', $kode_slide);
		$this->db->update($table, $where);
	}

	public function delete_data_slide($table, $kode_slide) {
		$this->db->select('*');
		$this->db->where('kode_slide', $kode_slide);
		$this->db->delete($table);
	}

}

/* End of file Slide_model.php */
/* Location: ./application/models/Slide_model.php */