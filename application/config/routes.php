<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'frontpage';
$route['404_override'] = 'frontpage';
$route['translate_uri_dashes'] = FALSE;

$route['produk']				= 'frontpage/detail_puyuh_petelur_view';
$route['testimoni']				= 'frontpage/testimoni_view';
$route['news']				    = 'frontpage/news_view';
$route['news-detail/(:any)']	= 'frontpage/news_detail_view/$1';
$route['profil']				= 'frontpage/profil_view';
$route['kontak']				= 'frontpage/kontak_view';
$route['distribusi']            = 'frontpage/distribusi';
$route['galeri']            	= 'frontpage/galeri_view';
$route['distribusi-detail/(:any)/(:any)/(:any)'] = 'frontpage/detail_distribusi/$1/$2/$3';

$route['produk/puyuh-petelur']  = 'frontpage/detail_puyuh_petelur_view';
$route['produk/ayam-petelur']	= 'frontpage/detail_ayam_petelur_view';
$route['produk/ayam-pedaging']	= 'frontpage/detail_ayam_pedaging_view';
$route['produk/ayam-joper']	= 'frontpage/detail_ayam_joper_view';

$route['admin']            		 = 'administrator';
$route['admin/home']             = 'administrator';
$route['admin/produk']           = 'administrator/data_produk_view';
$route['admin/edit/produk/(:any)']		 = 'administrator/edit_data_produk/$1';
$route['admin/tambah/produk']	 = 'administrator/tambah_produk_view';

$route['admin/berita']			= 'administrator/data_berita_view';
$route['admin/berita-video']	= 'administrator/data_berita_video_view';
$route['admin/tambah/berita']	= 'administrator/tambah_berita_view';


$route['admin/testimoni']				= 'administrator/data_testimoni_view';
$route['admin/testimoni-video']  		= 'administrator/data_testimoni_video_view';
$route['admin/tambah/testimoni'] 		= 'administrator/tambah_testimoni_view';

$route['admin/kontak-pesan']	=	'administrator/data_kontak_pesan_view';
$route['admin/kontak-order']	=	'administrator/data_kontak_order_view';
$route['admin/kontak-request-pakan']	=	'administrator/data_kontak_request_pakan_view';

$route['admin/slide'] = 'administrator/data_slide_view';
$route['admin/tambah/slide'] = 'administrator/tambah_slide_view';

$route['admin/distributor'] = 'administrator/data_distributor';

