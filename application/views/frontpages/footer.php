<footer>

    <div class="footer-row">
        <div class="container">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-footer col-about">
                <div class="logo">
                    <a href="blog-detail-regular2.html"><img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png'); ?>">   
                        <div class="name">PT. SARI ROSA ASIH</div>
                        <div class="slogan">Poultry Industry in Indonesia</div>
                    </a>
                </div>
                <div class="about-us">
                    <p>
                        PT Sari Rosa Asih (PT. SRA) merupakan perusahaan yang bergerak di bidang pakan ternak. PT SRA berdiri sejak Tahun 2004 dengan alamat di Bangsan, Sindumartani, Ngemplak, Sleman, Yogyakarta. Awal berdirinya, PT. SRA fokus memproduksi Pakan Puyuh petelur untuk kalangan sendiri dengan merk KUKILA Puyuh Petelur. 
                    </p>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-footer col-link">

                <h3 class="under-line">
                    Sosial Media
                    <div class="line"></div>
                </h3>
                <div class="fb-page" data-href="https://www.facebook.com/kukilafeed/" data-tabs="timeline" data-height="200" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/kukilafeed/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/kukilafeed/">Pakan Kukila</a></blockquote></div>
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.10&appId=1351576294968656';
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-footer col-link">

                <h3 class="under-line">
                    LEGALS
                    <div class="line"></div>
                </h3>
                <ul>
                    <li><a href="<?php echo base_url(); ?>">Privacy Policy</a></li>
                    <li><a href="<?php echo base_url(); ?>">Terms & Conditions</a></li>
                    <li><a href="<?php echo base_url(); ?>">© Developers PT SRA 2018</a></li>
                </ul>

                <h3>MENU</h3>

                <ul>
                    <li><a href="<?php echo base_url('profil'); ?>">Tentang Kami</a></li>
                    <li><a href="<?php echo base_url('produk'); ?>">Produk</a></li>
                    <li><a href="<?php echo base_url('artikel'); ?>">Artikel</a></li>
                    <li><a href="<?php echo base_url('testimoni'); ?>">Testimoni</a></li>
                    <li><a href="<?php echo base_url('kontak'); ?>">Kontak</a></li>
                </ul>

            </div>
        </div>
    </div>
    <div class="footer-under">
        <div class="container">
            © 2018-2019. PT. Sari Rosa Asih  by <a href="" target="_blank">Developers PT.SRA</a>.
        </div>
    </div>
</footer>

<a id="back-to-top" href="#" class="back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left">
    <span class="glyphicon glyphicon-chevron-up"></span>
</a>

 <!-- End of footer -->