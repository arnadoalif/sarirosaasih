

<div class="home-numbers" id="kontak_sales">
  <h3 style="font-weight: 900; color: #2081c7;">PAKAI KUKILA & JATAYU SEKARANG JUGA!</h3>
  <!-- <div class="subtitle">Daftar sales yang sudah ditentukan tiap wilayah</b></div> -->
  <div class="container inner">
    <div class="row">

      <div class="col-sm-12 col-md-3 col-lg-3">
        <h2>0813-2912-3898</h2>
        <div class="photo">
          <img class="img-responsive center-img" src="<?php echo base_url('asset_front/images/sales/sales-tuhu.png'); ?> ">
        </div>
        <h3>TUHU</h3>
        <h4 style="font-weight: 900; color: #2081c7;">SALES SPV</h4>
        <p>Seluruh Wilayah Jawa</p>
        <div class="btn-group">
          <a type="button" href="https://api.whatsapp.com/send?phone=6281329123898&text=Saya%20Mau%20Order" class="btn btn-success"><i class="fa fa-whatsapp"></i> Whatsapp</a>
          <a type="button"  href="tel:081329123898" class="btn btn-danger"><i class="fa fa-phone"></i> Hubungi</a>
        </div>
      </div>

      <div class="col-sm-12 col-md-3 col-lg-3">
        <h2>0815-7882-2227</h2>
        <div class="photo">
          <img class="img-responsive center-img" src="<?php echo base_url('asset_front/images/sales/sales-aji.png'); ?> ">
        </div>
        <h3>AJI</h3>
        <h4 style="font-weight: 900; color: #2081c7;">SALES AREA 1</h4>
        <p>Jogja Timur, Karisidenan Surakarta, Karisidenan Pati</p>
        <div class="btn-group">
          <a type="button" href="https://api.whatsapp.com/send?phone=6281578822227&text=Saya%20Mau%20Order" class="btn btn-success"><i class="fa fa-whatsapp"></i> Whatsapp</a>
          <a type="button" href="tel:081578822227" class="btn btn-danger"><i class="fa fa-phone"></i> Hubungi</a>
        </div>
        
      </div>

      <div class="col-sm-12 col-md-3 col-lg-3">
        <h2>0812-2751-5262</h2>
        <div class="photo">
          <img class="img-responsive center-img" src="<?php echo base_url('asset_front/images/sales/sales-handoko.png'); ?> ">
        </div>
          <h3>HANDOKO</h3>
          <h4 style="font-weight: 900; color: #2081c7;">SALES AREA 2</h4>
          <p>Jogja Barat, Karisidenan Kedu, Karisidenan Semarang</p>

        <div class="btn-group">
          <a type="button" href="https://api.whatsapp.com/send?phone=6281227515262&text=Saya%20Mau%20Order" class="btn btn-success"><i class="fa fa-whatsapp"></i> Whatsapp</a>
          <a type="button" href="tel:081227515262" class="btn btn-danger"><i class="fa fa-phone"></i> Hubungi</a>
        </div>
      </div>

      <div class="col-sm-12 col-md-3 col-lg-3">
        <h2>0812-7957-8576</h2>
        <div class="photo">
          <img class="img-responsive center-img" src="<?php echo base_url('asset_front/images/sales/sales-arinudin.png'); ?> ">
        </div>
          <h3>ARINUDIN</h3>
          <h4 style="font-weight: 900; color: #2081c7;">SALES AREA 3</h4>
          <p>Salatiga, Semarang, Wonosobo, Banjarnegara</p>

        <div class="btn-group">
          <a type="button" href="https://api.whatsapp.com/send?phone=6281279578576&text=Saya%20Mau%20Order" class="btn btn-success"><i class="fa fa-whatsapp"></i> Whatsapp</a>
          <a type="button" href="tel:081279578576" class="btn btn-danger"><i class="fa fa-phone"></i> Hubungi</a>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- End of Home Number -->