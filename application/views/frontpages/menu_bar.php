<nav class="navbar navbar-fixed-top">
    <div class="container">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <div class="logo">
                    <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png'); ?> ">
                        <div class="name">PT. SARI ROSA ASIH</div>
                        <div class="slogan">Poultry Industry in Indonesia</div>
                    </a>
                </div>
                <div class="mobile-alert visible-xs">
                    <ul class="nav navbar-nav navbar-right">
                    </ul>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse nav-main collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown active"><a href="<?php echo base_url(''); ?>">Beranda</a></li>
                    <li class="dropdown">
                        <a href="<?php echo base_url('produk'); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Produk<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('produk/puyuh-petelur'); ?>">Puyuh Petelur</a></li>
                            <li><a href="<?php echo base_url('produk/ayam-petelur'); ?>">Ayam Petelur</a></li>
                            <li><a href="<?php echo base_url('produk/ayam-pedaging'); ?>">Ayam Pedaging</a></li>
                            <li><a href="<?php echo base_url('produk/ayam-joper'); ?>">Ayam Joper / Pejantan</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url('news'); ?>">Artikel</a></li>
                    <li><a href="<?php echo base_url('distribusi'); ?>">Distribusi</a></li>
                    <li><a href="<?php echo base_url('galeri'); ?>">Galeri</a></li>
                    <!-- <li><a href="<?php echo base_url('testimoni'); ?>">Testimoni</a></li> -->
                    <li><a href="<?php echo base_url('kontak'); ?>">Kontak</a></li>
                    <li><a href="<?php echo base_url('profil'); ?>">Tentang Kami</a></li>


                    

                   <!--  <li class="dropdown mega-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blogs <span class="caret"></span></a>
                        <div class="dropdown-menu mega-dropdown-menu">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 blog-menu">
                                        <h4 class="under-line">
                                        Blogs
                                        <div class="line"></div>
                                        </h4>
                                        <ul class="short-menu">
                                            <li><a href="blog-category.html">Categories</a></li>
                                            <li><a href="blog-category-3cols.html">Categories 3 columns</a></li>
                                            <li><a href="blog-detail-regular.html">Detail + widget</a></li>
                                            <li><a href="blog-detail-regular2.html">Detail full + widget</a></li>
                                            <li><a href="blog-detail-full.html">Detail - no widget</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 widget-features">
                                        <ul>
                                            <li class="bigger">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a href="blog-detail-regular2.html">
                                                            <img class="media-object" src="<?php echo base_url('asset_front/images/pages/welcome1.jpg'); ?> " alt="...">
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                        <a href="blog-detail-regular2.html">Cras sit amet nibh libero, in gravida nulla</a>
                                                        </h4>
                                                        <div class="info">
                                                            <div class="date">
                                                                March 9, 2017
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 widget-features">
                                        <ul>
                                            <li>
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a href="blog-detail-regular2.html">
                                                            <img class="media-object" src="<?php echo base_url('asset_front/images/pages/welcome1.jpg'); ?>" alt="...">
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                        <a href="blog-detail-regular2.html">Cras sit amet nibh libero, in gravida nulla</a>
                                                        </h4>
                                                        <div class="info">
                                                            <div class="date">
                                                                March 9, 2017
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a href="blog-detail-regular2.html">
                                                            <img class="media-object" src="<?php echo base_url('asset_front/images/pages/welcome2.jpg'); ?>" alt="...">
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                        <a href="blog-detail-regular2.html">Neque porro quisquam est, qui dolorem</a>
                                                        </h4>
                                                        <div class="info">
                                                            <div class="date">
                                                                March 9, 2017
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a href="blog-detail-regular2.html">
                                                            <img class="media-object" src="<?php echo base_url('asset_front/images/pages/welcome3.jpg'); ?>" alt="...">
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                        <a href="blog-detail-regular2.html">Sed quia non numquam eius modi tempora</a>
                                                        </h4>
                                                        <div class="info">
                                                            <div class="date">
                                                                March 9, 2017
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown mega-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
                        <div class="dropdown-menu mega-dropdown-menu">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <h4 class="under-line">
                                        About us
                                        <div class="line"></div>
                                        </h4>
                                        <ul class="short-menu">
                                            <li><a href="page-about-us.html">Overview</a></li>
                                            <li><a href="page-company-history.html">Company History</a></li>
                                            <li><a href="page-case-studies.html">Case studies</a></li>
                                            <li><a href="page-faqs.html">FAQs</a></li>
                                            <li class="short-menu-header">Authentication</li>
                                            <li><a href="page-signin.html">Sign-in (both login + create new account)</a></li>
                                            <li><a href="page-login.html">Login</a></li>
                                            <li><a href="page-register.html">Create new account</a></li>
                                            <li class="short-menu-header">Logined</li>
                                            <li><a href="page-profile.html">Profile</a></li>
                                            <li><a href="page-alerts.html">Alerts</a></li>
                                            <li><a href="page-security.html">Security</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <h4 class="under-line">
                                        Others
                                        <div class="line"></div>
                                        </h4>
                                        <ul class="short-menu">
                                            <li><a href="page-blank.html">Blank page</a></li>
                                            <li><a href="page-portfolios.html">Isotope Portfolios</a></li>
                                            <li class="short-menu-header">Typography</li>
                                            <li><a href="page-typography.html">Heading, Font, Blockquote & Dropcaps..</a></li>
                                            <li class="short-menu-header">Searching</li>
                                            <li><a href="blog-search.html">Search with results list</a></li>
                                            <li><a href="blog-search-noresult.html">Search without</a></li>
                                            <li class="short-menu-header">Errors</li>
                                            <li><a href="page-404.html">404: Page Not Found</a></li>
                                            <li><a href="page-403.html">403: Forbidden</a></li>
                                            <li><a href="page-500.html">500: Internal Server Error</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <h4 class="under-line">
                                        Mobile
                                        <div class="line"></div>
                                        </h4>
                                        <ul class="short-menu">
                                            <li><a href="page-mobile-bottom-nav-menu.html">Bottom navigation menu</a></li>
                                            <li><a href="page-mobile-floatbutton.html">Float button</a></li>
                                            <li class="short-menu-header">More</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li> -->

                    <!-- <li><a href="page-contact.html">Contact</a></li> -->

                    <li class="dropdown dropdown-not-auto mega-dropdown menu-alert">
                        <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell-o" aria-hidden="true"></i>
                            <span class="badge">4</span>
                        </a> -->
                        <div class="dropdown-menu mega-dropdown-menu">
                            <div class="widget-features">
                                <ul class="menu-alert-message">
                                    <li>
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="page-alerts-detail.html">
                                                    <img class="media-object img-circle" src="<?php echo base_url('asset_front/images/face/face1.jpg'); ?>" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                <a href="page-alerts-detail.html">Cras sit amet nibh libero, in gravida nulla</a>
                                                </h4>
                                                <div class="info">
                                                    <div class="date">
                                                        March 9, 2017
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <!-- <li class="li-signin">
                        <a href="page-signin.html"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Sign in</a>
                    </li> -->

                    <li class="li-search">
                        <form data-toggle="validator" action="<?php echo base_url('frontpage/search_news'); ?>" method="GET">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control input-sm" placeholder="Search in website" id="txtSearch" />
                                <div class="input-group-btn">
                                    <button class="btn btn-primary btn-sm" type="submit">
                                    Go
                                    </button>
                                </div>
                            </div>
                        </form>
                    </li>
                </ul>
                </div><!--/.nav-collapse -->
            </div>
            </div><!--/.container -->
        </nav>

        <script type="text/javascript">
            // $(document).ready(function() {
            //     $(".menu-alert").hide();
            // });
        </script>