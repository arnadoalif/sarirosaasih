<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
        <style type="text/css">
          @media (max-width: 767px) {
              .detail-header h1 {
                  font-size: 45px;
                  top: 66px;
                  position: relative;
              }
            }
        </style>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>
        </header>
        <!-- End of Header -->

        <div class="detail-header detail-header-aboutus">
            <div class="container">
                <h1>DISTRIBUSI</h1>

            </div>
            <div class="background bg-m hidden-sm" style="height: 266px; left: -48px; background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5)), url(./asset_front/images/asset_produk/puyuh_petelur/distribusi.png);"></div>
            <div class="background bg-m hidden-lg" style="height: 266px; left: -48px; background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5)), url(./asset_front/images/asset_produk/puyuh_petelur/distribusi_mobile.jpg)no-repeat; background-size: cover;"></div>
        </div>

        <main>
            
            <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="content-begin">

                    <div class="col-md-5 col-md-offset-4">
                      <h1>DAFTAR POULTRY SHOP</h1>
                    </div>

                      
                      <div class="row">
                        <div class="list-address">
                        <?php foreach ($data_customer as $dt_customer): ?>
                          <div class="col-md-12 col-lg-4">
                            <div class="addrs">
                                <p>
                                  <?php if ($dt_customer->nama_wilayah == "BLITAR"): ?>
                                    <?php echo "JAWA TIMUR" ?>
                                  <?php else: ?>
                                    <?php echo $dt_customer->nama_wilayah ?>
                                  <?php endif ?>
                                  
                                  <br><strong><?php echo $dt_customer->nama_customer ?></strong><br>
                                <?php echo $dt_customer->alamat ?> - <?php echo $dt_customer->nama_wilayah ?><br>
                                <a href="<?php echo base_url('distribusi-detail/'.strtolower(str_replace(' ', '-', $dt_customer->nama_wilayah)).'/'.$dt_customer->X.'/'.$dt_customer->Y); ?>"><i class="fa fa-location-arrow"></i> Lihat Detail Lokasi</a>
                                <br>Telpon  : <a href="tel:<?php echo $dt_customer->nomor_telpon ?>" title="Telpon Sekarang"><?php echo $dt_customer->nomor_telpon ?></a>  
                                </p>
                            </div>
                            <hr>
                          </div>
                          
                        <?php endforeach ?>
                        </div>
                      </div>
                      
                  </div>
              </div>
            </div>

        </main>
        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>