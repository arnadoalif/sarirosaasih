<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
    </head>
    <style type="text/css">
        #map-canvas {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>

            <div class="detail-header detail-header-contact">
                <div id="map-canvas" style="width: 50%; height: 100%;float:left"></div>
                <div id="pano" style="width: 50%; height: 100%;float:left"></div>
                <!-- <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=Feedmill%20PT.%20Sari%20Rosa%20Asih&key=AIzaSyDCP31oYi_A7Kj0ceQEAS4rtWCfZ8JwnbM" allowfullscreen></iframe> -->
            </div>
        </header>
        <!-- End of Header -->
        <main>
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="detail-content">
                       <div class="detail-address">
                            <?php foreach ($data_detail_customer as $dt_detail_customer): ?>
                              <div class="col-md-12 col-lg-4">
                                <div class="addrs">
                                    <h3><strong><?php echo $dt_detail_customer->nama_customer ?></strong></h3><br>
                                    <p>
                                    <?php echo $dt_detail_customer->alamat ?> - <?php echo $dt_detail_customer->nama_wilayah ?><br>
                                    <a href="http://www.google.com/maps/place/<?php echo $dt_detail_customer->X.",".$dt_detail_customer->Y ?>" target="_blank">
                                        <i class="fa fa-location-arrow"></i> Lihat Detail Lokasi</a>
                                    <br> 
                                     Telpon  : <a href="tel:<?php echo $dt_detail_customer->nomor_telpon ?>" title="Telpon Sekarang"><?php echo $dt_detail_customer->nomor_telpon ?></a>
                                    </p>
                                </div>
                                <hr>
                              </div>
                              
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2>POULTRY WILAYAH <?php echo $nama_wilayah  ?></h2>
                
                    <div class="detail-address">
                        <?php foreach ($data_customer_wilayah as $dt_cus_wilayah): ?>
                            <div class="col-md-12 col-lg-4">
                                <div class="addrs">
                                    <p><strong><?php echo $dt_cus_wilayah->nama_customer ?></strong><br>
                                    <?php echo $dt_cus_wilayah->alamat ?> - <?php echo $dt_cus_wilayah->nama_wilayah ?><br>
                                    <a href="<?php echo base_url('distribusi-detail/'.strtolower(str_replace(' ', '-', $dt_cus_wilayah->nama_wilayah)).'/'.$dt_cus_wilayah->X.'/'.$dt_cus_wilayah->Y); ?>"><i class="fa fa-location-arrow"></i> Lihat Detail Lokasi</a>
                                    <br> 
                                    Telpon  : <a href="tel:<?php echo $dt_cus_wilayah->nomor_telpon ?>" title="Telpon Sekarang"><?php echo $dt_cus_wilayah->nomor_telpon ?>
                                    </p>
                                </div>
                            <hr>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </main>
        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
    
    <script type="text/javascript">
      var lat = "<?php echo $data_detail_customer[0]->X ?>";
      var lang = "<?php echo $data_detail_customer[0]->Y ?>";

      function initialize() {

        var baltimore = new google.maps.LatLng(lat, lang);
        var baltimore1 = new google.maps.LatLng(lat, lang);

        console.log("OKE "+ baltimore);
        var mapOptions = {
          center: baltimore,
          zoom: 18
        };
        var map = new google.maps.Map(
            document.getElementById('map-canvas'), mapOptions);
          var cafeMarker = new google.maps.Marker({
            position: baltimore1,
            map: map,
            icon: '<?php echo base_url('asset_front/images/marker.png'); ?>',
            title: 'Poultry'
        });

          console.log(map);

        var panoramaOptions = {
          position: baltimore,
          pov: {
            heading: 34,
            pitch: 10
          }
        };
        var panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'), panoramaOptions);
        map.setStreetView(panorama);
      }

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</html>