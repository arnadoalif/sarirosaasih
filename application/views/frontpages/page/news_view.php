<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>

            <div class="detail-header detail-header-blog-category">
                <div class="container">
                    <h1>Artikel</h1>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active">Artikel</a></li>
                    </ol>
                </div>
                <div class="background"></div>
            </div>
        </header>
        <!-- End of Header -->
        
            <main>
                <div class="clearfix"></div>

                <div class="blog-category-1">
                    <div class="container">
                        <h3 class="under-line">
                            <a href="">Artikel </a>
                            <div class="subtitle">
                                <!-- Quisque a eleifend velit. Nullam pellentesque -->
                            </div>
                            <div class="line"></div>
                        </h3>
                        <div class="row">
                            <?php foreach ($data_artikel as $dt_artikel): ?>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="blog-item">
                                        
                                        <?php if ($dt_artikel->type_artikel == "Video"): ?>
                                            <div class="photo">
                                                <iframe class="media-video" width="100%" src="https://www.youtube.com/embed/<?php echo $dt_artikel->link_video; ?>" frameborder="0" allowfullscreen style="height: 220px;"></iframe>
                                            </div>
                                        <?php else: ?>
                                            <div class="photo">
                                                <a href=""><img src="<?php echo base_url('./storage_img/img_berita/'.$dt_artikel->cover_artikel); ?>"></a>
                                               
                                            </div>
                                        <?php endif ?>

                                        <h3><a href="<?php echo base_url('news-detail/'.$dt_artikel->key_nama_artikel); ?>"><?php echo $dt_artikel->nama_artikel ?></a></h3>
                                        <div class="info">
                                            <span class="date"><i class="fa fa-calendar"></i> <?php echo date("d/m/Y H:i", strtotime($dt_artikel->create_at)); ?>. <a href="<?php echo base_url('news-detail/'.$dt_artikel->key_nama_artikel); ?>"><i class="fa fa-user"></i> <?php echo $dt_artikel->create_by ?></a>.</span>
                                            
                                        </div>

                                        <div class="description">
                                          
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </main>

        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>