﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="description" content="<?php echo $data_berita[0]->nama_artikel." ". substr(strip_tags($data_berita[0]->isi_artikel),0, 760); ?>....">
        <meta name="author" content="pakan ternak pakan kukila">
        <meta name="keywords" content="<?php echo $data_berita[0]->tags ?>" />

        <meta property="og:url"           content="<?php echo current_url(); ?>" />
        <meta property="og:type"          content="Sari Rosa Asih" />
        <meta property="og:title"         content="<?php echo $data_berita[0]->nama_artikel ?>" />
        <meta property="og:description"   content="<?php echo substr(strip_tags($data_berita[0]->isi_artikel),0, 760); ?>....." />

        <?php if (!empty($data_berita[0]->cover_artikel)): ?>
            <meta property="og:image"         content="<?php echo base_url('storage_img/img_berita'.$data_berita[0]->cover_artikel)?>" />
        <?php else: ?>
            <meta property="og:image"         content="<?php echo base_url('storage_img/img_berita/default.png')?>" />
        <?php endif ?>

        <?php require_once(APPPATH .'views/include/front/inc_style_artikel.php'); ?>
        <style type="text/css">
            .video_type {
                height: 500px;
            }

		@media (max-width: 767px) {
		.detail-header .breadcrumb {
    			padding-top: 0px;
    			font-size: 12px;
    			padding-bottom: 0px;
    			margin-top: -30px;
    			position: relative;
			
		}

		.m-title h1 {
    			font-size: 20px;
    			top: -111px !important;
    			position: relative;
		}

		.detail-info {
			top: -15px;
    			position: relative;
		}

		.detail-info a {
   			 color: #ffffff;
		}

		.detail-sharing {
    			padding-bottom: 0px;
    			position: relative;
    			margin-top: -25px;
		}

		}
        </style>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>

            <div class="detail-header detail-header-blogs-detail-full">
                <div class="container">
		   
                   <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a style="color: #0f0101;" href="<?php echo base_url(''); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a style="color: #0f0101;" href="<?php echo base_url('news') ?>">Artikel</a></li>                        
                        <li class="breadcrumb-item active"><a style="color: #0f0101;" href="<?php echo base_url('news') ?>"><?php echo $data_berita[0]->nama_artikel ?></a></li>                        
                    </ol>
                    <div class="detail-title m-title">
                        <h1><?php echo $data_berita[0]->nama_artikel ?></h1>
                    </div>
                </div>
                <div class="background" style="background: #2081c7;"></div>
            </div>
        </header>
        <!-- End of Header -->
        <main>
            
            <div class="container">
                <div class="detail-topinfo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="detail-info">
                        <span class="date">by <a href=""><?php echo $data_berita[0]->create_by ?></a> <?php echo date("M d, Y H", strtotime($data_berita[0]->create_at)); ?></span>
                        <span class="category">
                            <a href="">#<?php echo $data_berita[0]->kategori_artikel ?></a>
                        </span>
                    </div>
                   

                    <div class="detail-sharing">
                        <?php if (!empty($data_berita[0]->cover_artikel)): ?>
                            <a href="javascript:fbShare('<?php echo current_url(); ?>', 'Fb Share', 'Facebook share popup', '<?php echo base_url('storage_img/img_berita/'.$data_berita[0]->cover_artikel)?>', 520, 350)"> <i class="fa fa-facebook"></i></a>
                        <?php else: ?>
                            <a href="javascript:fbShare('<?php echo current_url(); ?>', 'Fb Share', 'Facebook share popup', '<?php echo base_url('storage_img/img_berita/'.$data_berita[0]->cover_artikel)?>', 520, 350)"><i class="fa fa-facebook"></i></a>
                        <?php endif ?>

                        <a href="https://plus.google.com/share?url=<?php echo current_url(); ?>" class="fa fa-google"></a>
                    </div>
                </div>

                <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blogs-content blogs-content-detail">

                    <div class="feature">

                        <?php if ($data_berita[0]->type_artikel == "Video"): ?>
                            
                            <div class="detail-content textalign">
                                <div class="image-box">
                                    <div class="photo video_type">
                                        <iframe class="media-video" width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $data_berita[0]->link_video ?>" frameborder="0" allowfullscreen=""></iframe>
                                    </div>
                                    <div class="desc"><?php echo $data_berita[0]->nama_artikel ?></div>
                                </div>

                                <p>
                                    <?php echo $data_berita[0]->isi_artikel ?>
                                </p>

                            </div>
                            

                        <?php else: ?>
                            
                        <div class="detail-content textalign">
                            <div class="image-box image-box-50-left">
                                <a target="_blank" href="images/blogs/blog2.jpg" data-lightbox="roadtrip" data-title="Quisque sit amet justo semper. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus">
                                    <img src="<?php echo base_url('./storage_img/img_berita/'.$data_berita[0]->cover_artikel); ?>" alt="Forest" >
                                </a>
                                <div class="desc"><?php echo $data_berita[0]->nama_artikel ?></div>
                            </div>

                            <p>
                                <?php echo $data_berita[0]->isi_artikel ?>
                            </p>

                        </div>

                        <?php endif ?>

                        <div class="detail-author">

                            <fieldset>
                                <legend>Author: <a href=""><?php echo ucwords($data_berita[0]->create_by); ?></a></legend>
                            </fieldset>

                        </div>
                    </div>

                    <div class="related-blogs">
                        <h3>Artikel Lainnya</h3>

                        <div class="posts row">
                            <?php 
                                $kategori_artikel = $data_berita[0]->kategori_artikel;
                                $key_nama_artikel = $data_berita[0]->key_nama_artikel;
                                $berita_kategori = $this->db->query("SELECT TOP 3 * FROM tbl_kukila_artikel WHERE kategori_artikel = '$kategori_artikel' AND key_nama_artikel != '$key_nama_artikel'")->result();
                             ?>

                             <?php foreach ($berita_kategori as $dt_berita_kategori): ?>    
                             
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item">
                                    
                                    <?php if ($dt_berita_kategori->type_artikel == "Video"): ?>
                                        <div class="photo">
                                            <iframe class="media-video" width="100%" src="https://www.youtube.com/embed/<?php echo $dt_berita_kategori->link_video; ?>" frameborder="0" allowfullscreen style="height: 150px;"></iframe>
                                        </div>
                                    <?php else: ?>
                                        <div class="article-img hovereffect">
                                            <a href=""><img src="<?php echo base_url('./storage_img/img_berita/'.$dt_berita_kategori->cover_artikel); ?>"></a>
                                            <div class="overlay">
                                                <a class="info" href="<?php echo base_url('news-detail/'.$dt_berita_kategori->key_nama_artikel); ?>"><i class="fa fa-arrow-right" aria-hidden="true"></i> click to show detail</a>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    
                                    <h3><a href=""><?php echo $dt_berita_kategori->nama_artikel ?></a></h3>
                                    <div class="info">
                                        <span class="date">by <a href=""><?php echo ucwords($dt_berita_kategori->create_by); ?></a> <?php echo date("M d, Y H", strtotime($dt_berita_kategori->create_at)); ?>.</span>
                                    </div>

                                    <!-- <div class="description">
                                        Dros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…
                                    </div> -->
                                </div>

                                <?php endforeach ?>

                            
                        </div>
                    </div>

                    <!-- <ul class="pagination pagination-lazyload">
                        <li>
                            <a href="#">Load more posts <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                        </li>
                    </ul> -->
                    <!-- End of related posted -->           
                </article>

                <!-- <aside class="col-lg-4 col-md-4 col-sm-4 col-xs-12">                
                    <div class="panel panel-default panel-noborder widget-categories">
                        <div class="panel-heading">
                            Categories
                        </div>
                        <div class="panel-body">
                            <ul>
                                <li><i class="fa fa-angle-right"></i> <a href="<?php echo base_url('news') ?>">Tips & Trik<span class="badge">0</span></a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="<?php echo base_url('news') ?>">Kesehatan <span class="badge">0</span></a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="<?php echo base_url('news') ?>">Berita <span class="badge">0</span></a></li>
                                <li><i class="fa fa-angle-right"></i> <a href="<?php echo base_url('news') ?>">Video (Tutorial Web)<span class="badge">0</span></a></li>
                            </ul>
                        </div>
                    </div>                
                                                
                    <div class="widget-search">
                        <form data-toggle="validator" action="" method="GET"> 
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search in website" id="txtSearch" required="">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">
                                        Go
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>    
                </aside> -->

            </div>

        </main>
        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
    <script type="text/javascript">
         function genericSocialShare(url){
         window.open(url,'sharer','toolbar=0,status=0,width=648,height=395');
         return true;
        }
    </script>
    <script>
        function fbShare(url, title, descr, image, winWidth, winHeight) {
            var winTop = (screen.height / 2) - (winHeight / 2);
            var winLeft = (screen.width / 2) - (winWidth / 2);
            window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }

        $(document).ready(function() {
            /**
               * jQuery function to prevent default anchor event and take the href * and the title to make a share popup
               *
               * @param  {[object]} e           [Mouse event]
               * @param  {[integer]} intWidth   [Popup width defalut 500]
               * @param  {[integer]} intHeight  [Popup height defalut 400]
               * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
               */
              $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
                    
                // Prevent default anchor event
                e.preventDefault();
                
                // Set values for window
                intWidth = intWidth || '500';
                intHeight = intHeight || '400';
                strResize = (blnResize ? 'yes' : 'no');

                // Set title and open popup with focus on it
                var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                    strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,            
                    objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
              }
              
              /* ================================================== */
              
              $(document).ready(function ($) {
                $('.customer.share').on("click", function(e) {
                  $(this).customerPopup(e);
                });
              });
         });
    </script>
</html>