﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>

            <div class="detail-header detail-header-aboutus">
                <!-- <div class="container">
                    <h1>Profil Perusahaan </h1>

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active">Profil</li>
                    </ol>
                </div> -->
                <div class="background bg1"></div>
            </div>
        </header>
        <!-- End of Header -->
        <main>
            
            <div class="detail-welcome3 textalign">
                <div class="container">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <img src="<?php echo base_url('/asset_front/images/asset_produk/puyuh_petelur/logo_sra.png'); ?>" class="img-responsive img-profil" alt="Image">

                        <h1 align="center">PROFIL</h1>

                        
                        <div class="profil-desktipsi">
                            <p>
                                PT Sari Rosa Asih (PT. SRA) merupakan perusahaan yang bergerak di bidang pakan ternak. PT SRA berdiri sejak Tahun 2004 dengan alamat di Bangsan, Sindumartani, Ngemplak, Sleman, Yogyakarta. Awal berdirinya, PT. SRA fokus memproduksi Pakan Puyuh petelur untuk kalangan sendiri dengan merk KUKILA Puyuh Petelur. 
                            </p>

                            <p>
                                Membaca peluang pasar serta semakin berkembangnya tim R&D PT. SRA, di tahun 2006 PT SRA meluncurkan varian produk KUKILA untuk Ayam, yaitu Kukila Ayam Petelur dan Kukila Ayam Pedaging . Pakan KUKILA merupakan Produk Pakan Premium dari PT Sari Rosa Asih.
                            </p>

                            <p>
                                Permintaan pasar akan produk pakan yang terjangkau dan berkualitas, membuat PT SRA kembali meluncurkan produk baru dengan merk  “JATAYU” pada tahun 2014. Jatayu memiliki dua varian, yaitu Jatayu Puyuh Petelur dan Jatayu Broiler Finisher. Jatayu hadir sebagai alternatif pakan unggas dengan formula terbaik dan harga lebih terjangkau.
                            </p>

                            <p>
                                Pakan KUKILA dan JATAYU telah resmi mendapatkan SNI dan telah terdistribusikan di kota besar seluruh pulau Jawa. Saat ini PT SRA sedang fokus untuk memasarkan produk-produknya di luar Pulau Jawa.
                            </p>

                            <p>
                                PT SRA berkomitmen dan terus berinovasi secara berkesinambungan, untuk menyediakan pakan berkualitas untuk seluruh peternak, sehingga dapat memberikan nilai tambah ekonomis dan meningkatkan kesejahteraan peternak Indonesia.
                            </p>

                            <div class="visi-misi">
                                <h2 align="center">VISI</h2>
                                <p>"Menyediakan Pakan Terak Yang Unggul DanTerjangkau Semua Lapisan Masyarakat."</p>

                                <h2 align="center">MISI</h2>
                                <p>"Meningkatkan Mutu Dan Inovasi Dalam Produk Pakan Ternak, Sehingga Dapat Memberikan Nilai Tambah Ekonomis Pada Ternak."</p>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div> 

            <?php $this->load->view('frontpages/kontak_sales'); ?>


        </main>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>