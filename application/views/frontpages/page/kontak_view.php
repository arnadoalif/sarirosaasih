<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>

            <div class="detail-header detail-header-contact">
                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3865.1038104496224!2d-73.99419406545111!3d40.74992589761114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1506178277233" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                <div id="map-canvas" style="width: 100%; height: 100%;float:left"></div>

                <!-- <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=Feedmill%20PT.%20Sari%20Rosa%20Asih&key=AIzaSyDgvGSY9N6atrir3pIvDL_k5n2-QVhXpOo" allowfullscreen></iframe> -->
            </div>
        </header>
        <!-- End of Header -->
        <main>

        <div class="container">
            <div class="home-contact">
                <div class="container content">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-left">
                        <h3>Kontak Perusahaan</h3>
                        <div class="media">
                            <div class="media-left media-middle">
                                <a href="https://www.google.com/maps/place/PT+Sari+Rosa+Asih/@-7.7086257,110.4746402,15z/data=!4m2!3m1!1s0x0:0x5ae603be0f12ab2a?sa=X&ved=0ahUKEwilrOiY6cLaAhVMRo8KHW8fAoIQ_BIIeTAP" target="_blank">
                                    <img src="<?php echo base_url('asset_front/images/icon-address.png'); ?>">
                                </a>
                            </div>
                            <div class="media-body">
                                <div class="description-kontak">
                                    Bangsan, Sindumartani, Ngemplak, Sleman, Yogyakarta
                                </div>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left media-middle">
                                <a href="#">
                                    <img src="<?php echo base_url('asset_front/images/icon-phone.png'); ?>">
                                </a>
                            </div>
                            <div class="media-body">
                                <div class="description-kontak">
                                    Telp. (0274) 2850062 <br>
                                    HOTLINE. 0811 2653 774
                                </div>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left media-middle">
                                <a href="#">
                                    <img src="<?php echo base_url('asset_front/images/icon-email.png'); ?>">
                                </a>
                            </div>
                            <div class="media-body">
                                <div class="description-kontak">
                                    <a href="mailto:pakankukila@gmail.com?Subject=Kontak%20Pakan%20Kukila">pakankukila@gmail.com</a>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="media">
                            <div class="media-left media-middle">
                                <a href="#">
                                    <img src="<?php echo base_url('asset_front/images/icon-skype.png'); ?>">
                                </a>
                            </div>
                            <div class="media-body">
                                <div class="description">
                                    <a href="">khoipng</a><br>
                                    Online 27/7
                                </div>
                            </div>
                        </div> -->

                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-right">
                        <?php if (isset($_SESSION['message_data'])): ?>
                            <div class="alert alert-success margin-bottom-30" role="alert">
                              <button type="button" class="close" data-dismiss="alert">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                              </button>
                              <?php echo $_SESSION['message_data'] ?>
                            </div>
                          <?php endif ?>

                          <?php if (isset($_SESSION['error_data'])): ?>
                            <div class="alert alert-danger margin-bottom-30" role="alert">
                              <button type="button" class="close" data-dismiss="alert">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                              </button>
                              <?php echo $_SESSION['error_data'] ?>
                            </div>
                          <?php endif ?>
                          
                        <form data-toggle="validator" action="<?php echo base_url('frontpage/action_kontak'); ?>" method="POST"> 
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input class="form-control input-lg" name="nama_pesan" id="inputsm" type="text" placeholder="Nama" required="">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input class="form-control input-lg" name="email_address" id="inputsm" type="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="form-control input-lg" name="nomor_telepon" id="inputsm" type="text" placeholder="Nomor Telepon" required="">
                            </div>
                           
                            <div class="form-group">
                                <select name="type_pesan" id="type_pesan" class="form-control input-lg" required="required">
                                    <option value="" selected>-- Bentuk Pesan -- </option>
                                    <option value="kritik_saran">Kritik & Saran</option>
                                    <option value="order_pakan">Order Pakan</option>
                                    <option value="request_pakan">Request Pakan</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input class="form-control input-lg" name="alamat" id="alamat" type="text" placeholder="Alamat" required="">
                            </div>

                            <div class="form-group">
                                <input class="form-control input-lg order" name="wilayah" id="inputsm" type="text" placeholder="Wilayah">
                            </div>
                            
                            <div class="form-group">
                                <input class="form-control input-lg request" name="nama_pakan" id="inputsm" type="text" placeholder="Apa Nama Pakan Yang Di Request ?">
                            </div>

                            <div class="form-group">
                                <textarea class="form-control input-lg" id="isi_pesan" name="isi_pesan" rows="6" placeholder="Tuliskan Pesan" required=""></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-block btn-primary btn-lg" type="submit">
                                    KIRIM PESAN
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
            
        </main>
        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
    <!-- <script src="https://maps.google.com/maps/api/js"></script> -->
   
    <script type="text/javascript">
        $(document).ready(function() {
            $(".order").hide();
            $(".request").hide();
            $("#alamat").hide();
            $("#isi_pesan").hide();

            $('#type_pesan').on('change',function(){
                if( $(this).val()==="kritik_saran"){
                    $("#isi_pesan").val("");
                    $("#alamat").show();
                    $("#isi_pesan").show();
                    $(".order").removeAttr("required");
                    $(".request").removeAttr("required");
                    $(".order").hide();
                    $(".request").hide();
                    $("#isi_pesan").attr("placeholder", "Tuliskan Pesan").blur();
                    $("#alamat").attr("placeholder", "Alamat Anda").blur();
                } else if ($(this).val() === "order_pakan") {
                    $("#alamat").show();
                    $("#isi_pesan").show();
                    $(".order").show();
                    $("#isi_pesan").val("");
                    $(".request").hide();
                    $(".order").attr("required", true);
                    $("#isi_pesan").attr("placeholder", "Tuliskan Pakan Yang Di Pesan Dan Jumlah").blur();
                     $("#alamat").attr("placeholder", "Tuliskan Alamat Lengkap Anda").blur();
                } else if($(this).val() === "request_pakan") {
                    $("#alamat").show();
                    $("#isi_pesan").show();
                    $("#isi_pesan").val("");
                    $(".order").hide();
                    $(".request").show();
                    $(".request").attr("required", true);
                    $("#isi_pesan").attr("placeholder", "Keterangan Request Pakan").blur();
                    $("#alamat").attr("placeholder", "Alamat Anda").blur();
                } else if ($(this).val() === "") {
                    $("#alamat").hide();
                    $("#isi_pesan").hide();
                    $("#alamat").val("");
                    $("#isi_pesan").val("");
                }


            });
        });
    </script>
    <script type="text/javascript">
          var lat = "-7.7086204";
          var lang = "110.4724515";

          function initialize() {

            var baltimore = new google.maps.LatLng(lat, lang);
            var baltimore1 = new google.maps.LatLng(lat, lang);

            var mapOptions = {
              center: baltimore,
              zoom: 18
            };
            var map = new google.maps.Map(
                document.getElementById('map-canvas'), mapOptions);
                var cafeMarker = new google.maps.Marker({
                position: baltimore1,
                map: map,
                icon: '',
                title: 'PT Sari Rosa Asih'
            });
            
            var contentString = "<b>PT Sari Rosa Asih</b> <br> Bangsan, Sindumartani, Ngemplak, Sleman, Yogyakarta <br> Email : <a href='mailto:pakankukila@gmail.co?subject=feedback'><b>pakankukila@gmail.com</b></a><br> Kontak Kantor & Hotline : <b> (0274) 2850062  | <a href='https://api.whatsapp.com/send?phone=628112653774' title='Whatsapp' target='_blank'>0811-2653-774</a> </b>";

            var infowindow = new google.maps.InfoWindow({
              content: contentString
            });

            cafeMarker.addListener('click', function() {
              infowindow.open(map, cafeMarker);
            });

            var panoramaOptions = {
              position: baltimore,
              pov: {
                heading: 34,
                pitch: 10
              }
            };
            var panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'), panoramaOptions);
            map.setStreetView(panorama);
          }

          google.maps.event.addDomListener(window, 'load', initialize);
        </script>
</html>