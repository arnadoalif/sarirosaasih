<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>

    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>
        </header>
        <!-- End of Header -->

        <div class="detail-header detail-header-blogs-detail-full">
            <div class="container">
                <div class="detail-title">
                    <h1></h1>
                </div>
            </div>
            <div class="background bg-m hidden-xs" style="left: -48px; background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5)), url(../asset_front/images/asset_produk/puyuh_petelur/header_puyuh_petelur.png);"></div>
            <div class="background bg-m hidden-lg" style="left: -30px; background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5)), url(../asset_front/images/asset_produk/puyuh_petelur/header_puyuh_petelur_mobile.png);"></div>
        </div>

        <div class="produk-kukila">
            <div class="blue-header">
                <div class="container">
                    <div class="img-produk">
                         <img src="<?php echo base_url('asset_front/images/asset_produk/puyuh_petelur/karung_kukila_puyuh_petelur.png'); ?>" class="img-responsive" alt="Image">
                    </div>
                    <div class="detail_header ">
                        <div class="row">
                            <div class="col-md-6 col-lg-8 pull-right">
                               <div class="col-sm-12 col-md-5 col-lg-5 border-left">
                                    <div class="img-logo hidden-xs">
                                        <img src="<?php echo base_url('asset_front/images/asset_produk/puyuh_petelur/logo_kukila.png'); ?>" class="img-responsive" alt="Image">
                                    </div>
                               </div>
                               <div class="col-sm-12 col-md-7 col-lg-7">
                                   <div class="text-kukila">
                                        <h1>PUYUH PETELUR (LZ- 48 Crumble)</h1>
                                        <h2></h2>
                                        <p>Pakan Lengkap Siap Konsumsi </p>
                                    </div>
                               </div>
                            </div>
                        </div> 
                    </div>
                    
                </div>
            </div>

            <div class="detail-produk">               
                <div class="col-md-6 col-lg-7 pull-right">
                    <h2 style="color: #4d4d4f;">KEUNGGULAN KUKILA PUYUH PETELUR :</h2>
                    <ol class="list-keunggulan">
                        <li> Ukuran telur lebih besar.</li>
                        <li> Cangkang telur lebih keras, sehingga meminimalisir kerusakan.</li>
                        <li> Produktifitas lebih tinggi </li>
                        <li> Bentuk Crumble, tidak banyak terbuang.</li>
                        <li> Lebih praktis & efisien dibanding Self Mixing </li>
                        <li> SNI : LZ-48 (221/PK.140/F3.A/07/2015)</li>
                    </ol>
                </div>
            </div>

            <div class="komposisi-produk-kukila">
                <div class="col-md-4 col-lg-4">
                    <div class="title-komposisi">
                        <h2>ANALISA KUKILA
                            PUYUH PETELUR
                            <br>
                            (LZ- 48 Crumble)
                        </h2>
                    </div>  
                </div>
                <div class="col-md-8 col-lg-8">
                    <img src="<?php echo base_url('asset_front/images/asset_produk/puyuh_petelur/table_analisa_kukila_petelur.png'); ?>" class="img-responsive" alt="Image">
                </div>
            </div>

        </div>

        <div class="produk-jatayu">
            <div class="purple-header">
                <div class="container">
                    <div class="img-produk-jatayu">
                         <img src="<?php echo base_url('asset_front/images/asset_produk/puyuh_petelur/karung_jatayu_puyuh_petelur.png'); ?>" class="img-responsive" alt="Image">
                    </div>
                    <div class="detail_header">
                        <div class="row">
                            <div class="col-md-6 col-lg-8 pull-right">
                               <div class="col-md-5 col-lg-5 border-leftjt">
                                    <div class="img-logo max-127 hidden-xs">
                                        <img src="<?php echo base_url('asset_front/images/asset_produk/puyuh_petelur/logo_jatayu.png'); ?>" class="img-responsive" alt="Image">
                                    </div>
                               </div>
                               <div class="col-md-7 col-lg-7">
                                   <div class="text-jatayu">
                                        <h1>PUYUH PETELUR (JTU-Crumble)</h1>
                                        <p>Pakan Lengkap Siap Konsumsi </p>
                                    </div>
                               </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="detail-produk">
                <div class="col-md-6 col-lg-7 pull-right">
                    <h2 style="color: #4d4d4f;">KEUNGGULAN JATAYU PUYUH PETELUR :</h2>
                    <ol class="list-keunggulan">
                        <li> Lebih praktis & eifisien dibandingkan mencampur pakan sendiri (self mixing).</li>
                        <li> Menambah nilai tambah ekonomis untuk peternak.</li>
                        <li> Harga lebih terjangkau. </li>
                    </ol>
                </div>
            </div>
            <div class="komposisi-produk-jatayu">
                <div class="col-md-4 col-lg-4">
                    <div class="title-komposisi">
                        <h2>ANALISA JATAYU
                            PUYUH PETELUR
                            <br>
                            (JT-Crumble)
                        </h2>
                    </div>  
                </div>
                <div class="col-md-8 col-lg-8">
                    <img src="<?php echo base_url('asset_front/images/asset_produk/puyuh_petelur/tabel_analisa_jatayu_petelur.png'); ?>" class="img-responsive" alt="Image">
                </div>
            </div>
        </div>

        <main>
            <div class="container-fluid pd-0">

            </div>
            
            
        
            <?php $this->load->view('frontpages/kontak_sales'); ?>

        </div>

        </main>
        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>