<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>
        </header>
        <!-- End of Header -->
        <main>
            
            <div class="container">
            <div class="detail-topinfo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="detail-info">
                    <span class="date">by <a href="">Admin</a> January 1, 2017.</span>
                    <span class="category">
                        <a href="">#Quisque Out</a>, 
                        <a href="">#Praesent volutpat rutrum </a>, 
                        <a href="">#Nulla sem sem</a>.
                    </span>
                    <span class="comment">
                        <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                    </span>
                </div>

                <div class="detail-sharing">
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-google"></a>
                    <a href="#" class="fa fa-pinterest"></a>
                    <a href="#" class="fa fa-tumblr"></a>
                    <a href="#" class="fa fa-reddit"></a>
                    <a href="#" class="fa fa-rss"></a>
                </div>
            </div>

            <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blogs-content blogs-content-detail">

                <div class="feature">

                    <div class="detail-description textalign">
                        <span class="dropcaps">L</span>orem ipsum dolor sit amet, consectetur adipiscing elit. In eu vulputate elit. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…

                    </div>

                    <div class="detail-content textalign">
                        <p>
                            Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…
                        </p>

                        <div class="image-box">
                            <a target="_blank" href="images/blogs/blog3.jpg" data-lightbox="roadtrip" data-title="Nullam pellentesque ligula aliquam, tempus eros vitae">
                                <img src="images/blogs/blog3.jpg" alt="Forest" >
                            </a>
                            <div class="desc">Nullam pellentesque ligula aliquam, tempus eros vitae</div>
                        </div>

                        <p>
                            Praesent volutpat rutrum tortor. Quisque sit amet justo semper. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…
                        </p>

                        <blockquote class="detail-quote">
                            <div class='quotemarks'>
                                Praesent volutpat rutrum tortor. Quisque sit amet justo semper. Quisque a eleifend velit.<br><br>
                                Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…. Technical skills now dominate in terms of the sheer number of competencies demanded in job descriptions more than cognitive and soft skills combined for virtually every career.. 
                            </div>
                            <small>@2017. Quisque sit amet justo semper. <a href="">See detail</a></small>

                        </blockquote>

                        <p>
                            Praesent volutpat rutrum tortor. Quisque sit amet justo semper. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…
                        </p>

                        <div class="image-box image-box-50-left">
                            <a target="_blank" href="images/blogs/blog2.jpg" data-lightbox="roadtrip" data-title="Quisque sit amet justo semper. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus">
                                <img src="images/blogs/blog2.jpg" alt="Forest" >
                            </a>
                            <div class="desc">Quisque sit amet justo semper. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus</div>
                        </div>

                        <p>
                            Porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…
                        </p>

                        <p>
                            Praesent volutpat rutrum tortor. Quisque sit amet justo semper. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…
                        </p>

                        <p>
                            Porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…
                        </p>

                        <p>
                            Praesent volutpat rutrum tortor. Quisque sit amet justo semper. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…
                        </p>
                        <h4>Quisque sit amet justo semper</h4>

                        <p>
                            Porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…
                        </p>

                        <p>
                            Praesent volutpat rutrum tortor. Quisque sit amet justo semper. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…
                        </p>

                    </div>

                    <div class="detail-author">

                        <fieldset>
                            <legend>Author: <a href="">Henry Pham</a></legend>
                            <div class="avatar">
                                <img src="images/face/face4.jpg">
                            </div>
                            <div class="author-intro">
                                Porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna… Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus
                            </div>

                            <div class="social-networks">
                                <div>
                                    Mobie: <b>012 899 87771</b>. Or connect with me 
                                </div>
                                <a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-vimeo-square" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-tumblr-square" aria-hidden="true"></i></a>
                            </div>
                        </fieldset>

                    </div>
                </div>


                <div class="related-blogs">
    <h3>Related posts</h3>

    <div class="posts row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">
            <div class="photo sticky-new">
                <a href=""><img src="images/blogs/blog2.jpg"></a>
            </div>
            <h3><a href="">Natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus</a></h3>
            <div class="info">
                <span class="date">by <a href="">Admin</a> January 1, 2017.</span>

                <span class="comment">
                    <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                </span>
            </div>

            <div class="description">
                Dros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…

            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">

            <div class="photo sticky-feature">
                <a href=""><img src="images/blogs/blog5.jpg"></a>
            </div>
            <h3><a href="">Phasellus ultrices diam lorem</a></h3>
            <div class="info">
                <span class="date">by <a href="">Admin</a> January 1, 2017.</span>

                <span class="comment">
                    <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                </span>
            </div>

            <div class="description">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu vulputate elit. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Quisque sit amet justo semper, porta metus id..

            </div>


        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">

            <div class="photo">
                <a href=""><img src="images/blogs/blog1.jpg"></a>
            </div>
            <h3><a href="">Quisque a eleifend velit. Nullam pellentesque ligula aliquam</a></h3>
            <div class="info">
                <span class="date">by <a href="">Admin</a> January 1, 2017.</span>
                <span class="comment">
                    <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                </span>
            </div>

            <div class="description">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu vulputate elit. Quisque a eleifend veli tellus. Praesent volutpat rutrum tortor..

            </div>
        </div>
    </div>

</div>
<ul class="pagination pagination-lazyload">
    <li>
        <a href="#">Load more posts <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
    </li>
</ul>
<!-- End of related posted -->                <div class="detail-comments">
    <a name="comments"></a>
    <h3>1 Comments</h3>
    <ul class="comments-list">
        <li class="item">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="images/face/face1.jpg" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <div class="content">
                        <div class="author">
                            <a href="">Consectetur Adipiscing</a> <span>said:</span>
                        </div>
                        <div class="info">
                            <div class="date">
                                March 9, 2017. At 12h:30 AM
                            </div>
                        </div>
                        <div class="text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu vulputate elit. Quisque a eleifend velit. 
                            <br><br>
                            Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante

                        </div>
                        <div class="button">
                            <a href="" class="btn btn-primary btn-sm"><i class="fa fa-reply" aria-hidden="true"></i> REPLY</a>
                            <a href="" class="btn btn-default btn-sm"><i class="fa fa-flag" aria-hidden="true"></i> REPORT</a>
                        </div>
                    </div>    

                </div>
            </div>
        </li>
        <li class="item">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="images/face/face2.jpg" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <div class="content">
                        <div class="author">
                            <a href="">Sit Amet</a> <span>said:</span>
                        </div>
                        <div class="info">
                            <div class="date">
                                March 9, 2017. At 12h:30 AM
                            </div>
                        </div>
                        <div class="text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu vulputate elit. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus.
                        </div>
                        <div class="button">
                            <a href="" class="btn btn-primary btn-sm"><i class="fa fa-reply" aria-hidden="true"></i> REPLY</a>
                            <a href="" class="btn btn-default btn-sm"><i class="fa fa-flag" aria-hidden="true"></i> REPORT</a>
                        </div>
                    </div>    

                </div>
            </div>
        </li>
        <li class="item item-reply">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="images/face/face1.jpg" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <div class="content">
                        <div class="author">
                            <a href="">Sit Amet</a> <span>reply:</span>
                        </div>
                        <div class="info">
                            <div class="date">
                                March 9, 2017. At 12h:30 AM
                            </div>
                        </div>
                        <div class="text">
                            Hi Sit Amet, lorem ipsum dolor sit amet, consectetur adipiscing elit..
                        </div>

                    </div>    

                </div>
            </div>
        </li>
        <li class="item item-reply">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="images/face/face2.jpg" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <div class="content">
                        <div class="author">
                            <a href="">Consectetur Adipiscing</a> <span>reply:</span>
                        </div>
                        <div class="info">
                            <div class="date">
                                March 9, 2017. At 12h:30 AM
                            </div>
                        </div>
                        <div class="text">
                            Thank you!! ^^
                        </div>

                    </div>    

                </div>
            </div>
        </li>
        <li class="item">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="images/face/face3.jpg" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <div class="content">
                        <div class="author">
                            <a href="">Eros Vitae</a> <span>said:</span>
                        </div>
                        <div class="info">
                            <div class="date">
                                March 9, 2017. At 12h:30 AM
                            </div>
                        </div>
                        <div class="text">
                            Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante

                        </div>
                        <div class="button">
                            <a href="" class="btn btn-primary btn-sm"><i class="fa fa-reply" aria-hidden="true"></i> REPLY</a>
                            <a href="" class="btn btn-default btn-sm"><i class="fa fa-flag" aria-hidden="true"></i> REPORT</a>
                        </div>
                    </div>    

                </div>
            </div>
        </li>
    </ul>
    <ul class="pagination pagination-lazyload">
        <li>
            <a href="#">Load more comments <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
        </li>
    </ul>

    <h3>Leave a reply</h3>
    <form data-toggle="validator" action="" method="GET"> 
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group">
                <input class="form-control" id="inputsm" type="text" placeholder="Name" required="">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group">
                <input class="form-control" id="inputsm" type="email" placeholder="Email" required="">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group">
                <input class="form-control" id="inputsm" type="text" placeholder="Website" required="">
            </div>
        </div>
        <div class="form-group">
            <textarea class="form-control" rows="6" placeholder="Message" required=""></textarea>
        </div>
        <div class="form-group">
            <button class="btn btn-primary" type="submit">
                Post comment
            </button>
        </div>
    </form>
</div>    
<!-- End of Comments -->
            </article>


        </div>

        </main>
        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>