<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>
        </header>
        <!-- End of Header -->
        <main>
            
        </main>
        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>