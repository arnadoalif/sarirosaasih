<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
        <style type="text/css">
            .gal-container{
                padding: 12px;
            }
            .gal-item{
                overflow: hidden;
                padding: 3px;
            }
            .gal-item .box{
                height: 350px;
                overflow: hidden;
            }
            .box img{
                height: 100%;
                width: 100%;
                object-fit:cover;
                -o-object-fit:cover;
            }
            .gal-item a:focus{
                outline: none;
            }
            .gal-item a:after{
                content:"\e003";
                font-family: 'Glyphicons Halflings';
                opacity: 0;
                background-color: rgba(0, 0, 0, 0.75);
                position: absolute;
                right: 3px;
                left: 3px;
                top: 3px;
                bottom: 3px;
                text-align: center;
                line-height: 350px;
                font-size: 30px;
                color: #fff;
                -webkit-transition: all 0.5s ease-in-out 0s;
                -moz-transition: all 0.5s ease-in-out 0s;
                transition: all 0.5s ease-in-out 0s;
            }
            .gal-item a:hover:after{
                opacity: 1;
            }
            .modal-open .gal-container .modal{
                background-color: rgba(0,0,0,0.4);
            }
            .modal-open .gal-item .modal-body{
                padding: 0px;
            }
            .modal-open .gal-item button.close{
                position: absolute;
                width: 25px;
                height: 25px;
                background-color: #000;
                opacity: 1;
                color: #fff;
                z-index: 999;
                right: -12px;
                top: -12px;
                border-radius: 50%;
                font-size: 15px;
                border: 2px solid #fff;
                line-height: 25px;
                -webkit-box-shadow: 0 0 1px 1px rgba(0,0,0,0.35);
                box-shadow: 0 0 1px 1px rgba(0,0,0,0.35);
            }
            .modal-open .gal-item button.close:focus{
                outline: none;
            }
            .modal-open .gal-item button.close span{
                position: relative;
                top: -3px;
                font-weight: lighter;
                text-shadow:none;
            }
            .gal-container .modal-dialogue{
                width: 80%;
            }
            .gal-container .description{
                position: relative;
                height: 40px;
                top: -40px;
                padding: 10px 25px;
                background-color: rgba(0,0,0,0.5);
                color: #fff;
                text-align: left;
            }
            .gal-container .description h4{
                margin:0px;
                font-size: 15px;
                font-weight: 300;
                line-height: 20px;
            }
            .gal-container .modal.fade .modal-dialog {
                -webkit-transform: scale(0.1);
                -moz-transform: scale(0.1);
                -ms-transform: scale(0.1);
                transform: scale(0.1);
                top: 100px;
                opacity: 0;
                -webkit-transition: all 0.3s;
                -moz-transition: all 0.3s;
                transition: all 0.3s;
            }

            .gal-container .modal.fade.in .modal-dialog {
                -webkit-transform: scale(1);
                -moz-transform: scale(1);
                -ms-transform: scale(1);
                transform: scale(1);
                -webkit-transform: translate3d(0, -100px, 0);
                transform: translate3d(0, -100px, 0);
                opacity: 1;
            }
            @media (min-width: 768px) {
            .gal-container .modal-dialog {
                width: 55%;
                margin: 50 auto;
            }
            }
            @media (max-width: 768px) {
                .gal-container .modal-content{
                    height:250px;
                }
            }
            /* Footer Style */
            i.red{
                color:#BC0213;
            }
            .gal-container{
                padding-top :75px;
                padding-bottom:75px;
            }
        </style>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>
        </header>
        <!-- End of Header -->

        <div class="detail-header detail-header-blog-category">
            <div class="container">
                <h1>Galeri</h1>

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li class="breadcrumb-item active"> Galeri</li>
                </ol>
            </div>
            <div class="background"></div>
        </div>


        <main>

            <div class="blogs-feature" id="galeri-slide">
                <div class="blogs-feature-slider">
                    <div id="carousel-blogs-feature-slider" class="carousel slide carousel-fade" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-blogs-feature-slider" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-blogs-feature-slider" data-slide-to="1" class=""></li>
                            <li data-target="#carousel-blogs-feature-slider" data-slide-to="2" class=""></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="slide-image" style="background: url(./asset_front/images/blogs/slide_galeri_1.png)">
                                </div>
                            </div>

                            <div class="item">
                                <div class="slide-image" style="background: url(./asset_front/images/blogs/slide_galeri_2.png)">
                                </div>
                            </div>

                            <div class="item">
                                <div class="slide-image" style="background: url(./asset_front/images/blogs/slide_galeri_3.png)">
                                </div>
                            </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#carousel-blogs-feature-slider" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-blogs-feature-slider" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>

                    </div>
                </div>
            </div>

            <div class="container">

            <article class="blogs-content blogs-content-3cols">
                
                <section>
                      <div class="container gal-container">
                        <?php for ($i = 1; $i <= (count(scandir('./asset_front/images/galeri')) - 2); $i++ ): ?>
                            
                            <?php if (($i % 2) == 0): ?>
                                <div class="col-md-4 col-sm-12 co-xs-12 gal-item">
                            <?php else: ?>
                                <div class="col-md-8 col-sm-12 co-xs-12 gal-item">
                            <?php endif ?>
                            
                          
                          <div class="box">
                            <a href="#" data-toggle="modal" data-target="#1">
                              <img src="<?php echo base_url('asset_front/images/galeri/galeri_'.$i.'.JPEG'); ?>">
                            </a>
                            <div class="modal fade" id="1" tabindex="-1" role="dialog">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                  <div class="modal-body">
                                    <img src="<?php echo base_url('asset_front/images/galeri/galeri_'.$i.'.JPEG'); ?>">
                                  </div>
                                    <!-- <div class="col-md-12 description">
                                      <h4>This is the first one on my Gallery</h4>
                                    </div> -->
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <?php endfor ?>
                      </div>
                </section>

                <div class="clearfix"></div>
                <!-- <ul class="pagination pagination-lg pagination-lazyload">
                    <li>
                        <a href="#">Load more blog <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                    </li>
                </ul> -->
            </article>
            </div>
            
        </main>

        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>