<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>

            <div class="detail-header detail-header-blog-category">
                <div class="container">
                    <h1>Testimoni</h1>

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(''); ?>">Home</a></li>
                        <li class="breadcrumb-item active"> Testimoni</li>
                    </ol>
                </div>
                <div class="background"></div>
            </div>
        </header>
        <!-- End of Header -->
        <main>

            <div class="blogs-feature">
                <div class="blogs-feature-slider">
                    <div id="carousel-blogs-feature-slider" class="carousel slide carousel-fade" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-blogs-feature-slider" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-blogs-feature-slider" data-slide-to="1" class=""></li>
                            <li data-target="#carousel-blogs-feature-slider" data-slide-to="2" class=""></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="slide-image" style="background: url(./asset_front/images/blogs/blog1.jpg)">    
                                    <div class="container welcome">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 inner">
                                            <h2 class="title">
                                                Laborum dolo rumes fugats untras 
                                            </h2>
                                            <div class="detail-info">
                                                <span class="date">by <a href="">Admin</a> January 1, 2017.</span>
                                                <span class="category">
                                                    <a href="">#Quisque Out</a>, 
                                                    <a href="">#Praesent volutpat rutrum </a>, 
                                                    <a href="">#Nulla sem sem</a>.
                                                </span>
                                                <span class="comment">
                                                    <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                                                </span>
                                            </div>
                                            <div class="subtitle">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. </p>
                                                <p>Nam neque nulla, venenatis non iaculis non, scelerisque ultricies metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. </p>
                                            </div>
                                            <div class="button">
                                                <a href="" class="button-primary">SEE DETAIL</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="item">
                                <div class="slide-image" style="background: url(./asset_front/images/blogs/blog2.jpg)">                                    

                                    <div class="container welcome">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 inner">
                                            <h2 class="title">
                                                Phasellus ultrices diam lorem
                                            </h2>
                                            <div class="detail-info">
                                                <span class="date">by <a href="">Admin</a> January 1, 2017.</span>
                                                <span class="category">
                                                    <a href="">#Quisque Out</a>, 
                                                    <a href="">#Praesent volutpat rutrum </a>
                                                </span>
                                                <span class="comment">
                                                    <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                                                </span>
                                            </div>
                                            <div class="subtitle">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. 
                                            </div>
                                            <div class="button">
                                                <a href="" class="button-primary">SEE DETAIL</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="slide-image" style="background: url(./asset_front/images/blogs/blog3.jpg)">                                    

                                    <div class="container welcome">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 inner">
                                            <h2 class="title">
                                                Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet
                                            </h2>
                                            <div class="detail-info">
                                                <span class="date">by <a href="">Admin</a> January 1, 2017.</span>
                                                <span class="category">

                                                    <a href="">#Nulla sem sem</a>.
                                                </span>
                                                <span class="comment">
                                                    <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                                                </span>
                                            </div>
                                            <div class="subtitle">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. 
                                            </div>
                                            <div class="button">
                                                <a href="" class="button-primary">SEE DETAIL</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#carousel-blogs-feature-slider" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-blogs-feature-slider" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>

                    </div>
                </div>
            </div>
            <!-- End if blogs-feature -->

            <div class="container">
                <article class="blogs-content blogs-content-3cols">

                    <!-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

                        <div class="blog-item">
                            <div class="photo">
                                <iframe class="media-video" width="100%" src="https://www.youtube.com/embed/r3ebOxltJ1w" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <h3><a href="">Qrhoncus risus eget, sodales ex. In malesuada urna magna. Vestibulum ante…</a></h3>
                            <div class="info">
                                <span class="date">by <a href="">Admin</a> January 1, 2017.</span>
                                <span class="category">
                                    <a href="">#Quisque Out</a>, 
                                    <a href="">#Praesent volutpat rutrum </a>, 
                                    <a href="">#Nulla sem sem</a>.
                                </span>
                                <span class="comment">
                                    <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                                </span>
                            </div>

                            <div class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

                        <div class="blog-item sticky-new">
                            <div class="photo">
                                <a href=""><img src="images/blogs/blog8.jpg"></a>
                            </div>
                            <h3><a href="">Ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet</a></h3>
                            <div class="info">
                                <span class="date">by <a href="">Admin</a> January 1, 2017.</span>
                                <span class="category">
                                    <a href="">#Praesent volutpat rutrum </a>, 
                                    <a href="">#Nulla sem sem</a>.
                                </span>
                                <span class="comment">
                                    <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                                </span>
                            </div>

                            <div class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu vulputate quam, tempus eros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…

                            </div>
                        </div>                        

                        <div class="blog-item">
                            <div class="photo">
                                <a href=""><img src="images/blogs/blog5.jpg"></a>
                            </div>
                            <h3><a href="">Phasellus ultrices diam lorem</a></h3>
                            <div class="info">
                                <span class="date">by <a href="">Admin</a> January 1, 2017.</span>
                                <span class="category">
                                    <a href="">#Quisque Out</a>, 
                                    <a href="">#Nulla sem sem</a>.
                                </span>
                                <span class="comment">
                                    <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                                </span>
                            </div>

                            <div class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu vulputate elit. Quisque a eleifend velit. Nullam pellentesque ligula aliquam, tempus eros vitae, hendrerit tellus. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna… K hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna

                            </div>
                        </div>

                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="blog-item">
                            <div class="photo">
                                <a href=""><img src="images/blogs/blog2.jpg"></a>
                            </div>
                            <h3><a href="">Natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus</a></h3>
                            <div class="info">
                                <span class="date">by <a href="">Admin</a> January 1, 2017.</span>
                                <span class="category">
                                    <a href="">#Praesent volutpat rutrum </a>, 
                                    <a href="">#Nulla sem sem</a>.
                                </span>
                                <span class="comment">
                                    <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                                </span>
                            </div>

                            <div class="description">
                                Dros vitae, hendrerit tellus. Praesent volutpat rutrum tortor. Quisque sit amet justo semper, porta metus id, ultrices ante. Nulla sem sem, bibendum eu magna…

                            </div>
                        </div>

                    </div>

                    <div class="clearfix"></div>
                    <ul class="pagination pagination-lg pagination-lazyload">
                        <li>
                            <a href="#">Load more Testimoni <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                        </li>
                    </ul> -->

                </article>
            </div>
            
        </main>
        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>