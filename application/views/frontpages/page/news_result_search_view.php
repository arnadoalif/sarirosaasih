<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>
        </header>
        <!-- End of Header -->

        <div class="detail-header detail-header-blog-category">
            <div class="container">
                <h1>Search: <u><?php echo $keyword_search ?></u></h1>

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li class="breadcrumb-item active">Search: <?php echo $keyword_search ?></li>
                </ol>
            </div>
            <div class="background"></div>
        </div>

        <main>

            <div class="container" style="top: 38px; position: relative;">
            
            <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blogs-search">
                <h2 style="color: #2081c7; font-weight: 700;">Hasil Pencarian Produk</h2>
                
                <?php foreach ($count_result_produk as $dt_produk): ?>
                    <div class="col-sm-6 col-md-6 col-lg-12">
                        <div class="search-item">
                            <div class="media">
                                
                                <div class="media-body">
                                    <h4 class="media-heading">
                                    <a href="<?php echo base_url($dt_produk->url); ?>" target="_blank"><?php echo $dt_produk->nama_produk ?></a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endforeach ?>

            </article>

            <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blogs-search">
                <h2 style="color: #2081c7; font-weight: 700;">Hasil Pencarian Artikel</h2>
                <?php foreach ($data_search as $dt_search): ?>  
                    <div class="search-item">

                        <div class="media">
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="media-left">

                                    <?php if ($dt_search->type_artikel == "Video"): ?>
                                        <div class="photo">
                                            <iframe class="media-video" width="100%" src="https://www.youtube.com/embed/<?php echo $dt_search->link_video; ?>" frameborder="0" allowfullscreen style="height: 220px;"></iframe>
                                        </div>
                                    <?php else: ?>
                                        <a href="<?php echo base_url('news-detail/'.$dt_search->key_nama_artikel); ?>">
                                            <img class="media-object" src="<?php echo base_url('./storage_img/img_berita/'.$dt_search->cover_artikel); ?>" alt="...">
                                        </a>
                                    <?php endif ?>

                                </div>
                            </div>
                            

                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <?php if ($dt_search->type_artikel == "Video"): ?>
                                             <a href="<?php echo base_url('news-detail/'.$dt_search->key_nama_artikel); ?>"> <i class="fa fa-youtube"></i> <?php echo $dt_search->nama_artikel ?></a>
                                        <?php else: ?>
                                             <a href="<?php echo base_url('news-detail/'.$dt_search->key_nama_artikel); ?>"><?php echo $dt_search->nama_artikel ?></a>
                                        <?php endif ?>
                                       
                                    </h4>
                                    <div class="info">
                                        <div class="date">
                                            <?php echo date("M d, Y H", strtotime($dt_search->create_at)); ?> - <a href="blog-detail-regular2.html"><?php echo $dt_search->create_by ?></a>
                                        </div>
                                        <span class="category">
                                            <a href="">#<?php echo $dt_search->kategori_artikel ?></a>
                                        </span>
                                    </div>

                                    <div class="description">
                                        <?php if ($dt_search->type_artikel == "Video"): ?>
                                            <a class="btn btn-info" href="https://www.youtube.com/watch?v=<?php echo $dt_search->link_video; ?>" target="_blank" role="button"><i class="fa fa-youtube-play"></i> Play Video</a>
                                        <?php else: ?>
                                            <?php echo substr(strip_tags($dt_search->isi_artikel),0, 230) ?>
                                        <?php endif ?>
                                        

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                <?php endforeach ?>
            </article>

        </div>
            
        </main>
        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>