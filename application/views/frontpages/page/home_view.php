<!DOCTYPE html>
<html lang="en">
    <head>
         <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>          
    </head>
    <body>
    <header>

        <?php $this->load->view('frontpages/menu_bar'); ?>

        <div class="slider">
            <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    
                    <?php $i = 0; foreach ($data_slide as $key => $dt_slide): ?>
                        
                        <?php if ($key == 0): ?>
                            <div class="item active">
                        <?php else: ?>
                            <div class="item">
                        <?php endif ?>
                        
                            <div class="slide-image" style="background: url(./storage_img/img_slide/<?php echo $dt_slide->cover_slide ?>)">
                            </div>
                            
                            <?php if ($key == 0): ?>
                                <div class="slide-imagemobile hidden-lg" style="background: url(./asset_front/images/slides/sillo.jpg)">
                                </div>
                            <?php elseif($key == 1): ?>
                                <div class="slide-imagemobile hidden-lg" style="background: url(./asset_front/images/slides/720%201.jpg)">
                                </div>
                            <?php elseif($key == 2): ?>
                                <div class="slide-imagemobile hidden-lg" style="background: url(./asset_front/images/slides/720%203.jpg)">
                                </div>
                            <?php elseif($key == 3): ?>
                                <div class="slide-imagemobile hidden-lg" style="background: url(./asset_front/images/slides/720%202.jpg)">
                                </div>
                            <?php endif ?>

                            <div class="welcome">
                                <h2 class="title animated fadeInDown">
                                    <?php echo $dt_slide->deskripsi_1 ?>
                                </h2>
                                <div class="subtitle animated fadeInDown animation-delay-1">
                                    <?php echo $dt_slide->deskripsi_2 ?>
                                </div>
                                <div class="button animated fadeInUp animation-delay-2">
                                    <a href="https://api.whatsapp.com/send?phone=6287739103898&text=Saya%20Mau%20Order" class="button-default">HUBUNGI MARKETING</a>
                                    <a href="#kontak_sales" class="button-primary"><i class="fa fa-shopping-cart"></i> PESAN PAKAN </a>
                                </div>
                            </div>
                        </div>

                    <?php $i++; endforeach ?>

                    <div class="services animated bounce">                                
                        <div class="container">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="service-item">
                                    <div class="title">
                                        PAKAN KUKILA
                                    </div>
                                    <div class="description-slide">
                                        Merk pakan premium dari PT. SRA, 
                                        Homogenitas pakan seragam, nutrisi lebih seimbang
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="service-item">
                                    <div class="title">
                                        PAKAN JATAYU
                                    </div>
                                    <div class="description-slide">
                                        Alternatif pakan unggas dengan harga terjangkau.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!-- end of slider -->

    </header>
    <!-- End of Header -->

    <main>
        <div class="home-why">
            <div class="container">
                <h3>KEUNGGULAN</h3>
                <div class="subtitle">Produk PT. Sari Rosa Asih</div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item">
                    <a href="#">
                        <div class="media">
                            <div class="media-left media-top">
                                <img src="<?php echo base_url('asset_front/images/SVG/icon-nutrisi.svg'); ?>">
                            </div>
                            <div class="media-body">
                                <h4 class="media-headinghome">NUTRISI LENGKAP</h4>
                                <div class="description-home">
                                    Kontrol ketat bahan baku & proses produksi untuk menjamin kualitas nutrisi pakan.
                                </div>
                            </div>
                        </div>
                    </a>    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item">
                    <a href="#">
                        <div class="media">                        
                            <div class="media-left media-top">
                                <img src="<?php echo base_url('asset_front/images/SVG/icon-memuaskan.svg'); ?>">
                            </div>
                            <div class="media-body">
                                <h4 class="media-headinghome">HASIL MEMUASKAN</h4>
                                <div class="description-home">
                                    Riset dilakukan sejak tahun 2004 untuk menghasilkan produk berkualitas.
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item">
                    <a href="#">
                        <div class="media">
                            <div class="media-left media-top">                            
                                <img src="<?php echo base_url('asset_front/images/SVG/icon-harga.svg'); ?>">                            
                            </div>
                            <div class="media-body">
                                <h4 class="media-headinghome">HARGA TERJANGKAU</h4>
                                <div class="description-home">
                                    Efisiensi produksi & distribus demi menekan harga jual.
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- End of Home Why choose us -->

        <div class="home-letstry panel-toparrow">
            <div class="container-fluid">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-left">
                    <img class="mockup" src="<?php echo base_url('asset_front/images/produk-pakan-sra.png'); ?>">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 col-right">
                    <h2>PROFIL PT. SARI ROSA ASIH</h2>
                    <div class="description-profil" style="color: #fff;">
                       <p>
                           PT Sari Rosa Asih (PT. SRA) merupakan perusahaan yang bergerak di bidang pakan ternak. PT SRA berdiri sejak Tahun 2004 dengan alamat di Bangsan, Sindumartani, Ngemplak, Sleman, Yogyakarta. Awal berdirinya, PT. SRA fokus memproduksi Pakan Puyuh petelur untuk kalangan sendiri dengan merk KUKILA Puyuh Petelur. 
                       </p>
                       <p>
                           Pakan KUKILA dan JATAYU telah resmi mendapatkan SNI dan telah terdistribusikan di kota besar seluruh pulau Jawa. Saat ini PT SRA sedang fokus untuk memasarkan produk-produknya di luar Pulau Jawa.
                       </p>
                        
                    </div>    
                    <div class="button">
                        <a href="<?php echo base_url('profil'); ?>" class="button-info">SELENGKAPNYA <i class="fa fa-angle-double-right"></i></a>
                    </div>
                    <!-- <div class="description">
                        Free 14 days. Not credit card info.
                    </div> -->

                </div>
            </div>                
        </div>  

        <!-- End of Home Let's try -->        
        <div class="home-whatwedid">
            <div class="container">
                <h3>Produk</h3>
                <div class="subtitle">Produk PT. Sari Rosa Asih</div>
                

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
                    <div class="hovereffect">
                        <img src="<?php echo base_url('asset_front/images/porfolios/puyuh_petelur.png'); ?>">
                        <div class="name">PUYUH PETELUR</div>
                        <div class="overlay">

                            <div class="full">
                                <div class="button">
                                     <a data-toggle="modal" href="<?php echo base_url('produk/puyuh_petelur'); ?>">LIHAT DETAIL PAKAN <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
                    <div class="hovereffect">
                        <img src="<?php echo base_url('asset_front/images/porfolios/ayam_petelur.png'); ?>">
                        <div class="name">AYAM PETELUR</div>
                        <div class="overlay">

                            <div class="full">
                                <div class="button">
                                     <a data-toggle="modal" href="<?php echo base_url('produk/ayam_petelur'); ?>">LIHAT DETAIL PAKAN <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
                    <div class="hovereffect">
                        <img src="<?php echo base_url('asset_front/images/porfolios/ayam_pedaging.png'); ?>">
                        <div class="name">AYAM PEDAGING</div>
                        <div class="overlay">

                            <div class="full">
                                <div class="button">
                                     <a data-toggle="modal" href="<?php echo base_url('produk/ayam_pedaging'); ?>">LIHAT DETAIL PAKAN <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
                    <div class="hovereffect">
                        <img src="<?php echo base_url('asset_front/images/porfolios/ayam_pajantan_joper.png'); ?>">
                        <div class="name">AYAM JANTAN / JAWA SUPER (JOPER)</div>
                        <div class="overlay">

                            <div class="full">
                                <div class="button">
                                    <a data-toggle="modal" href="<?php echo base_url('produk/ayam_joper'); ?>">LIHAT DETAIL PAKAN <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  

            </div>
        </div>
        <!-- End of Home What we did -->

        <div class="home-distribusi">
            <div class="container">
                <h3>DIMANA MENDAPATKAN PAKAN KUKILA DAN JATAYU?</h3>
                <div class="img-maps">
                    <img src="<?php echo base_url('asset_front/images/maps_distribusi.png'); ?>" class="img-responsive" alt="Image">
                </div>
                <a class="btn btn-drak" href="<?php echo base_url('distribusi'); ?>" role="button">DETAIL DISTRIBUSI</a>
            </div>
        </div>

        <div class="home-promotion">
            <div class="container-fluid">
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    <div class="description-promotion">
                        <p>
                            Di Daerah Anda <span class="cb-yellow"> Belum Tersedia? </span>
                            Jadilah <span class="cb-yellow">Agen</span> Kukila dan Jatayu!
                        </p>
                        <p>
                            <span class="c-yellow">Dapatkan</span> <span class="cb-yellow">Keuntungan
                            Jutaan</span><span class="c-yellow">Rupiah Setiap</span> <span class="cb-yellow">Minggunya!</span>
                        </p>   
                    </div>
                         
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <a class="btn btn-lg btn-default btn-lgpromotion" href="tel:087739103898" role="button">KLIK DISINI <br>
                    UNTUK 
                    MENJADI<br>
                    AGEN</a>
                </div>
            </div>
        </div>
        

        <?php $this->load->view('frontpages/kontak_sales'); ?>     

        <div class="home-testimonial">
            <div class="container content">
                <h3 style="font-weight: 900; color: #2081c7;">TESTIMONI</h3>
                <div class="row">
                    <?php foreach ($data_testimoni as $dt_testimoni): ?>
                        <div class="col-md-4 py-2">
                            <div class="video">
                                <!-- <iframe src="https://www.youtube.com/embed/<?php echo $dt_testimoni->link_video; ?>?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe></div> -->
                                <iframe class="media-video" width="100%" src="https://www.youtube.com/embed/<?php echo $dt_testimoni->link_video; ?>" frameborder="0" allowfullscreen style="height: 220px;"></iframe>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
        <!-- End of Home testimonial -->

             
        <div class="home-blogs">
            <h3 style="font-weight: 900; color: #2081c7;">News </h3>
            <!-- <div class="menu">
                <ul>
                    <li><b>Pilih Kategori</b> </li>
                    <li class="active"><a href="">Tips</a> </li>
                    <li><a href="">Kesehatan</a> </li>
                    <li><a href="">Berita</a> </li>
                    <li><a href="">Video</a> </li>
                    <li><a href="">Lainnya</a> </li>
                </ul>
            </div> -->

            <div class="contents-article">
                <div id="blogs-carousel" class="carousel slide carousel-fade" data-ride="carousel">

                    <div class="carousel-inner">
                        
                        <?php
                            $jml_rows = count($data_artikel);
                            $no = 1;
                            $i = 0;
                        ?>

                        <?php foreach ($data_artikel as $dt_artikel): ?>
                            
                            <?php $i++; ?>
                            <?php if ($no == 1): ?>
                                <div class="item active" id="<?php echo $i.'-data' ?>">
                                    <div class="container">
                            <?php endif ?>
                                
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="article">
                                            <?php if ($dt_artikel->type_artikel == "Video"): ?>
                                                <div class="photo">
                                                    <iframe class="media-video" width="100%" src="https://www.youtube.com/embed/<?php echo $dt_artikel->link_video; ?>" frameborder="0" allowfullscreen style="height: 220px;"></iframe>
                                                </div>
                                            <?php else: ?>
                                                <div class="article-img hovereffect">
                                                    <a href=""><img src="<?php echo base_url('./storage_img/img_berita/'.$dt_artikel->cover_artikel); ?>"></a>
                                                    <div class="overlay">
                                                        <a class="info" href="<?php echo base_url('news-detail/'.$dt_artikel->key_nama_artikel); ?>"><i class="fa fa-arrow-right" aria-hidden="true"></i> click to show detail</a>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                            
                                            <div class="article-content">
                                                <h5><a href="<?php echo base_url('news-detail/'.$dt_artikel->key_nama_artikel); ?>"><?php echo $dt_artikel->nama_artikel ?></a></h5>
                                                <div class="description">
                                                   
                                                </div>
                                                <div class="date">
                                                   <i class="fa fa-calendar"></i> <?php echo date("d/m/Y H:i", strtotime($dt_artikel->create_at)); ?>. <a href="<?php echo base_url('news-detail/'.$dt_artikel->key_nama_artikel); ?>"><i class="fa fa-user"></i> <?php echo $dt_artikel->create_by ?></a>.
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                            <?php if ($no == 3 || $i == $jml_rows): ?>
                                    </div>
                                </div>

                                <?php $no = 0; ?>


                            <?php endif ?>


                            <?php $no++; ?>
                        <?php endforeach ?>

                    <div class="clearfix"></div>
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#blogs-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#blogs-carousel" data-slide-to="1"></li>
                        <li data-target="#blogs-carousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#blogs-carousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#blogs-carousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div> <!-- contents-article -->

        </div>
        <!-- End of Home Blogs -->        

        </main>

        <?php $this->load->view('frontpages/footer'); ?>

        </body>

 <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#4-data').removeClass('active');
     });
 </script>
</html>