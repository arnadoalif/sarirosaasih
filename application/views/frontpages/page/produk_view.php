<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>

            <div class="detail-header detail-header-aboutus">
                <div class="container">
                    <h1>Produk PT Sari Rosa Asih</h1>

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Produk</li>
                    </ol>
                </div>
                <div class="background bg3"></div>
            </div>
        </header>
        <!-- End of Header -->
        <main>

            <div class="detail-case-studies gray-background">
                <div class="container">
                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-push-6 col-md-push-6 col-sm-push-6 col-1">


                        <div id="carousel-casestudies2" class="carousel slide carousel-fade sticky-feature" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-casestudies2" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-casestudies2" data-slide-to="1"></li>
                                <li data-target="#carousel-casestudies2" data-slide-to="2"></li>
                            </ol>

                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="<?php echo base_url('asset_front/images/porfolios/jatayu_puyuh_petelur.jpg'); ?>">
                                </div>
                                <div class="item">
                                    <img src="<?php echo base_url('asset_front/images/porfolios/jatayu_ayam_pedaging.jpg'); ?>">
                                </div>
                                <div class="item">
                                    <img src="<?php echo base_url('asset_front/images/porfolios/jatayu_pejantan_joper.jpg'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-pull-6 col-md-pull-6 col-sm-pull-6 col-2">
                        <h3>Produk Pakan JATAYU</h3>

                        <p><span class="dropcaps">JATAYU</span>
                            Produk Pakan JATAYU adalah produk terbaru dari PT. Sari Rosa Asih yang di luncurkan pada tahun 2016 dengan formula khusus dengan harga terjangkau dibandingkan mencampur pakan sendiri.

                            <ul>
                                <li>Pakan Jatayu Puyuh Petelur (JTU-CRUMBLE)</li>
                                <li>Pakan Jatayu Broiler Finisher (JT2-F-CRUMBLE)</li>
                                <li>Pakan Jatayu Jantan/Joper (JT2-F-CRUMBLE)</li>
                            </ul>
                        </p>

                        <p class="customer">
                            <i class="fa fa-podcast" aria-hidden="true"></i> Kostumer : <b>Seluruh Wilayah Pulau Jawa</b>.                                                                                              
                        </p>

                        <p class="description">
                            <i class="fa fa-calendar-o" aria-hidden="true"></i> 
                            Website: <b><a href="http://www.pakanjatayu.com" target="_blank">www.pakanjatayu.com</a></b>

                        </p>

                        <div class="button">
                            <a href="" class="button-default"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> DOWNLOAD BROWSUR</a>
                        </div>

                    </div>
                </div>
            </div> 
            <!-- end of case study right -->

            <div class="detail-case-studies">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-1">


                        <div id="carousel-casestudies3" class="carousel slide carousel-fade" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-casestudies3" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-casestudies3" data-slide-to="1"></li>
                                <li data-target="#carousel-casestudies3" data-slide-to="2"></li>
                            </ol>

                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="<?php echo base_url('asset_front/images/porfolios/kukila_ayam_pedaging.jpg'); ?>">
                                </div>
                                <div class="item">
                                    <img src="<?php echo base_url('asset_front/images/porfolios/kukila_puyuh_petelur.jpg'); ?>">
                                </div>
                                <div class="item">
                                    <img src="<?php echo base_url('asset_front/images/porfolios/kukila_ayam_petelur.jpg'); ?>">
                                </div>
                                <div class="item">
                                    <img src="<?php echo base_url('asset_front/images/porfolios/kukila_jantan_joper.jpg'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-2">
                        <h3>Produk Pakan KUKILA</h3>

                        <p><span class="dropcaps">KUKILA</span>
                        Produk Pakan KUKILA merupakan produk pertama PT Sari Rosa Asih, pakan kukila merukana merk pakan premium dengan keunggulan..

                        <ul>
                            <li>Homogenitas pakan lebih seragam, nutrisi lebih seimbnag.</li>
                            <li>Potongan crumble KUKILA ideal, pakan tidak tersisa / terbuang.</li>
                            <li>KUKILA Puyuh identik dengan kualitas DOQ Peksi Gunaraharja</li>
                            <li>Harga KUKILA bersaing dan mudah didipatkan.</li>
                            <li>Pakan KUKILA telah ber-SNI.</li>
                        </ul>
                        <p><strong>Jenis Pakan Kukila</strong></p>
                        <ul>
                            <li>Pakan Kukila Puyuh Petelur (LZ-48) CRUMBLE</li>
                            <li>Pakan Kukila Ayam Pedaging (BRZ-15) CRUMBLE</li>
                            <li>Pakan Kukila Ayam Petelur (LZ-48) CRUMBLE</li>
                            <li>Pakan Kukila Pejantan / Joper</li>
                        </ul>
                        </p>
                       

                        <p class="customer">
                            <i class="fa fa-podcast" aria-hidden="true"></i> Customer: <b>Seluruh Wilayah Pulau Jawa</b>.                                                          
                        </p>

                        <p class="description">
                            <i class="fa fa-calendar-o" aria-hidden="true"></i>                       
                            Website: <b><a href="http://www.pakankukila.com" target="_blank">www.pakankukila.com</a></b>

                        </p>

                        <div class="button">
                            <a href="" class="button-default"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> DOWNLOAD PDF</a>
                        </div>

                    </div>

                </div>
            </div> 
            <!-- end of case study left -->
            
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                    
                    <div class="portfolios-isotope">
                        <div class="portfolioFilter">
                            <a href="#" data-filter="*" class="current">Jenis Pakan</a>
                            <a href="#" data-filter=".kukila">Pakan Kukila</a>
                            <a href="#" data-filter=".jatayu">Pakan Jatayu</a>
                        </div>

                        <div class="portfolioContainer row">
                            <?php foreach ($data_produk as $dt_produk): ?>
                                
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item * <?php echo $dt_produk->kategori_produk ?>">
                                    <div class="hovereffect">
                                        <img src="<?php echo base_url('storage_img/img_produk/'.$dt_produk->cover_produk); ?>">
                                        <div class="name"><?php echo $dt_produk->nama_produk ?></div>
                                        <div class="overlay">

                                            <div class="full">
                                                <div class="button">
                                                    <a data-toggle="modal" data-target="#projectDetail<?php echo $dt_produk->kode_produk ?>">View Detail Produk <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                                </div>
                                                

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endforeach ?>
                            
                        </div>
                    </div>
                </div>
            </div>

             <!-- Modal -->
            <div id="projectDetail" class="modal modalFullscreen">

                <div class="modal-content">
                    <span class="closeX" data-dismiss="modal">&times;</span>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-left">

                            <div id="myCarousel3" class="carousel slide carousel-fade" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#myCarousel3" data-slide-to="0" class="active"></li>
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img src="<?php echo base_url('asset_front/images/porfolios/kukila_ayam_pedaging.jpg'); ?>" class="left-img">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-right">
                            <h3>
                                KUKILA
                            </h3>
                            <div class="description">
                                <p class="customer">                             
                                    BAHAN YANG DIPAKAI: <br>
                                    <b>Jagung, Bekatul, Bungkil Kedelai, Fulfat, Tepung Daging & Tulang, Tepung tulang, Mineral, Dicalsiumfasfat, Natrium klorida, Asam amino</b>.                                                                
                                </p>
                                <p>
                                    DILARANG: <br><b>Dipakai Sebagai Pakan Ternak Ruminansia (sapi, kerbau, kambing, domba) </b>
                                </p>
                                

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Nama Analisa</th>
                                            <th>Nilai Analisa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>KADAR AIR MAX</td>
                                            <td>11 %</td>
                                        </tr>
                                        <tr>
                                            <td>PROTEIN</td>
                                            <td>20-21 %</td>
                                        </tr>
                                        <tr>
                                            <td>LEMAK KASAR</td>
                                            <td>3-7% </td>
                                        </tr>
                                        <tr>
                                            <td>SARAT KASAR MAX</td>
                                            <td>4%</td>
                                        </tr>
                                        <tr>
                                            <td>ABU MAX</td>
                                            <td>14%</td>
                                        </tr>
                                        <tr>
                                            <td>KALSIUM</td>
                                            <td>2,5-3,5%</td>
                                        </tr>
                                        <tr>
                                            <td>FOSFOR</td>
                                            <td>0,6-1%</td>
                                        </tr>
                                        <tr>
                                            <td>ANTIBIOTIK</td>
                                            <td>+ %</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </main>
        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>