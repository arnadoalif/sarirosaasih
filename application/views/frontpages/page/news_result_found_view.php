<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>
    </head>
    <body>
        <header>
            <?php $this->load->view('frontpages/menu_bar'); ?>
        </header>
        <!-- End of Header -->

        <div class="detail-header detail-header-blog-category">
            <div class="container">
                <h1>Search: <u><?php echo $keyword_search; ?></u></h1>

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li class="breadcrumb-item active">Search: <?php echo $keyword_search; ?></li>
                </ol>
            </div>
            <div class="background"></div>
        </div>

        <main>

             <div class="container">
                <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blogs-search">


                    <div class="nothing-found-big">
                        <span>Maff</span>
                        <div class="nothing">Pencarian Tidak Tersedia</div>
                        <div class="bottom">Periksa kembali kata kunci anda yang di cari</div>
                    </div>

                    <div class="row">
                        <div class="widget-search col-lg-4  col-md-4  col-sm-4  col-xs-12">
                            <form data-toggle="validator" action="<?php echo base_url('frontpage/search_news'); ?>" method="GET"> 
                                <div class="input-group">
                                    <input type="text" name="search" class="form-control" placeholder="Search in website" id="txtSearch">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary" type="submit">
                                            Go
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </article>


            </div>
            
        </main>
        <?php $this->load->view('frontpages/footer'); ?>
    </body>
    <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>