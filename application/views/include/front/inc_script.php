


<div id="kontak-staytop" class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<h3>INGIN MENJADI AGEN ? HUBUNGI</h3>
				<h3>PAK TUHU</h3>
				<p>0813-2912-3898</p>
				<div class="btn-group-horizontal">
					<a class="btn btn-sm btn-danger" href="tel:081329123898" role="button"><i class="fa fa-phone"></i> Telepon</a>
					<a class="btn btn-sm btn-success" href="https://api.whatsapp.com/send?phone=6281329123898&text=Saya%20Mau%20Order" role="button"><i class="fa fa-whatsapp"></i>Whastapp</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<h3>
					INGIN MENCOBA <br>KUKILA & JATAYU<br> KLIK TOMBOL DISTRIBUSI
				</h3>
				<a class="btn btn-sm btn-primary" href="<?php echo base_url('distribusi'); ?>" role="button"><i class="fa fa-location-arrow"></i> DAFTAR DISTRIBUSI</a>
			</div>
		</div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url('asset_front/libs/bootstrap.min.js'); ?> "></script>

<script src="<?php echo base_url('asset_front/libs/lightbox-plus-jquery.min.js'); ?> "></script>
<script src="<?php echo base_url('asset_front/libs/isotope.pkgd.min.js'); ?> "></script>

<!-- Placed at the end of the document so the pages load faster -->
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo base_url('asset_front/js/ie10-viewport-bug-workaround.js'); ?> "></script>

<script src="<?php echo base_url('asset_front/js/common.js'); ?> "></script>

<script>

    $(document).ready(function () {
        backToTop();
        checkNavbarScroll();
    });
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115062658-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93310500-1');
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/589e7d4da9e5680aa3aecd84/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
