<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title">PT <span>Sari Rosa Asih</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png'); ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png'); ?>" alt="">Administrator
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <!-- <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    
                  </ul>
                </li> -->
                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Tambah Barita</h3>
              </div> 
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tambah Barita <small>Tambahakan berita PT. Sari Rosa Asih</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />

                    <form id="demo-form2" action="<?php echo base_url('administrator/action_tambah_berita'); ?>" method="POST" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Judul Artikel <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="judul_artikel" id="judul_artikel" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Type Artikel <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="type_artikel" id="inputtype_artikel" class="form-control col-md-7 col-xs-12" required="required">
                            <option value="Text">Berita Text</option>
                            <option value="Video">Berita Video</option>
                            <option value="" selected="">- Pilih Artikel -</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kategori Artikel <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="kategori_artikel" id="inputKategori_artikel" class="form-control col-md-7 col-xs-12" required="required">
                            <option value="Tips Trik">Tips Trik</option>
                            <option value="Kesehatan">Kesehatan</option>
                            <option value="Berita">Berita</option>
                            <option value="" selected="">Kategori Berita</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group upload_video">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Link Id Youtube <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="link_id_youtube" id="judul_artikel" class="form-control col-md-7 col-xs-12">
                          <span><span class="label label-info"><s>https://youtu.be/</s><span style="color: red; font-style: italic; font-weight: 600;">Xj8Pqlj9Vbc</span> copy dan paste id ke form</span></span>
                        </div>
                      </div>
                      <div class="form-group upload_non_video">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Cover Artikel <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="file" name="cover_artikel" id="cover_artikel" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Isi Artikel <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea name="isi_artikel" cols="30" rows="30" id="summernote" style="display:none;" class="form-control col-md-7 col-xs-12"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tag Keyword Artikel <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" data-role="tagsinput" name="tags" value="jual beli pakan ternak, pakan ayam, pakan puyuh, ayam broiler, peternakan, pemasok pakan ayam, ayam pedaging, pakan ayam pedaging, harga pakan ternak, pakan ayam petelur, harga pakan <?php echo date('Y'); ?>" id="tags_1" required="required"  class="tags form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button">Cancel</button>
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#summernote').summernote({
          height: 300
        });

        $(".upload_video").hide();
        $(".upload_non_video").hide();

       $('#inputtype_artikel').on('change',function(){
          if( $(this).val()==="Video"){
            $(".upload_video").show();
            $(".upload_non_video").hide();
          } else if ($(this).val() === "") {
            $(".upload_video").hide();
            $(".upload_non_video").hide();
          } else {
            $(".upload_video").hide();
            $(".upload_non_video").show();
          }
      });

      });
    </script>
  </body>
</html>