﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
    <style>
      #myMap {
         height: 400px;
         width: 575px;
      }

      #myMapDepo {
         height: 400px;
         width: 575px;
      }
    </style>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title">PT <span>Sari Rosa Asih</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png'); ?>" alt="">Administrator
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
           <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Distributor<small> Distributor Wilayah</small></h3>
              </div>
              
            </div>

            <div class="clearfix"></div>
                <?php if (isset($_SESSION['message_data'])): ?>
                  <div class="alert alert-success margin-bottom-30" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <?php echo $_SESSION['message_data'] ?>
                  </div>
                <?php endif ?>

                <?php if (isset($_SESSION['error_data'])): ?>
                  <div class="alert alert-danger margin-bottom-30" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <?php echo $_SESSION['error_data'] ?>
                  </div>
                <?php endif ?>
            <div class="row">
              
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  
                  <form action="<?php echo base_url('administrator/action_tambah_wilayah'); ?>" method="POST" role="form">
                    <legend>Form Tambah Wilayah</legend>
                  
                    <div class="form-group">
                      <label for="">Nama Wilayah</label>
                      <input type="text" name="nama_wilayah" class="form-control" placeholder="Nama Wilayah" required>
                    </div>

                    <div class="form-group">
                      <label for="">Nama Google Maps</label>
                      <input type="text" name="wilayah_maps" class="form-control" id="searchTextField" autocomplete="on" runat="server" placeholder="Wilayah Google Maps">
                       <input type="hidden" name="state" class="form-control" id="city2" value="" required placeholder="Y">
                    </div>

                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label for="">Koordinat Wilayah X</label>
                          <input type="text" name="koordinat_x" class="form-control" id="koordinat_x" value="" placeholder="X">
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label for="">Koordinat Wilayah Y</label>
                          <input type="text" name="koordinat_y" class="form-control" id="koordinat_y" value="" placeholder="Y">
                        </div>
                      </div>
                    </div>                  
                    <button type="submit" class="btn pull-right btn-primary">Submit Wilayah</button>
                  </form>

                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      
                    </p>
                    <table class="display table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Nama Wilayah</th>
                          <th>Kor Wilayah X</th>
                          <th>Kor Wilayah Y</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php foreach ($data_wilayah as $dt_wilayah): ?>
                        <tr>
                          <td><?php echo $dt_wilayah->nama_wilayah ?></td>
                          <td><?php echo $dt_wilayah->koordinat_x ?></td>
                          <td><?php echo $dt_wilayah->koordinat_y ?></td>
                          <td>
                            <a class="btn btn-danger btn-sm" href="<?php echo base_url('administrator/action_delete_wilayah/'.$dt_wilayah->kode_wilayah); ?>" role="button"><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>

                        <?php endforeach ?>
                      </tbody>
                    </table>
                  </div>

                </div>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  
                   <form action="<?php echo base_url('administrator/action_tambah_customer'); ?>" method="POST" role="form">
                    <legend>Form Tambah Depo / Poultry Shop</legend>
                  
                    <div class="form-group">
                      <label for="">Nama Customer / Poultry Shop</label>
                      <input type="text" name="nama_customer" required class="form-control" id="" placeholder="Nama Depo / Poultry Shop">
                    </div>

                    <div class="form-group">
                      <label for="">Area Wilayah</label>
                      <select name="posisi_wilayah" id="input" class="form-control" required placeholder="Posisi Area">
                        <option value="" selected>-- PILIH WILAYAH --</option>
                        <?php foreach ($data_wilayah as $opt_wilayah): ?>
                          <option value="<?php echo $opt_wilayah->kode_wilayah ?>"><?php echo $opt_wilayah->nama_wilayah ?></option>
                        <?php endforeach ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="">Nomor Telpon</label>
                      <input type="number" name="nomor_telpon" class="form-control" id="" required placeholder="Nomor Telpon">
                    </div>

                    <div class="form-group">
                      <label for="">Alamat Depo</label>
                      <textarea name="alamat" id="input" class="form-control" rows="3" required="required"></textarea>
                    </div>

                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label for="">Koordinat Depo X</label>
                          <input type="text" name="long_x" onfocus="pan()" class="form-control" id="latitude_depo" placeholder="Koordinat X">
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label for="">Koordinat Depo Y</label>
                          <input type="text" name="long_y" onfocus="pan()" class="form-control" id="longitude_depo" placeholder="Koordinat Y">
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="">Drag Drop Marker Map</label>
                      <div id="myMapDepo"></div>
                    </div>
                  
                    <button type="submit" class="btn pull-right btn-primary">Submit Depo / Poultry Shop</button>
                  </form>
                  
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <hr>
                <div class="x_panel">
                  <h3 align="center">DATA CUSTOMER</h3>
                  <div class="x_content">

                    <table id="datatable" class="table table-striped ">
                      <thead>
                        <tr>
                          <th>Area</th>
                          <th>Nama Customer</th>
                          <th>Nomor Telopn</th>
                          <th>X</th>
                          <th>Y</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($data_depo as $dt_depo): ?>
                          <tr>
                            <th><?php echo $dt_depo->nama_wilayah ?></th>
                            <th><?php echo $dt_depo->nama_customer ?></th>
                            <th><?php echo $dt_depo->nomor_telpon ?></th>
                            <th><?php echo $dt_depo->X ?></th>
                            <th><?php echo $dt_depo->Y ?></th>
                            <th>
                              <a class="btn btn-info btn-sm" href="http://www.google.com/maps/place/<?php echo $dt_depo->X.",".$dt_depo->Y ?>" target="_blank" role="button"><i class="fa fa-map"></i> OPEN MAPS</a>
                              <a class="btn btn-default" data-toggle="modal" href='#modal-id<?php echo $dt_depo->kode_depo ?>'><i class="fa fa-edit"></i> EDIT DATA</a>
                              <a class="btn btn-danger" href="<?php echo base_url('administrator/action_delete_customer/'.$dt_depo->kode_depo); ?>" role="button"><i class="fa fa-trash"></i> DELETE DATA</a>
                            </th>
                          </tr>

                          <div class="modal fade" id="modal-id<?php echo $dt_depo->kode_depo ?>">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title"><?php echo $dt_depo->nama_customer ?></h4>
                                </div>
                                <div class="modal-body">
                                  
                                  <form action="<?php echo base_url('administrator/action_edit_customer'); ?>" method="POST" role="form">
                                    <legend>Form Tambah Depo / Poultry Shop</legend>
                                  
                                    <div class="form-group">
                                      <label for="">Nama Customer / Poultry Shop</label>
                                      <input type="hidden" name="kode_depo" id="input" class="form-control" value="<?php echo $dt_depo->kode_depo ?>">
                                      <input type="text" name="nama_customer" value="<?php echo $dt_depo->nama_customer ?>" required class="form-control" id="" placeholder="Nama Depo / Poultry Shop">
                                    </div>

                                    <div class="form-group">
                                      <label for="">Area Wilayah</label>
                                      <select name="posisi_wilayah" id="input" class="form-control" required placeholder="Posisi Area">
                                        <option value="">-- PILIH WILAYAH --</option>
                                        <?php foreach ($data_wilayah as $opt_wilayah): ?>
                                          <?php if ($opt_wilayah->kode_wilayah === $dt_depo->kode_wilayah): ?>
                                            <option value="<?php echo $opt_wilayah->kode_wilayah ?>" selected><?php echo $opt_wilayah->nama_wilayah ?></option>
                                          <?php else: ?>
                                            <option value="<?php echo $opt_wilayah->kode_wilayah ?>"><?php echo $opt_wilayah->nama_wilayah ?></option>
                                          <?php endif ?>
                                          
                                        <?php endforeach ?>
                                      </select>
                                    </div>

                                    <div class="form-group">
                                      <label for="">Nomor Telpon</label>
                                      <input type="number" name="nomor_telpon" value="<?php echo $dt_depo->nomor_telpon ?>" class="form-control" id="" required placeholder="Nomor Telpon">
                                    </div>

                                    <div class="form-group">
                                      <label for="">Alamat Depo</label>
                                      <textarea name="alamat" id="input" class="form-control" rows="3" required="required"><?php echo $dt_depo->alamat ?></textarea>
                                    </div>

                                    <div class="row">
                                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                          <label for="">Koordinat Depo X</label>
                                          <input type="text" name="long_x" value="<?php echo $dt_depo->X ?>" class="form-control" id="latitude_depo" placeholder="Koordinat X">
                                        </div>
                                      </div>
                                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                          <label for="">Koordinat Depo Y</label>
                                          <input type="text" name="long_y" value="<?php echo $dt_depo->Y ?>" class="form-control" id="longitude_depo" placeholder="Koordinat Y">
                                        </div>
                                      </div>
                                    </div>
                                    
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn pull-right btn-primary">Submit Depo / Poultry Shop</button>
                                  </form>


                                </div>
                                
                              </div>
                            </div>
                          </div>

                        <?php endforeach ?>
                      </tbody>
                    </table>
                   

                  </div>
                </div>
                
                
                
              </div>

            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
      $(document).ready(function() {
          $('table.display').DataTable();

        $('form input').on('keypress', function(e) {
            return e.which !== 13;
        });

      });
    </script>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9W8kzf0p7qMy4S7ulgyaFTaodr5vsIYQ&libraries=places"></script>

    <script>
        function initialize() {
          var input = document.getElementById('searchTextField');
          var autocomplete = new google.maps.places.Autocomplete(input);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('city2').value = place.name;
                document.getElementById('koordinat_x').value = place.geometry.location.lat();
                document.getElementById('koordinat_y').value = place.geometry.location.lng();
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <script type="text/javascript"> 
      var map;
      var marker;
      var myLatlng = new google.maps.LatLng(-7.797068,110.370529);
      var geocoder = new google.maps.Geocoder();
      var infowindow = new google.maps.InfoWindow();

      
      function initialize(){
      var mapOptions = {
        zoom: 18,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      map = new google.maps.Map(document.getElementById("myMapDepo"), mapOptions);

      marker = new google.maps.Marker({
        map: map,
        position: myLatlng,
        draggable: true 
      }); 

      geocoder.geocode({'latLng': myLatlng }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            $('#latitude_depo,#longitude_depo').show();
            $('#address_depo').val(results[0].formatted_address);
            $('#latitude_depo').val(marker.getPosition().lat());
            $('#longitude_depo').val(marker.getPosition().lng());
            infowindow.setContent(results[0].formatted_address);
            infowindow.open(map, marker);
          }
        }
      });

      google.maps.event.addListener(marker, 'dragend', function() {

        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              $('#address_depo').val(results[0].formatted_address);
              $('#latitude_depo').val(marker.getPosition().lat());
              $('#longitude_depo').val(marker.getPosition().lng());
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
              }
            }
            });
        });
      }
      google.maps.event.addDomListener(window, 'load', initialize);

      function pan() {
          var panPoint = new google.maps.LatLng(document.getElementById("latitude_depo").value, document.getElementById("longitude_depo").value);
          map.panTo(panPoint);
          var marker = new google.maps.Marker({
            position: panPoint,
            map: map,
            draggable: true 
          });

          geocoder.geocode({'latLng': panPoint }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                $('#latitude_depo,#longitude_depo').show();
                $('#address_depo').val(results[0].formatted_address);
                $('#latitude_depo').val(marker.getPosition().lat());
                $('#longitude_depo').val(marker.getPosition().lng());
                infowindow.setContent(results[0].formatted_address);
                infowindow.open(map, marker);
              }
            }
          });

          google.maps.event.addListener(marker, 'dragend', function() {

            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                  $('#address_depo').val(results[0].formatted_address);
                  $('#latitude_depo').val(marker.getPosition().lat());
                  $('#longitude_depo').val(marker.getPosition().lng());
                  infowindow.setContent(results[0].formatted_address);
                  infowindow.open(map, marker);
                  }
                }
                });
            });

          google.maps.event.addDomListener(window, 'load', initialize);
       }

    </script>

  </body>
</html>