<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title">PT <span>Sari Rosa Asih</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png'); ?>" alt="">Administrator
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <!-- <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li> -->
                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
           <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Data Berita Text<small> List berita PT. Sari Rosa Asih</small></h3>
              </div>
              
            </div>

            <div class="clearfix"></div>

            <div class="row">
              
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      
                      <?php if (isset($_SESSION['message_data'])): ?>
                        <div class="alert alert-success margin-bottom-30" role="alert">
                          <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <?php echo $_SESSION['message_data'] ?>
                        </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                        <div class="alert alert-danger margin-bottom-30" role="alert">
                          <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <?php echo $_SESSION['error_data'] ?>
                        </div>
                      <?php endif ?>

                    </p>
                    <table id="datatable" class="table table-striped ">
                      <thead>
                        <tr>
                          <th>Cover</th>
                          <th>Judul Artikel</th>
                          <th>Kategori</th>
                          <th>Author</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php foreach ($data_berita_text as $dt_berita_text): ?>
                        
                          <tr>
                            <td><img src="<?php echo base_url('storage_img/img_berita/'.$dt_berita_text->cover_artikel); ?>" alt="<?php echo $dt_berita_text->nama_artikel; ?>" style="max-width: 190px;"></td>
                            <td><a href="<?php echo $dt_berita_text->key_nama_artikel; ?>" target="_blank"><?php echo $dt_berita_text->nama_artikel; ?></a> </td>
                            <td><?php echo $dt_berita_text->kategori_artikel ?></td>
                            <td><?php echo $dt_berita_text->create_by ?></td>
                            
                            <td>
                              <a class="btn btn-primary btn-sm" data-toggle="modal" href='#<?php echo $dt_berita_text->kode_artikel ?>' role="button"><i class="fa fa-pencil"></i></a>
                              <a class="btn btn-danger btn-sm" href="<?php echo base_url('administrator/action_delete_berita/'.$dt_berita_text->kode_artikel); ?>" role="button"><i class="fa fa-trash"></i></a>
                            </td>
                          </tr>

                          <div class="modal fade" id="<?php echo $dt_berita_text->kode_artikel ?>">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title"><?php echo $dt_berita_text->nama_artikel ?></h4>
                                </div>

                                  <form action="<?php echo base_url('administrator/action_edit_berita') ?>" method="POST" role="form" enctype="multipart/form-data">
                                    <div class="modal-body">
                                      
                                        <div class="form-group">
                                          <input type="hidden" name="kode_artikel" id="input" class="form-control" value="<?php echo $dt_berita_text->kode_artikel ?>">
                                          <label for="">Judul Artikel</label>
                                          <input type="text" name="judul_artikel" id="judul_artikel" value="<?php echo $dt_berita_text->nama_artikel ?>" required class="form-control">
                                        </div>

                                        <div class="form-group">
                                          <label for="">Kategori Artikel</label>
                                          <select name="kategori_artikel" id="inputKategori_artikel" class="form-control" required="required">
                                            <option <?php echo $dt_berita_text->kategori_artikel == 'Tips Trik' ? 'selected = "selected"': ''; ?> value="Tips Trik">Tips Trik</option>
                                            <option <?php echo $dt_berita_text->kategori_artikel == 'Kesehatan' ? 'selected = "selected"': ''; ?> value="Kesehatan">Kesehatan</option>
                                            <option <?php echo $dt_berita_text->kategori_artikel == 'Berita' ? 'selected = "selected"': ''; ?> value="Berita">Berita</option>
                                            <option value="">Kategori Berita</option>
                                          </select>
                                        </div>

                                        <div class="form-group">
                                          <label for="">Cover Artikel</label>
                                          <input type="file" name="cover_artikel" id="cover_artikel" class="form-control">
                                          <input type="hidden" name="cover_old" id="inputCover_old" class="form-control" value="<?php echo $dt_berita_text->cover_artikel ?>">
                                        </div>

                                        <div class="form-group">
                                          <label for="">Isi Artikel</label>
                                          <textarea name="isi_artikel" cols="30" rows="30" id="summernote" style="display:none;" class="form-control col-md-7 col-xs-12">
                                            <?php echo $dt_berita_text->isi_artikel ?>
                                          </textarea>
                                        </div>

                                        <div class="form-group">
                                          <label for="">Tags</label>
                                          <input type="text" data-role="tagsinput" name="tags" value="<?php echo $dt_berita_text->tags ?>" id="tags_1" required="required"  class="tags form-control col-md-7 col-xs-12">

                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Update Berita</button>
                                    </div>
                                  </form>
                                  
                                </div>
                              </div>
                            </div>
                          </div>

                        <?php endforeach ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
      $(document).ready(function() {
         $('#summernote').summernote({
          height: 300
        });
      });
    </script>
  </body>
</html>