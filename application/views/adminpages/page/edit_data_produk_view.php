<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title">PT <span>Sari Rosa Asih</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png'); ?>" alt="">Administrator
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

               <!--  <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    
                  </ul>
                </li> -->
                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
           <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Produk</h3>
              </div> 
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Produk <small> PT. Sari Rosa Asih</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />

                    <form id="demo-form2" action="<?php echo base_url('administrator/action_edit_produk'); ?>" method="POST" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">

                      <?php foreach ($data_produk as $dt_produk): ?>
                        
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Produk <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="kode_produk" id="inputKode_pro" class="form-control" value="<?php echo $dt_produk->kode_produk ?>">
                          <input type="text" id="nama_produk" name="nama_produk" value="<?php echo $dt_produk->nama_produk ?>" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kategori Produk <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="kategori_produk" id="kategori_produk" id="input" class="form-control col-md-7 col-xs-12" required="required">
                            <option value="">-- Pilih Jenis Pakan --</option>
                            <option <?php echo $dt_produk->kategori_produk == 'kukila' ? 'selected = "selected"': ''; ?> value="kukila">Pakan Kukila</option>
                            <option <?php echo $dt_produk->kategori_produk == 'jatayu' ? 'selected = "selected"': ''; ?> value="jatayu">Pakan Jatayu</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Foto Cover Produk <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="file" name="foto_cover" id="input" class="form-control" value="">
                          <input type="text" name="old_cover" id="input" class="form-control" value="<?php echo $dt_produk->cover_produk ?>" required="required">

                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Deskripsi Produk <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea name="deskripsi_produk" cols="30" rows="30" style="display:none;" class="summernote form-control col-md-7 col-xs-12">
                            <?php echo $dt_produk->deskripsi_produk ?>
                          </textarea>
                        </div>
                      </div>

                      <?php endforeach ?>

                      <div class="ln_solid"></div>

                      <div class="row">
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6">
                              <label>Analisa Produk*</label>
                              <table class="table table-striped table-hover nomargin">
                                <thead>
                                  <tr>
                                    <th>Nama Analisa</th>
                                    <th>Nilai Analisa</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($data_analisa as $dt_analisa): ?>
                                    
                                  <tr>
                                    <td>
                                      <input type="text" name="nama_analisa[]" id="input" class="form-control" value="<?php echo $dt_analisa->nama_analisa ?>">
                                    </td>
                                    <td>
                                      <input type="text" name="nilai_analisa[]" id="input" class="form-control" value="<?php echo $dt_analisa->nilai_analisa ?>" required="required">
                                    </td>
                                  </tr>
                                  
                                  <?php endforeach ?>
                                </tbody>
                              </table>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <label>Hasil Produk *</label>
                              <textarea name="hasil_produk" class="summernote form-control" rows="6" data-lang="en-US">
                                <?php echo $dt_produk->hasil_produk ?>
                              </textarea>
                              
                              <hr>
                              <label>Rate 1 Tahun *</label>
                              <textarea name="rate" class="summernote form-control" rows="6" data-lang="en-US">
                                <?php echo $dt_produk->rate_analisa ?>
                              </textarea>
                            </div>
                          </div>
                        </div>


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                          <button class="btn btn-primary" type="button">Cancel</button>
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
      $(document).ready(function() {
        $('.summernote').summernote({
          height: 300
        });
      });
    </script>
  </body>
</html>