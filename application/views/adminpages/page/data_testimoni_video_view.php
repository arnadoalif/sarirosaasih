<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title">PT <span>Sari Rosa Asih</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png'); ?>" alt="">Administrator
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <!-- <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li> -->
                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Data Testimoni Video <small> List Testimoni PT. Sari Rosa Asih</small></h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
              
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      
                    </p>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Nama Testimoni</th>
                          <th>Link Video</th>
                          <th>Create At</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php foreach ($data_testimoni_video as $dt_testimoni_video): ?>
                          <tr>
                            <td><?php echo $dt_testimoni_video->nama_testimoni ?></td>
                            <td><iframe class="media-video" width="100%" src="https://www.youtube.com/embed/<?php echo $dt_testimoni_video->link_video; ?>" frameborder="0" allowfullscreen style="height: 220px;"></iframe></td>
                            <td><?php echo $dt_testimoni_video->create_at ?></td>
                            <td>
                              <a class="btn btn-xs btn-primary" data-toggle="modal" href='#modal-id<?php echo $dt_testimoni_video->kode_testimoni ?>' role="button"><i class="fa fa-pencil"></i></a>
                              <a class="btn btn-xs btn-danger" href="<?php echo base_url('administrator/action_delete_testimoni/'.$dt_testimoni_video->kode_testimoni); ?>" role="button"><i class="fa fa-trash"></i></a>
                            </td>
                          </tr>

                          <div class="modal fade" id="modal-id<?php echo $dt_testimoni_video->kode_testimoni ?>">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title">Modal title</h4>
                                </div>
                                <form id="demo-form2" action="<?php echo base_url('administrator/action_edit_testimoni_video'); ?>" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <label for="">Nama Testimoni</label>
                                      <input type="hidden" name="kode_testimoni" id="inputKode_tes" class="form-control" value="<?php echo $dt_testimoni_video->kode_testimoni ?>" required="required" pattern="" title="">
                                     <input type="text" name="nama_testimoni" id="nama_testimoni" required="required" value="<?php echo $dt_testimoni_video->nama_testimoni ?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                      <label class="">Profesi Testimoni <span class="required">*</span></label>
                                      <input type="text" name="profesi_testimoni" id="profesi_testimoni" value="<?php echo $dt_testimoni_video->profesi_testimoni ?>" required="required" class="form-control">
                                    </div>
                                    <div class="form-group upload_video">
                                      <label class="">Link Id Youtube <span class="required">*</span></label>
                                        <input type="text" name="link_id_youtube" id="judul_artikel" value="<?php echo $dt_testimoni_video->link_video ?>" class="form-control">
                                        <span><span class="label label-info"><s>https://youtu.be/</s><span style="color: red; font-style: italic; font-weight: 600;">Xj8Pqlj9Vbc</span> copy dan paste id ke form</span></span>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        <?php endforeach ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
  </body>
</html>