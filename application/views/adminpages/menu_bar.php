<!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Produk <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('admin/produk'); ?>">Data Produk</a></li>
                      <li><a href="<?php echo base_url('admin/tambah/produk'); ?>">Tambah Produk</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> News <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('admin/berita'); ?>">Data Berita Text</a></li>
                       <li><a href="<?php echo base_url('admin/berita-video'); ?>">Data Berita Video</a></li>
                      <li><a href="<?php echo base_url('admin/tambah/berita'); ?>">Tambah Berita</a></li>
                     
                    </ul>
                  </li>

                  <li><a><i class="fa fa-desktop"></i> Testimoni <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('admin/tambah/testimoni'); ?>">Tambah Testimoni</a></li>
                      <li><a href="<?php echo base_url('admin/testimoni'); ?>">Data Testimoni</a></li>
                      <li><a href="<?php echo base_url('admin/testimoni-video'); ?>">Data Testimoni Video</a></li>
                      
                    </ul>
                  </li>

                  <li><a><i class="fa fa-table"></i> Kontak <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('admin/kontak-pesan') ?>">Data Kontak Kritik & Saran</a></li>
                      <li><a href="<?php echo base_url('admin/kontak-order') ?>">Data Kontak Order</a></li>
                      <li><a href="<?php echo base_url('admin/kontak-request-pakan') ?>">Data Kontak Request Pakan</a></li>
                    </ul>
                  </li>

                </ul>
              </div>
              <div class="menu_section">
                <h3>Configuration</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Setting <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('admin/distributor') ?>">Distributor</a></li>
                      <li><a href="<?php echo base_url('admin/slide') ?>">Slider</a></li>
                      <li><a href="<?php echo base_url('admin/kontak') ?>">Kontak Person</a></li>
                    </ul>
                  </li>
                  
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->