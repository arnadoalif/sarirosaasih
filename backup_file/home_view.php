<!DOCTYPE html>
<html lang="en">
    <head>
         <?php require_once(APPPATH .'views/include/front/inc_style.php'); ?>          
    </head>
    <body>
    <header>

        <?php $this->load->view('frontpages/menu_bar'); ?>

        <div class="slider">
            <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="slide-image" style="background: url(./asset_front/images/slides/slide10.jpg)">                                    
                        </div>
                        <div class="welcome">
                            <h2 class="title animated fadeInDown">
                                PT SARI ROSA ASIH (PT. SRA) 
                            </h2>
                            <div class="subtitle animated fadeInDown animation-delay-1">
                                Terus berkotmitmen dan berinovasi 
                                menyediakan pakan berkualitas
                                untuk seluruh peternak Indonesia.
                            </div>
                            <div class="button animated fadeInUp animation-delay-2">
                                <a href="<?php echo base_url('profil') ?>" class="button-default">PROFIL PERUSAHAAN</a>
                                <a href="#kontak_sales" class="button-primary"><i class="fa fa-shopping-cart"></i> PESAN PAKAN </a>
                               
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="slide-image" style="background: url(./asset_front/images/slides/slide11.jpg)">                                    
                        </div>
                        <div class="welcome animated fadeInDown">
                            <h2 class="title animated fadeInDown">
                                RISET BERKESINAMBUNGAN  
                            </h2>
                            <div class="subtitle animated fadeInDown animation-delay-1">
                                Kami melakukan Riset secara kontinyu, 
                                untuk meningkatkan kualitas produk kami.
                            </div>
                            <!-- <div class="button animated fadeInUp animation-delay-2">
                                <a href="page-download.html" class="button-primary"><i class="fa fa-download"></i> DOWNLOAD </a>
                                <a href="page-company-history.html" class="button-default">OUR HISTORY</a>
                            </div> -->
                        </div>
                    </div>

                    <div class="item">
                        <div class="slide-image" style="background: url(./asset_front/images/slides/slide12.jpg)">                                    
                        </div>
                        <div class="welcome animated fadeInDown">
                            <h2 class="title animated fadeInDown">
                                BAHAN BAKU BERKUALITAS 
                            </h2>
                            <div class="subtitle animated fadeInDown animation-delay-1">
                                Kualitas ketat bahan baku demi menghasilkan
                                produk pakan dengan nutrisi terbaik.
                            </div>
                           <!--  <div class="button animated fadeInUp animation-delay-2">
                                <a href="page-download.html" class="button-primary"><i class="fa fa-download"></i> DOWNLOAD </a>
                                <a href="page-faqs.html" class="button-default">Frequently Asked Questions</a>
                            </div> -->
                        </div>
                    </div>

                    <div class="item">
                        <div class="slide-image" style="background: url(./asset_front/images/slides/slide13.jpg)">                                    
                        </div>
                        <div class="welcome animated fadeInDown">
                            <h2 class="title animated fadeInDown">
                                PRODUK TELAH TERUJI
                            </h2>
                            <div class="subtitle animated fadeInDown animation-delay-1">
                                Para peternak telah membuktikan hasil yang memuaskan.
                            </div>
                           <!--  <div class="button animated fadeInUp animation-delay-2">
                                <a href="page-download.html" class="button-primary"><i class="fa fa-download"></i> DOWNLOAD </a>
                                <a href="page-faqs.html" class="button-default">Frequently Asked Questions</a>
                            </div> -->
                        </div>
                    </div>


                    <div class="services animated bounce">                                
                        <div class="container">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="service-item">
                                    <div class="title">
                                        PAKAN KUKILA
                                    </div>
                                    <div class="description">
                                        Merk pakan premium dari PT. SRA, 
                                        Homogenitas pakan seragam, nutrisi lebih seimbang
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="service-item">
                                    <div class="title">
                                        PAKAN JATAYU
                                    </div>
                                    <div class="description">
                                        Formula lebih unggul dengan Harga terjangkau 
                                        dibandingkan mencampur pakan sendiri (Self Mixing).
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!-- end of slider -->

    </header>
    <!-- End of Header -->

    <main>
        <div class="home-why">
    <div class="container">
        <h3>KEUNGGULAN</h3>
        <div class="subtitle">Produk PT. Sari Rosa Asih</div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item">
            <a href="#">
                <div class="media">
                    <div class="media-left media-top">
                        <img src="<?php echo base_url('asset_front/images/SVG/icon-nutrisi.svg'); ?>">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">NUTRISI LENGKAP</h4>
                        <div class="description">
                            Kontrol ketat bahan baku & proses produksi untuk menjamin kualitas nutrisi pakan.
                        </div>
                    </div>
                </div>
            </a>    
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item">
            <a href="#">
                <div class="media">                        
                    <div class="media-left media-top">
                        <img src="<?php echo base_url('asset_front/images/SVG/icon-memuaskan.svg'); ?>">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">HASIL MEMUASKAN</h4>
                        <div class="description">
                            Riset dilakukan sejak tahun 2004 untuk menghasilkan produk berkualitas.
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item">
            <a href="#">
                <div class="media">
                    <div class="media-left media-top">                            
                        <img src="<?php echo base_url('asset_front/images/SVG/icon-harga.svg'); ?>">                            
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">HARGA TERJANGKAU</h4>
                        <div class="description">
                            Efisiensi produksi & distribus demi menekan harga jual.
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<!-- End of Home Why choose us -->

        <div class="home-letstry panel-toparrow">

    <div class="container-fluid">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-left">
            <img class="mockup" src="<?php echo base_url('asset_front/images/produk-pakan-sra.png'); ?>">
        </div>
        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 col-right">
            <h2>PROFIL PT. SARI ROSA ASIH</h2>
            <div class="description">
                PT Sari Rosa Asih (PT SRA) merupakan perusahaan yang bergerak di bidang pakan ternak unggas. Awalnya PT SRA ini didirikan untuk menyuplai pakan puyuh kemitraan PT Peksi Gunaraharja.
                <br><br>
                PT Peksi Gunaraharja (PT Peksi) merupakan perusahaan pioner perpuyuhan di Indonesia yang berdiri sejak tahun 1997. PT Peksi sebagai “inti” menyediakan bibit puyuh dan pakan kepada “plasma” (peternak puyuh).
            </div>    
            <div class="button">
                <a href="<?php echo base_url('profil'); ?>" class="button-info">SELENGKAPNYA <i class="fa fa-angle-double-right"></i></a>
            </div>
            <!-- <div class="description">
                Free 14 days. Not credit card info.
            </div> -->

        </div>
    </div>                
</div>  
<!-- End of Home Let's try -->        
<div class="home-whatwedid">

    <div class="container">
        <h3>Produk</h3>
        <div class="subtitle">Produk PT. Sari Rosa Asih</div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
            <div class="hovereffect">
                <img src="<?php echo base_url('asset_front/images/porfolios/kukila_ayam_pedaging.jpg'); ?>">
                <div class="name">KUKILA AYAM PEDAGING</div>
                <div class="overlay">

                    <div class="full">
                        <div class="button">
                            <a data-toggle="modal" data-target="#projectDetail">View Detail Produk <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                        <!-- <div class="customer">
                            Customer: Microsoft Asia. Team: 5 members. Time: 14 days<br>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
            <div class="hovereffect">
                <img src="<?php echo base_url('asset_front/images/porfolios/kukila_puyuh_petelur.jpg'); ?>">
                <div class="name">KUKILA PUYUH PETELUR</div>
                <div class="overlay">

                    <div class="full">
                        <div class="button">
                            <a data-toggle="modal" data-target="#projectDetail">View Detail Produk<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                       <!--  <div class="customer">
                            Customer: Microsoft Asia. Team: 5 members. Time: 14 days<br>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
            <div class="hovereffect">
                <img src="<?php echo base_url('asset_front/images/porfolios/kukila_ayam_petelur.jpg'); ?>">
                <div class="name">KUKILA AYAM PETELUR</div>
                <div class="overlay">

                    <div class="full">
                        <div class="button">
                            <a data-toggle="modal" data-target="#projectDetail">View Detail Produk<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                        <!-- <div class="customer">
                            Customer: Microsoft Asia. Team: 5 members. Time: 14 days<br>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
            <div class="hovereffect">
                <img src="<?php echo base_url('asset_front/images/porfolios/kukila_jantan_joper.jpg'); ?>">
                <div class="name">KUKILA PEJANTAN/JOPER</div>
                <div class="overlay">

                    <div class="full">
                        <div class="button">
                            <a data-toggle="modal" data-target="#projectDetail">View Detail Produk<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                        <!-- <div class="customer">
                            Customer: Microsoft Asia. Team: 5 members. Time: 14 days<br>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
            <div class="hovereffect">
                <img src="<?php echo base_url('asset_front/images/porfolios/jatayu_puyuh_petelur.jpg'); ?>">
                <div class="name">JATAYU PUYUH PETELUR</div>
                <div class="overlay">

                    <div class="full">
                        <div class="button">
                            <a data-toggle="modal" data-target="#projectDetail">View Detail Project <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                        <!-- <div class="customer">
                            Customer: Microsoft Asia. Team: 5 members. Time: 14 days<br>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
            <div class="hovereffect">
                <img src="<?php echo base_url('asset_front/images/porfolios/jatayu_ayam_pedaging.jpg'); ?>">
                <div class="name">JATAYU AYAM PEDAGING</div>
                <div class="overlay">

                    <div class="full">
                        <div class="button">
                            <a data-toggle="modal" data-target="#projectDetail">View Detail Project <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                        <!-- <div class="customer">
                            Customer: Microsoft Asia. Team: 5 members. Time: 14 days<br>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
            <div class="hovereffect">
                <img src="<?php echo base_url('asset_front/images/porfolios/jatayu_pejantan_joper.jpg'); ?>">
                <div class="name">JATAYU JANTAN/JOPER</div>
                <div class="overlay">

                    <div class="full">
                        <div class="button">
                            <a data-toggle="modal" data-target="#projectDetail">View Detail Project <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                        <!-- <div class="customer">
                            Customer: Microsoft Asia. Team: 5 members. Time: 14 days<br>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
            <div class="hovereffect">
                <img src="<?php echo base_url('asset_front/images/porfolios/pakan_request_khusus.jpg'); ?>">
                <div class="name">PAKAN REQUEST KHUSUS</div>
                <div class="overlay">

                    <div class="full">
                        <div class="button">
                            <a data-toggle="modal" data-target="#projectDetail">View Detail Project <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                        <!-- <div class="customer">
                            Customer: Microsoft Asia. Team: 5 members. Time: 14 days<br>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
</div>
<!-- End of Home What we did -->

<!-- Modal -->
<div id="projectDetail" class="modal modalFullscreen">

    <div class="modal-content">
        <span class="closeX" data-dismiss="modal">&times;</span>

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-left">

                <div id="myCarousel3" class="carousel slide carousel-fade" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel3" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel3" data-slide-to="1"></li>
                        <li data-target="#myCarousel3" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="<?php echo base_url('asset_front/images/porfolios/1.jpg'); ?> " class="left-img">
                        </div>

                        <div class="item">
                            <img src="<?php echo base_url('asset_front/images/porfolios/2.jpg'); ?> " class="left-img">
                        </div>

                        <div class="item">
                            <img src="<?php echo base_url('asset_front/images/porfolios/3.jpg'); ?> " class="left-img">
                        </div>
                    </div>

                </div>

            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-right">
                <h3>
                    My project name
                </h3>
                <div class="description">
                    <p class="customer">
                        Customer: <b>Microsoft Asia</b>.                                
                        Address: <b>No 1 - Lorem ipsum dolor sit amet </b>.                                                                
                    </p>
                    <p>
                        Website: <b><a href="">herrypham.com</a> </b>
                    </p>
                    <p>
                        Team members: <b>5</b>. Time: <b>14 days</b>.                                     
                    </p>
                    <p class="team-face">
                        <img src="<?php echo base_url('asset_front/images/face/face1.jpg'); ?> ">
                        <img src="<?php echo base_url('asset_front/images/face/face2.jpg'); ?> ">
                        <img src="<?php echo base_url('asset_front/images/face/face3.jpg'); ?> ">
                        <img src="<?php echo base_url('asset_front/images/face/face4.jpg'); ?> ">
                        <img src="<?php echo base_url('asset_front/images/face/face5.jpg'); ?> ">
                    </p>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. Nam neque nulla, venenatis non iaculis non, scelerisque ultricies metus.

                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. 

                    </p>
                </div>
                <div class="button">
                    <a href="" class="button-default">MORE INFORMATION</a>
                </div>
            </div>
        </div>
    </div>
</div>  

<div class="home-numbers" id="kontak_sales">
    <h3>PEMESANAN LANGSUNG DAPAT MENGHUBUNGI CUSTOMER SERVICE KAMI</h3>
    <div class="subtitle">Daftar sales yang sudah ditentukan tiap wilayah</b></div>
    <div class="container inner">

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 item">
            <h5>0813-2912-3898</h5>
            <div class="photo">
                <img src="<?php echo base_url('asset_front/images/sales/sales-tuhu.png'); ?> ">
            </div>
            <h5>SALES SPV</h5>
            <div class="deskripsi">
                <h3>TUHU</h3>
                <p>Seluruh Wilayah Jawa</p>
            </div>

        </div>     
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 item">
            <h5>0815-7882-2227</h5>
            <div class="photo">
                <img src="<?php echo base_url('asset_front/images/sales/sales-aji.png'); ?> ">
            </div>
            <h5>SALES AREA 1</h5>
             <div class="deskripsi">
                <h3>AJI</h3>
                <p>Jogja Timur, Karisidenan Surakarta, Karisidenan Pati</p>
            </div>
        </div>     
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 item">
            <h5>0812-2751-5262</h5>
            <div class="photo">
                <img src="<?php echo base_url('asset_front/images/sales/sales-handoko.png'); ?> ">
            </div>
            <h5>SALES AREA 2</h5>
            <div class="deskripsi">
                <h3>HANDOKO</h3>
                <p>Jogja Barat, Karisidenan Kedu, Karisidenan Semarang</p>
            </div>

        </div>      

    </div>
</div>
<!-- End of Home Number --> 

<!-- <div class="home-tryfree30 panel-toparrow">
    <div class="container">
        <h3>Kontak Sales</h3>
        <div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra
            aliquam.<br> Nam neque nulla, venenatis non iaculis non, scelerisque ultricies metus.
        </div>

        <div class="button">
            <a href="page-download.html" class="button-primary"><i class="fa fa-book"></i> Daftar Kontak Sales</a>
        </div>
    </div>
</div> -->
<!-- End of Home tryfree30 -->        

  

<div class="home-testimonial">
    <div class="container content">

        <div id="myCarousel5" class="carousel slide carousel-fade" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel5" data-slide-to="0" class="active"></li>
               <!--  <li data-target="#myCarousel5" data-slide-to="1"></li>
                <li data-target="#myCarousel5" data-slide-to="2"></li> -->
            </ol>

            <div class="carousel-inner">
                <div class="item active">
                    <div class="avatar">
                        <img class="img-circle" src="<?php echo base_url('asset_front/images/face/face2.jpg') ?>">
                    </div>
                    <h5>PAKDE WAKIDI</h5>
                    <div class="info">Peternak Ayam Broiler - Kebumen</div>
                    <div class="quotes">
                        Pakane apik krabang telor tebal telore yo standar ora gegeden ora keciliken sak zak kanggo 2170 ekor yo standar irit. 
                        Pakannya bagus (baik), krabang telur tebal, telurnya juga standar tidak terlalu besar tidak juga kekecilan. Satu sak bisa untuk 2.170 ekor, ya termasuk standar irit.
                    </div>
                </div>
                <!-- <div class="item">
                    <div class="avatar">
                        <img class="img-circle" src="<?php echo base_url('asset_front/images/face/face2.jpg') ?>">
                    </div>
                    <h5>Dr. Molestie Justo</h5>
                    <div class="info">CO - Adipiscing Asia - Project: <a data-toggle="modal" data-target="#projectDetail">Praesent molestie</a></div>
                    <div class="quotes">
                        Amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. Nam neque nulla, venenatis non iaculis non, scelerisque ultricies metus.
                    </div>
                </div>
                <div class="item">
                    <div class="avatar">
                        <img class="img-circle" src="<?php echo base_url('asset_front/images/face/face4.jpg') ?>">
                    </div>
                    <h5>Mrs. Pharetra Aliquam</h5>
                    <div class="info">Project Manager - Nam Neque Nulla Corp - Project: <a data-toggle="modal" data-target="#projectDetail">Nam neque nulla</a></div>
                    <div class="quotes">
                        Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. Nam neque nulla, venenatis non iaculis non, scelerisque ultricies metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </div>
                </div> -->

            </div>
        </div>
    </div>
</div>
<!-- End of Home testimonial -->

     
<div class="home-blogs">
    <h3>News</h3>
    <div class="menu">
        <ul>
            <li><b>Pilih Kategori</b> </li>
            <li class="active"><a href="">Tips</a> </li>
            <li><a href="">Kesehatan</a> </li>
            <li><a href="">Berita</a> </li>
            <li><a href="">Video</a> </li>
            <li><a href="">Lainnya</a> </li>
        </ul>

    </div>

    <div class="contents-article">
        <div id="blogs-carousel" class="carousel slide carousel-fade" data-ride="carousel">

            <div class="carousel-inner">
                
                <!-- <div class="item active">
                    <div class="container">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="article">
                                <div class="article-img hovereffect">
                                    <a href="blog-detail-regular2.html"><img src="<?php echo base_url('asset_front/images/articles/article1.jpg'); ?>"></a>
                                    <div class="overlay">
                                        <a class="info" href="blog-detail-regular2.html"><i class="fa fa-arrow-right" aria-hidden="true"></i> click to show detail</a>
                                    </div>
                                </div>
                                <div class="article-content">
                                    <h5><a href="blog-detail-regular2.html">New project has just finished when consectetur adipiscing elit</a></h5>
                                    <div class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. 
                                    </div>
                                    <div class="date">
                                        12/7/2017 2:30. <a href="">Jenny Ngo</a>. <a href="">14 comments</a>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="article sticky-feature">
                                <div class="article-img hovereffect">
                                    <a href="blog-detail-regular2.html"><img src="<?php echo base_url('asset_front/images/articles/article2.jpg'); ?>"></a>
                                    <div class="overlay">
                                        <a class="info" href="blog-detail-regular2.html"><i class="fa fa-arrow-right" aria-hidden="true"></i> click to show detail</a>
                                    </div>
                                </div>
                                <div class="article-content">
                                    <h5><a href="blog-detail-regular2.html">Review: Chronicling Books and Souls Integer non elementum nunc</a></h5>
                                    <div class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. 
                                    </div>
                                    <div class="date">
                                        3/9/2017 1:50. <a href="">Henry Pham</a>. <a href="">0 comments</a>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="article">
                                <div class="article-img hovereffect">
                                    <a href="blog-detail-regular2.html"><img src="<?php echo base_url('asset_front/images/articles/article3.jpg'); ?>"></a>
                                    <div class="overlay">
                                        <a class="info" href="blog-detail-regular2.html"><i class="fa fa-arrow-right" aria-hidden="true"></i> click to show detail</a>
                                    </div>
                                </div>
                                <div class="article-content">
                                    <h5><a href="blog-detail-regular2.html">Morning Briefing: Asia Edition. Praesent molestie justo dictum</a></h5>
                                    <div class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. 
                                    </div>
                                    <div class="date">
                                        12/7/2017 20:10. <a href="">Admin</a>. <a href="">14 comments</a>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div> 
                <div class="item">
                    <div class="container">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="article">
                                <div class="article-img hovereffect">
                                    <a href="blog-detail-regular2.html"><img src="<?php echo base_url('asset_front/images/articles/article4.jpg'); ?>"></a>
                                    <div class="overlay">
                                        <a class="info" href="blog-detail-regular2.html"><i class="fa fa-arrow-right" aria-hidden="true"></i> click to show detail</a>
                                    </div>
                                </div>
                                <div class="article-content">
                                    <h5><a href="blog-detail-regular2.html">New project has just finished when consectetur adipiscing elit</a></h5>
                                    <div class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. 
                                    </div>
                                    <div class="date">
                                        12/7/2017 2:30. <a href="">Jenny Ngo</a>. <a href="">14 comments</a>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="article">
                                <div class="article-img hovereffect">
                                    <a href="blog-detail-regular2.html"><img src="<?php echo base_url('asset_front/images/articles/article5.jpg'); ?>"></a>
                                    <div class="overlay">
                                        <a class="info" href="blog-detail-regular2.html"><i class="fa fa-arrow-right" aria-hidden="true"></i> click to show detail</a>
                                    </div>
                                </div>
                                <div class="article-content">
                                    <h5><a href="blog-detail-regular2.html">Review: Chronicling Books and Souls Integer non elementum nunc</a></h5>
                                    <div class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. 
                                    </div>
                                    <div class="date">
                                        3/9/2017 1:50. <a href="">Henry Pham</a>. <a href="">0 comments</a>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="article sticky-new">
                                <div class="article-img hovereffect">
                                    <a href="blog-detail-regular2.html"><img src="<?php echo base_url('asset_front/images/articles/article6.jpg'); ?>"></a>
                                    <div class="overlay">
                                        <a class="info" href="blog-detail-regular2.html"><i class="fa fa-arrow-right" aria-hidden="true"></i> click to show detail</a>
                                    </div>
                                </div>
                                <div class="article-content">
                                    <h5><a href="blog-detail-regular2.html">Morning Briefing: Asia Edition. Praesent molestie justo dictum</a></h5>
                                    <div class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. 
                                    </div>
                                    <div class="date">
                                        12/7/2017 20:10. <a href="">Admin</a>. <a href="">14 comments</a>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>  -->

                
                    <div class="item active">
                        <div class="container">
                            
                            <?php $i = 0; foreach ($data_artikel as $artikel): ?>

                                <?php if ($i % 4 == 3): ?>
                                    <?php echo $i ."<br>"; ?>
                                    <?php $i++; ?>
                                <?php else: ?>
                                    <?php echo $i."- lanjutkan"."<br>"; ?>
                                <?php endif ?>
                            
                                <!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                                    <div class="article">
                                        <?php if ($artikel->type_artikel == "Video"): ?>
                                            <div class="photo">
                                                <iframe class="media-video" width="100%" src="https://www.youtube.com/embed/<?php echo $artikel->link_video ?>" frameborder="0" allowfullscreen style="height: 220px;"></iframe>
                                            </div>
                                        <?php else: ?>
                                            <div class="article-img hovereffect">
                                                <a href=""><img src="<?php echo base_url('./storage_img/img_berita/'.$artikel->cover_artikel); ?>"></a>
                                                <div class="overlay">
                                                    <a class="info" href=""><i class="fa fa-arrow-right" aria-hidden="true"></i> click to show detail</a>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                        
                                        <div class="article-content">
                                            <h5><a href="blog-detail-regular2.html"><?php echo $artikel->nama_artikel ?></a></h5>
                                            <div class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer non elementum nunc. Praesent molestie justo dictum pharetra aliquam. 
                                            </div>
                                            <div class="date">
                                                12/7/2017 2:30. <a href="">Jenny Ngo</a>. <a href="">14 comments</a>
                                            </div>

                                        </div>
                                    </div>

                                </div>  -->


                            <?php $i++; endforeach ?>

                        </div>
                    </div>



            </div>

            <div class="clearfix"></div>
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#blogs-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#blogs-carousel" data-slide-to="1"></li>
                <li data-target="#blogs-carousel" data-slide-to="2"></li>
            </ol>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#blogs-carousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#blogs-carousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div> <!-- contents-article -->
</div>
<!-- End of Home Blogs -->        

      
<!-- <div class="home-contact">

    <div class="container content">
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-left">
            <h3>Contact us</h3>
            <div class="media">
                <div class="media-left media-middle">
                    <a href="#">
                        <img src="<?php echo base_url('asset_front/images/icon-address.png'); ?>">
                    </a>
                </div>
                <div class="media-body">
                    <div class="description">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    </div>
                </div>
            </div>
            <div class="media">
                <div class="media-left media-middle">
                    <a href="#">
                        <img src="<?php echo base_url('asset_front/images/icon-phone.png'); ?>">
                    </a>
                </div>
                <div class="media-body">
                    <div class="description">
                        +01 8908 90888
                    </div>
                </div>
            </div>
            <div class="media">
                <div class="media-left media-middle">
                    <a href="#">
                        <img src="<?php echo base_url('asset_front/images/icon-email.png'); ?>">
                    </a>
                </div>
                <div class="media-body">
                    <div class="description">
                        <a href="mailto:loremipsum@gmail.com">loremipsum@gmail.com</a><br><a href="mailto:contact@henrypham.com">contact@loremipsum.com</a>
                    </div>
                </div>
            </div>
            <div class="media">
                <div class="media-left media-middle">
                    <a href="#">
                        <img src="<?php echo base_url('asset_front/images/icon-skype.png'); ?>">
                    </a>
                </div>
                <div class="media-body">
                    <div class="description">
                        <a href="">loremipsum</a><br>
                        Online 27/7
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-right">
            <form  data-toggle="validator" action="" method="GET"> 
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-group">
                        <input class="form-control input-lg" id="inputsm" type="text" placeholder="Name" required>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-group">
                        <input class="form-control input-lg" id="inputsm" type="email" placeholder="Email" required>
                    </div>
                </div>
                <div class="form-group">
                    <input class="form-control input-lg" id="inputsm" type="text" placeholder="Subject" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control input-lg" rows="6" placeholder="Message" required></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-lg" type="submit">
                        SEND MESSAGE
                    </button>
                </div>
            </form>
        </div>
    </div>
</div> -->
<!-- End of Home Contact -->

</main>

<?php $this->load->view('frontpages/footer'); ?>

</body>

 <?php require_once(APPPATH .'views/include/front/inc_script.php'); ?>
</html>