<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
    <style>
      #myMap {
         height: 400px;
         width: 575px;
      }

      #myMapDepo {
         height: 400px;
         width: 575px;
      }
    </style>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title">PT <span>Sari Rosa Asih</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png') ?> " alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png'); ?>" alt="">Administrator
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
           <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Distributor<small> Distributor Wilayah</small></h3>
              </div>
              
            </div>

            <div class="clearfix"></div>

            <div class="row">
              
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  
                  <form action="" method="POST" role="form">
                    <legend>Form Tambah Wilayah</legend>
                  
                    <div class="form-group">
                      <label for="">Nama Wilayah</label>
                      <input type="text" class="form-control" id="" placeholder="Wilayah">
                    </div>

                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label for="">Koordinat Wilayah X</label>
                          <input type="text" class="form-control" id="latitude" placeholder="X">
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label for="">Koordinat Wilayah Y</label>
                          <input type="text" class="form-control" id="longitude" placeholder="Y">
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="">Alamat Wilayah</label>
                      <input type="text" class="form-control" id="address" placeholder="Alamat">
                    </div>

                    <div class="form-group">
                      <label for="">Drag Drop Marker Map</label>
                      <div id="myMap"></div>
                    </div>
                  
                    <button type="submit" class="btn pull-right btn-primary">Submit Wilayah</button>
                  </form>

                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      
                      <?php if (isset($_SESSION['message_data'])): ?>
                        <div class="alert alert-success margin-bottom-30" role="alert">
                          <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <?php echo $_SESSION['message_data'] ?>
                        </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                        <div class="alert alert-danger margin-bottom-30" role="alert">
                          <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <?php echo $_SESSION['error_data'] ?>
                        </div>
                      <?php endif ?>

                    </p>
                    <table class="table table-striped datatable">
                      <thead>
                        <tr>
                          <th>Nama Wilayah</th>
                          <th>Kor Wilayah X</th>
                          <th>Kor Wilayah Y</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                </div>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  
                  <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Tambah Depo</a>
                  <div class="modal fade" id="modal-id">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                          
                          <form action="" method="POST" role="form">
                            <legend>Form Tambah Depo / Poultry Shop</legend>
                          
                            <div class="form-group">
                              <label for="">Nama Depo / Poultry Shop</label>
                              <input type="text" class="form-control" id="" placeholder="Nama Depo / Poultry Shop">
                            </div>

                            <div class="form-group">
                              <label for="">Posisi Wilayah</label>
                              <select name="" id="input" class="form-control" required="Posisi Wilayah">
                                <option value="">-- PILIH WILAYAH --</option>
                              </select>
                            </div>

                            <div class="form-group">
                              <label for="">Nama Pemilik</label>
                              <input type="text" class="form-control" id="" placeholder="Nama Pemilik">
                            </div>

                            <div class="form-group">
                              <label for="">Alamat Depo</label>
                              <textarea name="" id="input" class="form-control" rows="3" required="required"></textarea>
                            </div>

                            <div class="row">
                              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                  <label for="">Koordinat Depo X</label>
                                  <input type="text" class="form-control" id="latitude_depo" placeholder="X">
                                </div>
                              </div>
                              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                  <label for="">Koordinat Depo Y</label>
                                  <input type="text" class="form-control" id="longitude_depo" placeholder="Y">
                                </div>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="">Alamat Depo</label>
                              <input type="text" class="form-control" id="address_depo" placeholder="Alamat">
                            </div>

                            <div class="form-group">
                              <label for="">Drag Drop Marker Map</label>
                              <div id="myMapDepo"></div>
                            </div>
                          
                            <button type="submit" class="btn pull-right btn-primary">Submit Depo / Poultry Shop</button>
                          </form>

                        </div>
                        
                      </div>
                    </div>
                  </div>
                  
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      
                      <?php if (isset($_SESSION['message_data'])): ?>
                        <div class="alert alert-success margin-bottom-30" role="alert">
                          <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <?php echo $_SESSION['message_data'] ?>
                        </div>
                      <?php endif ?>

                      <?php if (isset($_SESSION['error_data'])): ?>
                        <div class="alert alert-danger margin-bottom-30" role="alert">
                          <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <?php echo $_SESSION['error_data'] ?>
                        </div>
                      <?php endif ?>

                    </p>
                    <table class="table table-striped datatable">
                      <thead>
                        <tr>
                          <th>Cover</th>
                          <th>Judul Artikel</th>
                          <th>Kategori</th>
                          <th>Author</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
      $(document).ready(function() {
         $('#summernote').summernote({
          height: 300
        });
      });
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdWLY_Y6FL7QGW5vcO3zajUEsrKfQPNzI"></script>
    <script type="text/javascript"> 
      var map;
      var marker;
      var myPosition = new google.maps.LatLng(-7.797068,110.370529);
      var geocoder = new google.maps.Geocoder();
      var infowindow = new google.maps.InfoWindow();
      function initialize(){
      var mapOptions = {
        zoom: 18,
        center: myPosition,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      map = new google.maps.Map(document.getElementById("myMap"), mapOptions);

      marker = new google.maps.Marker({
        map: map,
        position: myPosition,
        draggable: true 
      }); 

      geocoder.geocode({'latLng': myPosition }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            $('#latitude,#longitude').show();
            $('#address').val(results[0].formatted_address);
            $('#latitude').val(marker.getPosition().lat());
            $('#longitude').val(marker.getPosition().lng());
            infowindow.setContent(results[0].formatted_address);
            infowindow.open(map, marker);
          }
        }
      });

      google.maps.event.addListener(marker, 'dragend', function() {

        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              $('#address').val(results[0].formatted_address);
              $('#latitude').val(marker.getPosition().lat());
              $('#longitude').val(marker.getPosition().lng());
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
              }
            }
            });
        });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <script type="text/javascript"> 
      var map;
      var marker;
      var myLatlng = new google.maps.LatLng(-7.797068,110.370529);
      var geocoder = new google.maps.Geocoder();
      var infowindow = new google.maps.InfoWindow();
      function initialize(){
      var mapOptions = {
        zoom: 18,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      map = new google.maps.Map(document.getElementById("myMapDepo"), mapOptions);

      marker = new google.maps.Marker({
        map: map,
        position: myLatlng,
        draggable: true 
      }); 

      geocoder.geocode({'latLng': myLatlng }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            $('#latitude_depo,#longitude_depo').show();
            $('#address_depo').val(results[0].formatted_address);
            $('#latitude_depo').val(marker.getPosition().lat());
            $('#longitude_depo').val(marker.getPosition().lng());
            infowindow.setContent(results[0].formatted_address);
            infowindow.open(map, marker);
          }
        }
      });

      google.maps.event.addListener(marker, 'dragend', function() {

        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              $('#address_depo').val(results[0].formatted_address);
              $('#latitude_depo').val(marker.getPosition().lat());
              $('#longitude_depo').val(marker.getPosition().lng());
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
              }
            }
            });
        });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </body>
</html>